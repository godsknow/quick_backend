<?php 
class Migration_test_user_admin_db extends CI_Migration{

    public function up(){

        $this->dbforge->add_field(array(

            'id' => array (
                'type' => 'INT',
                'unsigned' => TRUE ,
                'auto_increment' => TRUE ,
                'null' => FALSE
            )
            , "'name'  VARCHAR(255) NOT NULL"
            , "'password'  VARCHAR(255) NOT NULL"
            , "'type'  VARCHAR(45) NOT NULL"
            ));
            $this->dbforge->add_key('id',TRUE);
            $this->dbforge->create_table('user_admin',true);
    }

    public function down() {
        $this->dbforge->drop_table('user_admin',TRUE);
    }
}
?>