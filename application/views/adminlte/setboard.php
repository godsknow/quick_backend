<?php
$modeEdit = FALSE;
if(isset($data) && !is_null($data) ) {
  $modeEdit = true;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header'];?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Cost</a></li>
                        <li class="breadcrumb-item active">Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Set Board</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="changeboard"  method="post" onsubmit="return validateForm()" name="changeboard">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="form-group">
                                    <?php
                    // if($modeEdit){

                    foreach($data as $row) {  
                        ?>

                                        <label>Name</label>
                                        <input type="text" class="form-control" name="value_Name"
                                        value="<?php echo  $row->BoardName;?>">
                                        <br>
                                        <label>BoardId</label>
                                        <input type="text" class="form-control" name="value_BoardId"
                                        value="<?php echo  $row->BoardId;?>">
                                        <br>
                                        <label>BoardNumber</label>
                                        <input type="text" class="form-control" name="value_BoardNumber"
                                        value="<?php echo  $row->BoardNumber;?>">
                                        <br>
                                        <label>CreateDate</label>
                                        <input type="text" class="form-control" name="station_id"
                                        value="<?php echo  $row->CreateDate;?>" readonly>
                                        <br>
                                        <label>Capacity</label>
                                        <input type="text" class="form-control" name="station_id"
                                        value="<?php echo  $row->Capacity;?>" readonly>
                                    </div>
                                <?php 
                                }
                                ?>
                            <!-- /.card-body -->
                            <div class="text-center">
                            <input type="hidden" name="row_id" value="<?php echo $template['row_id'];?>">
                                <button type="submit"  class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js');?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js');?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js');?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('asset/adminlte/plugins/select2/js/select2.full.min.js');?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js');?>">
</script>
<!-- InputMask -->
<script src="<?php echo base_url('asset/adminlte/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js');?>"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/daterangepicker/daterangepicker.js');?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js');?>">
</script>
<!-- Tempusdominus Bootstrap 4 -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js');?>">
</script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>"></script>
<!-- page script -->
<script>
$(function() {
    $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
        "searching": false,
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});

$(function() {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
    })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', {
        'placeholder': 'mm/dd/yyyy'
    })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY hh:mm A'
        }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month')
                    .endOf('month')
                ]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
        },
        function(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
        format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
        $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function() {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

})

function validateForm() {
  var value = document.forms["changeCost"]["value"].value;  
  if (value == "" ) {
    alert("No Edit");
    return false;
  }
}

</script>