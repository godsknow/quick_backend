<?php
$modeEdit = false;
if (isset($data) && !is_null($data)) {
 $modeEdit = true;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">ตู้น้ำมัน</a></li>
                        <li class="breadcrumb-item active">เพิ่มตู้น้ำมัน</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มตู้น้ำมัน</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="AddStation" method="POST" role="form" onsubmit="return validateForm()"
                            name="AddStation">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>ชื่อตู้</label>
                                            <input type="text" class="form-control" name="StationName"
                                                placeholder="เพิ่มชื่อตู้">
                                            <br>
                                            <label>รหัสเฉพาะ</label>
                                            <input type="text" class="form-control" Id="StationCode" name="StationCode"
                                                placeholder="เพิ่มรหัสเฉพาะ" readonly>
                                            <input type="hidden" Id="count" value="<?php echo $CountNo[0]->CountNo;?>">
                                            <br>
                                            <div class="row">
                                                <div class="col">
                                                    <label>ตำแหน่งสถาที่</label>
                                                    <input type="text" class="form-control" name="LocationName"
                                                        placeholder="เพิ่มตำแหน่งสถาที่">
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>ละติจูด</label>
                                                    <input type="text" class="form-control" id="Latitude"
                                                        name="Latitude" placeholder="เพิ่มละติจูด" readonly>
                                                </div>
                                                <div class="col-4">
                                                    <label>ลองจิจูด</label>
                                                    <input type="text" class="form-control" id="Longitude"
                                                        name="Longitude" placeholder="เพิ่มลองจิจูด" readonly>
                                                </div>
                                                <div class="col-4">
                                                    <label>กด</label>
                                                    <button type="button" class="form-control btn-danger"
                                                        onclick="getLocation()">
                                                        กำหนด
                                                    </button> <br>
                                                </div>
                                            </div>
                                            <label>ที่อยู่ไอพี</label>
                                            <input type="text" class="form-control" name="Ip_Address"
                                                placeholder="เพิ่มที่อยู่ไอพี">
                                            <br>
                                            <label>น้ำมันคงเหลือ</label>
                                            <input type="text" class="form-control" name="Remaining"
                                                placeholder="เพิ่มน้ำมันคงเหลือ">
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>เซ็ตชั่นไอดี</label>
                                            <input type="text" class="form-control" name="SectionId"
                                                placeholder="เพิ่มเซ็ตชั่นไอดี">
                                            <br>
                                            <label>เวอร์ชั่นของแอพลิเคชั่น</label>
                                            <input type="text" class="form-control" name="AppReleaseVersion"
                                                placeholder="เพิ่มเวอร์ชั่นของแอพลิเคชั่น">
                                            <br>
                                            <label>เวอร์ชั่นของแอพลิเคชั่นปัจจุบัน</label>
                                            <input type="text" class="form-control" name="AppCurrentVersion"
                                                placeholder="เพิ่มเวอร์ชั่นของแอพลิเคชั่นปัจจุบัน">
                                            <br>
                                            <label>ปริมาตรของตู้น้ำมัน</label>
                                            <input type="text" class="form-control" name="MaxCapacity"
                                                placeholder="เพิ่มปริมาตรของตู้น้ำมัน">
                                            <br>
                                            <label>ยอดเงิน</label>
                                            <input type="number" class="form-control" name="Balance"
                                                placeholder="เพิ่มยอดเงิน" value="0">
                                            <br>
                                            <label for="">เลือกเส้นทาง </label>
                                            <select class="form-control" name="CusId">
                                                <option> --Selected-- </option>
                                                <?php
foreach ($nameRoute['result'] as $row => $value) {
 ?>
                                                <option value="<?php echo $value['RouteGroupId']; ?>">
                                                    <?php echo $value['RouteName']; ?></option>
                                                <?php
}
?>
                                            </select>
                                            <br>
                                            <div class="text-center">
                                                <button type="submit" class="form-control btn btn-primary">ตกลง</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <div class="row">
                                    <div class="col">

                                    </div>
                                </div>
                        </form>

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('asset/adminlte/plugins/select2/js/select2.full.min.js'); ?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js'); ?>">
</script>
<!-- InputMask -->
<script src="<?php echo base_url('asset/adminlte/plugins/moment/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js'); ?>"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>">
</script>
<!-- Tempusdominus Bootstrap 4 -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?>">
</script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
<!-- page script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
    integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
    crossorigin="anonymous"></script>
<script>
$(function() {
    // $("#example1").DataTable({
    //     "responsive": true,
    //     "autoWidth": false,
    //     "searching": false,
    // });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});

var latitude = document.getElementById("Latitude");
var longitude = document.getElementById("Longitude");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    // alert(position.coords.latitude);
    longitude.value = position.coords.longitude;
    latitude.value = position.coords.latitude;
    //   x.innerHTML = "Latitude: " + position.coords.latitude +
    //longitude.value = position.coords.longitude;
}

function validateForm() {
    var StationName = document.forms["AddStation"]["StationName"].value;
    var StationCode = document.forms["AddStation"]["StationCode"].value;
    var LocationName = document.forms["AddStation"]["LocationName"].value;
    var Latitude = document.forms["AddStation"]["Latitude"].value;
    var Longitude = document.forms["AddStation"]["Longitude"].value;
    var Ip_Address = document.forms["AddStation"]["Ip_Address"].value;
    var Remaining = document.forms["AddStation"]["Remaining"].value;
    var SectionId = document.forms["AddStation"]["SectionId"].value;
    var AppReleaseVersion = document.forms["AddStation"]["AppReleaseVersion"].value;
    var AppCurrentVersion = document.forms["AddStation"]["AppCurrentVersion"].value;
    var MaxCapacity = document.forms["AddStation"]["MaxCapacity"].value;
    var Balance = document.forms["AddStation"]["Balance"].value;

    if (StationName == "" || StationCode == "" || LocationName == "" || Latitude == "" || Longitude == "" ||
        Ip_Address == "" || Remaining == "" || SectionId == "" || AppReleaseVersion == "" || AppCurrentVersion == "" ||
        MaxCapacity == "" || Balance == "") {
        alert("No Register");
        return false;
    }
}

function pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

$(function() {
    var a = $('#count').val();

    var result = pad(a, 6);
    $('#StationCode').val("HPO-" + result);
});
</script>