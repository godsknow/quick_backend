
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $template['header'];?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                <a href="<?php echo $template['link'];?>">
                  <?php echo $template['parent'];?>
                </a>
              </li>
              <li class="breadcrumb-item active">
                <?php echo $template['page'];?>
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <iframe src="http://grafana.wealthvending.co.th/d/ZRZhOLZMk/oil-other?orgId=1&from=now%2FM&to=now%2FM&kiosk&theme=light" width="100%" height="600" frameborder="0"></iframe>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js');?>"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <!-- DataTables -->
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js');?>"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url('asset/adminlte/dist/js/demo.js');?>"></script>
  

