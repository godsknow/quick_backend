  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $template['header'];?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                <a href="<?php echo $template['link'];?>">
                  <?php echo $template['parent'];?>
                </a>
              </li>
              <li class="breadcrumb-item active">
                <?php echo $template['page'];?>
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner" onload="showHint()">
                <h3 id="demo">
                  <?php foreach ($sumValueOneMonth['result'][0] as $key => $value) { ?>
                      <th><?php echo number_format(round($value)); ?></th> บาท
                      <!-- <i class="fas fa-arrow-up"></i> -->
                  <?php } ?>
                </h3>  
                <p>ยอดขายภายใน 1 เดือน</p>
              </div>
              <div class="icon">
                <i class="fas fa-money-bill-wave"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3> <!--id="sumInvoice"-->
                <?php foreach ($sumCapacityOneMonth['result'][0] as $key => $value) { ?>
                    <th><?php echo number_format(round($value)); ?></th> ลิตร
                <?php } ?>
                </h3>
                <p>ปริมาณน้ำมันที่ขายไปภายใน 1 เดือน</p>
              </div>
              <div class="icon">
                <i class="fas fa-sort-numeric-up-alt"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php foreach ($countNumOilStation['result'][0] as $key => $value) { ?>
                    <th><?php echo $value; ?></th>
                <?php } ?> ตู้</h3>
                <p>จำนวนตู้น้ำมัน</p>
              </div>
              <div class="icon">
                <i class="fas fa-database"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
        </div>
        <div class="container-fluid">
        <div class="row">
          <div class="col-12">
                <div class="card card-primary card-outline card-outline-tabs">
                  <div class="card-body">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                      <div class="tab-pane fade show active" id="custom-tabs-four-show" role="tabpanel" aria-labelledby="custom-tabs-four-show-tab">
                        <table id="example2" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <?php foreach ($selectInvoice['result'][0] as $key => $value) { ?>
                                  <th><?php echo $key; ?></th>
                              <?php } ?>
                            </tr>
                          </thead>
                          <tbody><?php foreach ($selectInvoice['result'] as $key => $value) { ?>
                            <tr>
                            <?php foreach ($value as $key2 => $value2) { ?>
                              <td><?php echo $value2;?></td>
                            <?php } ?>
                            </tr>
                            <?PHP } ?>
                          </tbody>
                          <tfoot>
                            <tr><?php foreach ($selectInvoice['result'][0] as $key => $value) { ?>
                              <th><?php echo $key; ?></th>
                              <?php } ?>
                               <!-- <th>Action</th>  -->
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>            
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      </div>
    </section>
    <!-- โค้ดเก่าดึง Grafana -->
    <!-- <section class="content">
      <div class="container-fluid">
        <iframe src="http://grafana.wealthvending.co.th/d/swWzOYZGk/invoice?orgId=1&from=1594691514977&to=1594713114977&theme=light&kiosk" width="100%" height="600" frameborder="0"></iframe>
    </section> -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js');?>"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <!-- DataTables -->
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js');?>"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url('asset/adminlte/dist/js/demo.js');?>"></script>
  <script>
    setInterval(function showHint() {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        //alert(this.readyState);
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("demo").innerHTML = this.responseText;
        // } else {
        //   alert("Error");
        }
      };
      xmlhttp.open("GET", "../ajax/ajexInvoice", true);
      xmlhttp.send();
    },2000)
  </script>
 