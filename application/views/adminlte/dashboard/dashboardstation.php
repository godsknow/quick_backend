
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $template['header'];?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                <a href="<?php echo $template['link'];?>">
                  <?php echo $template['parent'];?>
                </a>
              </li>
              <li class="breadcrumb-item active">
                <?php echo $template['page'];?>
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- <iframe src="http://grafana.wealthvending.co.th/d/sYTodLZGz/stations?orgId=1&from=1594691908411&to=1594713508411&theme=light&kiosk" width="100%" height="600" frameborder="0"></iframe> -->
      <!-- /.container-fluid -->
      <!-- <button type="button" onclick="showHint()">Change Content</button>
      <p id="demo"></p> -->
      <div class="row">
          <!-- Left col -->
          <div  class="col-9"  >
            <div  class="container-fluid">
            <iframe src="http://grafana.wealthvending.co.th/d-solo/sYTodLZGz/stations?orgId=1&from=1594691908411&to=1594713508411&theme=light&panelId=2" width="800" height="340" frameborder="0"></iframe>                   
              </div>
            </div>
            <!-- /.card -->
            <div class="col-3">
         
            <div class="col-12">
            <!-- small box -->
            <div id = "card" class="small-box bg-info" style=" height: 160px;" onload="showonlineMachine()">
              <div class="inner">
                <p>จำนวนตู้น้ำมัน (online)</p>
                <h3 id = "demo"  style="font-size:40px ;"><?php foreach ($countStationEnable['result'][0] as $key => $value) { ?>
                    <th><?php echo number_format(round($value)); ?></th> ตู้
                <?php } ?></h3> 
                <!-- <h3 id = "demo"> </h3> -->
                <div class="icon" style = "position: absolute; bottom: 110px; right: 10px;" >
                <i class="fas fa-database"></i>
                </div>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
              <!-- /.card-body -->
            </div>
            <!-- การ์ดตู้น้ำมันออนไลน์ -->
            <div class="col-12">
            <!-- small box -->
            <div id = "card" class="small-box bg-warning" style=" height: 160px;" onload="showofflneMachine()">
              <div class="inner">
                <p>จำนวนตู้น้ำมัน (offline)</p>
                <h3 class = "quotes" id = "demo2" style="font-size:40px ;"><?php foreach ($countStationDisable['result'][0] as $key => $value) { ?>
                    <th><?php echo number_format(round($value)); ?></th> ตู้
                <?php } ?></h3>
                <div class="icon" style = "position: absolute; bottom: 110px; right: 10px;" >
                <i class="fas fa-database"></i>
                </div>
              </div>  
               <!-- การ์ดตู้น้ำมันออฟไลน์ -->
            </div>
              <!-- /.card-body -->
            </div>
            </div>
          
            <!-- /.card -->
            <div class="col-sm-6">
                    <p style="font-size:25px">ตารางแสดงข้อมูลตู้น้ำมัน</p>
                </div>
          <div class="col-12">
                <div class="card card-primary card-outline card-outline-tabs">
                  <div class="card-body">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                      <div class="tab-pane fade show active" id="custom-tabs-four-show" role="tabpanel" aria-labelledby="custom-tabs-four-show-tab">
                        <table id="example2" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <?php foreach ($data['result'][0] as $key => $value) { ?>
                                  <th><?php echo $key; ?></th>
                              <?php } ?>
                            </tr>
                          </thead>
                          <tbody><?php foreach ($data['result'] as $key => $value) { ?>
                            <tr>
                            <?php foreach ($value as $key2 => $value2) { ?>
                              <td><?php echo $value2;?></td>
                            <?php } ?>
                            </tr>
                            <?PHP } ?>
                          </tbody>
                          <tfoot>
                            <tr><?php foreach ($data['result'][0] as $key => $value) { ?>
                              <th><?php echo $key; ?></th>
                              <?php } ?>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                      <div class="tab-pane fade" id="custom-tabs-four-create" role="tabpanel" aria-labelledby="custom-tabs-four-create-tab">
                          Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut ligula tempor, et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas sollicitudin, nisi a luctus interdum, nisl ligula placerat mi, quis posuere purus ligula eu lectus. Donec nunc tellus, elementum sit amet ultricies at, posuere nec nunc. Nunc euismod pellentesque diam. 
                      </div>
                      <div class="tab-pane fade" id="custom-tabs-four-edit" role="tabpanel" aria-labelledby="custom-tabs-four-edit-tab">
                        Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna. 
                      </div>
                      <div class="tab-pane fade" id="custom-tabs-four-delete" role="tabpanel" aria-labelledby="custom-tabs-four-delete-tab">
                        Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis. 
                      </div>
                    </div>            
             
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.col -->
        </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js');?>"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <!-- DataTables -->
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js');?>"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url('asset/adminlte/dist/js/demo.js');?>"></script>
  <script>
  var c = 1;
  var res;
  function showonlineMachine() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if(c == 1){
        res = this.responseText;
        document.getElementById("demo").innerHTML = this.responseText  + ' ตู้';
        c++;
      }else if (res != this.responseText){
        $('#demo').animate({'opacity': 0}, 1000, function () {
        $(this).text(this.responseText + ' ตู้');
        }).animate({'opacity': 1}, 1000);
        res = this.responseText;
      }else if (res == this.responseText){
        document.getElementById("demo").innerHTML = this.responseText  + ' ตู้';
      }
    }
  };
      xmlhttp.open("GET", "../ajax/ajexcountonline", true);
      xmlhttp.send();
  }
  function showofflineMachine() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("demo2").innerHTML = this.responseText  + ' ตู้';
    
    }
      };
      xmlhttp.open("GET", "../ajax/ajexcountoffline", true);
      xmlhttp.send();
  }
  window.setInterval(showofflineMachine, 1000);
  window.setInterval(showonlineMachine, 1000);
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
        "searching": false,

      });
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        
      });
    });
  </script>
  <style>
  #card:hover {
  transform: scale(0.97, 0.97);
  box-shadow: 2px 2px 10px 10px rgba(0,0,0,0.1), 
    -5px -5px 30px 15px rgba(0,0,0,0.1);
  }
  </style>
