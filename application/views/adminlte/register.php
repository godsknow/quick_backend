<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Registration</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('asset/adminlte/plugins/fontawesome-free/css/all.min.css');?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url('asset/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('asset/adminlte/dist/css/adminlte.min.css');?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
  <a href="#"><img src="<?php echo base_url('asset/1592909022698.png'); ?>" alt=""></a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg"> <b> Register</b></p>

      <form action="addUser" method="post">
        <div class="input-group mb-3">
        <?php echo form_error('Name', '<div class="error text-danger">', '</div>'); ?>
          <input type="text" class="form-control" placeholder="Full name" name="Name">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
        <?php echo form_error('Email', '<div class="error text-danger">', '</div>'); ?>
          <input type="email" class="form-control" placeholder="Email" name="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
        
        <?php echo form_error('Password', '<div class="error text-danger">', '</div>'); ?>
          <input type="password" class="form-control" placeholder="Password" name="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
         <?php echo form_error('RePassword', '<div class="error text-danger">', '</div>'); ?>
          <input type="password" class="form-control" placeholder="Retype password" name="RePassword">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
         <?php echo form_error('RePassword', '<div class="error text-danger">', '</div>'); ?>
          <!-- <select [(ngModel)]="model">
            <option *ngFor="let item of list" [value]="item">{{item}}</option>
          </select> -->
         <select class="form-control" name="CusCode">
         <?php 
          		foreach($customer as $row)
              {
                ?>
                  <option value="<?php echo $row->id ;?>"><?php echo $row->CompanyName; ?></option>
                <?php
              }
          ?>
         </select>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-address-book"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree">
              <label for="agreeTerms">
               I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div> -->
          <!-- /.col -->
          <div class="col">
            <button type="submit" class="btn btn-primary btn-block form-control">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!--
      <div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i>
          Sign up using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i>
          Sign up using Google+
        </a>
      </div>
      -->
      <center>
      <a href="login" class="text-center">I already have a membership</a>
      </center>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js'); ?>"></script>
</body>
</html>
