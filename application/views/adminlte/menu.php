<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wealth Oil BackEnd</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" href="<?php echo base_url(''); ?>"> -->

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('asset/adminlte/plugins/fontawesome-free/css/all.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
    <!-- DatePicker -->
    <link rel="stylesheet" href="<?php echo base_url('asset/adminlte/plugins/daterangepicker/daterangepicker.css'); ?>">
    <link rel="stylesheet"
        href="<?php echo base_url('asset/adminlte/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css'); ?>">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'); ?>">
    <link rel="stylesheet"
        href="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css'); ?>">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
        href="<?php echo base_url('asset/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') ?>">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet"
        href="<?php echo base_url('asset/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url('asset/adminlte/plugins/select2/css/select2.min.css'); ?>">
    <link rel="stylesheet"
        href="<?php echo base_url('asset/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css'); ?>">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet"
        href="<?php echo base_url('asset/adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('asset/adminlte/dist/css/adminlte.min.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>


<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="<?php //echo base_url('asset/adminlte/index3.html'); ?>" class="brand-link">
                <img src="<?php echo base_url('asset/adminlte/dist/img/AdminLTELogo.png'); ?>" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">OilStation</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="<?php echo base_url('asset/adminlte/dist/img/user2-160x160.jpg'); ?>"
                            class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">
                            <?php echo $_SESSION['username']; ?></a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <?php // var_dump($permissiom);exit;
?>
                        <?php
$i = 0;
if (isset($menu) && count($menu) > 0) {
 // var_dump($menu);

 foreach ($menu as $key => $mainMenu) {

//    var_dump($role == $mainMenu['permissiom']);

  if (is_null($mainMenu['parent'])) {
   foreach ($_SESSION['key'] as $key2 => $role) {
    if (strtoupper($role->Name) == strtoupper($mainMenu['permissiom']) ) {
     ?>
                        <li
                            class="nav-item has-treeview <?php if ($permissiom == $mainMenu['permissiom']) {echo 'menu-open';}?>">
                            <a href="#"
                                class="nav-link <?php if ($permissiom == $mainMenu['permissiom']) {echo 'active';}?>">
                                <i class="nav-icon <?php echo $mainMenu['icon']; ?>"></i>
                                <p>
                                    <?php echo $mainMenu['name']; ?>
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>

                            <?php
}
   }
  } else {
   ?>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?php echo base_url($mainMenu['link']); ?>"
                                        class="nav-link <?php if ($page == $mainMenu['name']) {echo 'active';}?>">
                                        &nbsp;&nbsp;&nbsp;<i class="<?php echo $mainMenu['icon']; ?>"></i>
                                        <p><?php echo $mainMenu['name']; ?></p>
                                    </a>
                                </li>
                            </ul>
                            <?php

  }

  // echo 'parent= '.$menu[$i+1]['parent']; fas fa-angle-right
  if (isset($menu[$i + 1]) && is_null($menu[$i + 1]['parent'])) {
   ?>
                        </li>
                        <?php
}
  $i++;

 }
}

?>
                        </li>
                        <!--
          <?php
foreach ($menu as $key => $value) {
 if ($value['parent'] == null) {?>
                <a href="#" class="nav-link">
                  <i class="nav-icon <?php echo $value['icon']; ?>"></i>
                  <p><?php echo $value['name'] . '<br/>'; ?>
                    <i class="right fas fa-angle-left"></i>
                  </p>

                </a>
              <?php }?>
              <?php if ($value['parent'] != null) {?>
                <li class="nav-item">
                  <a href="<?php echo base_url('asset/adminlte/index.html'); ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p><?php echo $value['name'] . '<br/>'; ?></p>
                  </a>
              </li>
              <?php }?>
            <?php }?>
              -->
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>