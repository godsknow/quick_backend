<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo $template['link']; ?>">
                                <?php echo $template['parent']; ?>
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <?php echo $template['page']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="col-5">
                            <br>
                            <div class="card card-primary card-outline card-outline-tabs">

                                <div class="card-body">

                                    <form method="POST" action=''>
                                        <div class='input-group text'>
                                            SET KEY :&nbsp;&nbsp;&nbsp;
                                            <input id='addkey' name='useraddkey' type='text' class='form-control ' />
                                            <div class='input-group-append'>
                                                &nbsp;&nbsp;&nbsp;
                                                <button type='submit' class='btn btn-block btn-info'> Submit </button>
                                            </div>
                                        </div>
                                        <br>
                                        <?php foreach ($data['result'] as $key => $value) {?>
                                        <?php
foreach ($value as $key => $value1) {
 if ($key != 'Id') {

  ?>
                                        <table>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" name="keyset[]" value=<?php echo $value1; ?>>
                                                </th>
                                                <th>
                                                <?php echo $value1; ?>
                                                </th>
                                            </tr>
                                        </table>



                                        <?php
} else {
  ?>
                                        <?PHP
}
}
}

?>

                                    </form>
                                </div> <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>

<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js'); ?>"></script>
<!-- page script -->
<script>
$(function() {
    // $("#example1").DataTable({
    //   "responsive": true,
    //   "autoWidth": false,
    //   "searching": false,

    // });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "responsive": true,

    });
});
</script>