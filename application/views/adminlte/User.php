<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo $template['link']; ?>">
                                <?php echo $template['parent']; ?>
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <?php echo $template['page']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="col-12">
                            <div style="width:150px; margin:10px 0px;">
                                <?php if (isset($data['linkadd'])) {?>
                                <a href="<?php echo $data['linkadd']; ?>">
                                    <input type="button" class="form-control btn-success" value="เพิ่มข้อมูลผู้ใช้งาน">
                                </a>
                                <?php } else {?>
                                &nbsp;
                                <?php }?>
                            </div>
                            <div class="card card-primary card-outline card-outline-tabs">
                                <div class="card-body">

                                    <div class="tab-pane fade show active" id="custom-tabs-four-show" role="tabpanel"
                                        aria-labelledby="custom-tabs-four-show-tab">

                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <?php
                                                    foreach ($data['result'][0] as $key => $value) {
                                                        if ($key != 'rowId') 
                                                        {
                                                            ?>
                                                    <th><?php echo $key; ?></th>
                                                    <?php
                                                        } else {
                                                    ?>
                                                    <th>ข้อมูล</th>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                    <!-- <th>Delete</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($data['result'] as $key => $value) { ?>
                                                <tr>
                                                    <?php
                                                        foreach ($value as $key2 => $value2) {
                                                            if ($key2 != 'rowId') {
                                                    ?>
                                                    <td style="vertical-align: middle;"><?php echo $value2; ?></td>
                                                    <?php
                                                            } else {
                                                    ?>
                                                    <td>
                                                        <a href="setboard?id=<?php echo $value2; ?>">
                                                            <a
                                                                href="<?php echo $data['linkedit']; ?>?id=<?php echo $value2; ?>">
                                                                <input type="button" class="form-control btn-warning"
                                                                    value="แก้ไข">
                                                            </a>
                                                            &nbsp;
                                                            <a href="setpassword?id=<?php echo $value2; ?>">
                                                                <input type="button" class="form-control btn-warning"
                                                                    value="ตั่งค่ารหัสผ่านใหม่">
                                                            </a>
                                                    </td>
                                                </tr>
                                            <?PHP
                                                            }
                                                        }
                                                }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                <?php
                                                    foreach ($data['result'][0] as $key => $value) {
                                                        if ($key != 'rowId') {
                                                            ?>
                                                    <th><?php echo $key; ?></th>
                                                <?php
                                                        } else {
                                                ?>
                                                    <th>ข้อมูล</th>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>

<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js'); ?>"></script>
<!-- page script -->
<script>
$(function() {
    // $("#example1").DataTable({
    //   "responsive": true,
    //   "autoWidth": false,
    //   "searching": false,

    // });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "responsive": true,

    });
});
</script>