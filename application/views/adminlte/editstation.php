<?php
$modeEdit = false;
if (isset($data) && !is_null($data)) {
 $modeEdit = true;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">ตู้น้ำมัน</a></li>
                        <li class="breadcrumb-item active">แก้ไขตู้น้ำมัน</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">แก้ไขตู้น้ำมัน</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="setEditStation" method="POST" role="form" onsubmit="return validateForm()"
                            name="AddStation">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>ชื่อตู้น้ำมัน</label>
                                            <input type="text" class="form-control" name="StationName"
                                                value="<?php echo $selectEdit['result'][0]["StationName"]; ?>">
                                            <br>
                                            <label>รหัสเฉพาะ</label>
                                            <input type="text" class="form-control" name="StationCode"
                                                value="<?php echo $selectEdit['result'][0]["StationCode"]; ?>">
                                            <br>
                                            <div class="row">
                                                <div class="col">
                                                    <label>ตำแหน่งสถานที่</label>
                                                    <input type="text" class="form-control" name="LocationName"
                                                        value="<?php echo $selectEdit['result'][0]["LocationName"]; ?>">
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>ปริมาตรของตู้น้ำมัน</label>
                                            <input type="text" class="form-control" name="MaxCapacity"
                                                value="<?php echo $selectEdit['result'][0]["MaxCapacity"]; ?>">
                                            <br>
                                            <label>น้ำมันคงเหลือ</label>
                                            <input type="text" class="form-control" name="Remaining"
                                                value="<?php echo $selectEdit['result'][0]["Remaining"]; ?>">
                                            <br>
                                            <br><br>
                                            <div class="text-center">
                                                <input type="hidden" name="row_id" value="<?php echo $id; ?>">
                                                <input type="hidden" name="LocationId"
                                                    value="<?php echo $selectEdit['result'][0]["LocationId"]; ?>">
                                                <button type="submit" class="form-control btn btn-primary">ตกลง</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="row">
                                <div class="col">

                                </div>
                            </div>
                        </form>

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('asset/adminlte/plugins/select2/js/select2.full.min.js'); ?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js'); ?>">
</script>
<!-- InputMask -->
<script src="<?php echo base_url('asset/adminlte/plugins/moment/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js'); ?>"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>">
</script>
<!-- Tempusdominus Bootstrap 4 -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?>">
</script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
<!-- page script -->
<script>
$(function() {
    // $("#example1").DataTable({
    //     "responsive": true,
    //     "autoWidth": false,
    //     "searching": false,
    // });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});

var latitude = document.getElementById("Latitude");
var longitude = document.getElementById("Longitude");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    // alert(position.coords.latitude);
    longitude.value = position.coords.longitude;
    latitude.value = position.coords.latitude;
    //   x.innerHTML = "Latitude: " + position.coords.latitude +
    //longitude.value = position.coords.longitude;
}

function validateForm() {
    var StationName = document.forms["AddStation"]["StationName"].value;
    var StationCode = document.forms["AddStation"]["StationCode"].value;
    var LocationName = document.forms["AddStation"]["LocationName"].value;
    var Latitude = document.forms["AddStation"]["Latitude"].value;
    var Longitude = document.forms["AddStation"]["Longitude"].value;
    var Ip_Address = document.forms["AddStation"]["Ip_Address"].value;
    var Remaining = document.forms["AddStation"]["Remaining"].value;
    var SectionId = document.forms["AddStation"]["SectionId"].value;
    var AppReleaseVersion = document.forms["AddStation"]["AppReleaseVersion"].value;
    var AppCurrentVersion = document.forms["AddStation"]["AppCurrentVersion"].value;
    var MaxCapacity = document.forms["AddStation"]["MaxCapacity"].value;
    var Balance = document.forms["AddStation"]["Balance"].value;

    if (StationName == "" || StationCode == "" || LocationName == "" || Latitude == "" || Longitude == "" ||
        Ip_Address == "" || Remaining == "" || SectionId == "" || AppReleaseVersion == "" || AppCurrentVersion == "" ||
        MaxCapacity == "" || Balance == "") {
        alert("No Register");
        return false;
    }
}
</script>