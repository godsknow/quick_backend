<?php
$modeEdit = false;
if (isset($data) && !is_null($data)) {
 $modeEdit = true;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">ลูกค้า</a></li>
                        <li class="breadcrumb-item active">เพิ่มข้อมูลลูกค้า</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มข้อมูลลูกค้า</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?php
foreach ($data['result'] as $key => $value) {
 ?>
                        <form action="UpdateCustomer" method="POST" role="form" onsubmit="return validateForm()"
                            name="AddCustomer">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>ชื่อบริษัท</label>
                                            <input type="text" class="form-control" name="CompanyName"
                                                value="<?php echo $value['CompanyName']; ?>">
                                            <br>
                                            <label>ชื่อ</label>
                                            <input type="text" class="form-control" name="FirstName"
                                                value="<?php echo $value['FirstName']; ?>">
                                            <br>
                                            <div class="row">
                                                <div class="col">
                                                    <label>นามสกุล</label>

                                                    <input type="text" class="form-control" name="LastName"
                                                        value="<?php echo $value['LastName']; ?>">

                                                    <br>
                                                </div>
                                            </div>
                                            <label> อีเมล</label>
                                            <input type="Email" class="form-control" name="Email"
                                                value="<?php echo $value['Email']; ?>">
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>โทรศัพท์</label>
                                            <input type="tel" class="form-control" name="Tel"
                                                value="<?php echo $value['Tel']; ?>">
                                            <br>
                                            <label>ชื่อเฉพาะ</label>
                                            <input type="text" class="form-control" name="CusCode"
                                                value="<?php echo $value['CusCode']; ?>">
                                            <br>
                                            <label>ที่อยู่</label>
                                            <input type="text" class="form-control" name="Address1"
                                                value="<?php echo $value['Address1']; ?>">
                                            <br>
                                            <br>
                                            <input type="hidden" class="form-control" name="Id"
                                                value="<?php echo $value['Id']; ?>">
                                            <?php
}
?>
                                            <div class="text-center">
                                                <button type="submit" class="form-control btn btn-primary">ตกลง</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <div class="row">
                                    <div class="col">

                                    </div>
                                </div>
                        </form>

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('asset/adminlte/plugins/select2/js/select2.full.min.js'); ?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js'); ?>">
</script>
<!-- InputMask -->
<script src="<?php echo base_url('asset/adminlte/plugins/moment/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js'); ?>"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>">
</script>
<!-- Tempusdominus Bootstrap 4 -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?>">
</script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
<!-- page script -->
<script>
$(function() {
    // $("#example1").DataTable({
    //     "responsive": true,
    //     "autoWidth": false,
    //     "searching": false,
    // });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});

var latitude = document.getElementById("Latitude");
var longitude = document.getElementById("Longitude");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    // alert(position.coords.latitude);
    longitude.value = position.coords.longitude;
    latitude.value = position.coords.latitude;
    //   x.innerHTML = "Latitude: " + position.coords.latitude +
    //longitude.value = position.coords.longitude;
}

function validateForm() {
    var CompanyName = document.forms["AddCustomer"]["CompanyName"].value;
    var FirstName = document.forms["AddCustomer"]["FirstName"].value;
    var LastName = document.forms["AddCustomer"]["LastName"].value;
    var CusCode = document.forms["AddCustomer"]["CusCode"].value;
    var Address1 = document.forms["AddCustomer"]["Address1"].value;
    var Email = document.forms["AddCustomer"]["Email"].value;
    var Tel = document.forms["AddCustomer"]["Tel"].value;
    var UserName = document.forms["AddCustomer"]["UserName"].value;
    var Password = document.forms["AddCustomer"]["Password"].value;

    if (CompanyName == "" || FirstName == "" || LastName == "" || CusCode == "" || Address1 == "" ||
        Email == "" || Tel == "" || UserName == "" || Password == "") {
        alert("Please fill your information completely.");
        return false;
    }
}
</script>