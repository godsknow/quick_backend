<?php
// var_dump($data);exit();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo $template['link']; ?>">
                                <?php echo $template['parent']; ?>
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <?php echo $template['page']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="col-12">
                            <div style="width:320px; margin:10px 0px;">
                                <form method="POST" action="reportdaily">
                                    <div class='input-group date' id='reservationdate' data-target-input='nearest'>
                                        ค้นหา :&nbsp;&nbsp;&nbsp;
                                        <input id='searchdate' name='searchdate' type='text'
                                            class='form-control datetimepicker-input' data-target='#reservationdate' />
                                        <div class='input-group-append' data-target='#reservationdate'
                                            data-toggle='datetimepicker'>
                                            <div class='input-group-text'><i class='fa fa-calendar'></i></div>
                                            &nbsp;&nbsp;&nbsp;
                                            <button type='submit' class='btn btn-block btn-info'> ค้นหา </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php if (isset($data['linkadd'])) {?>
                            <div style="width:100px; margin:10px 0px;">
                                <a href="<?php echo $data['linkadd']; ?>">
                                    <input type="button" class="form-control btn-primary" value="เพิ่ม">
                                </a>
                            </div>
                            <?php } else {?><?php }?>
                            <div class="card card-primary card-outline card-outline-tabs">
                                <div class="card-body">
                                    <div class="tab-pane fade show active" id="custom-tabs-four-show" role="tabpanel"
                                        aria-labelledby="custom-tabs-four-show-tab">
                                        <?php 
                                        if(isset($data['result']) && !is_null($data['result']) )
                                        {
                                        ?>
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <?php
                                                    foreach ($data['result'][0] as $key => $value) {
                                                        if ($key != 'rowId') 
                                                        {
                                                            ?>
                                                    <th><?php echo $key; ?></th>
                                                    <?php
                                                        } else {
                                                    ?>
                                                    <th>ข้อมูล</th>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                    <!-- <th>Delete</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($data['result'] as $key => $value) { ?>
                                                <tr>
                                                    <?php
                                                        foreach ($value as $key2 => $value2) {
                                                            if ($key2 != 'rowId') {
                                                    ?>
                                                    <td style="vertical-align: middle;"><?php echo $value2; ?></td>
                                                    <?php
                                                            } else {
                                                    ?>
                                                    <td>
                                                        <a href="setboard?id=<?php echo $value2; ?>">
                                                            <a
                                                                href="<?php echo $data['linkedit']; ?>?id=<?php echo $value2; ?>">
                                                                <input type="button" class="form-control btn-warning"
                                                                    value="แก้ไข">
                                                            </a>
                                                            &nbsp;
                                                            <a href="setpassword?id=<?php echo $value2; ?>">
                                                                <input type="button" class="form-control btn-warning"
                                                                    value="รีเซ็ตรหัสผ่าน">
                                                            </a>
                                                    </td>
                                                </tr>
                                                <?PHP
                                                            }
                                                        }
                                                }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <?php
                                                    foreach ($data['result'][0] as $key => $value) {
                                                        if ($key != 'rowId') {
                                                            ?>
                                                    <th><?php echo $key; ?></th>
                                                    <?php
                                                        } else {
                                                ?>
                                                    <th>Action</th>
                                                    <?php
                                                        }
                                                    }
                                                ?>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <?php
                                        }else{
                                        ?>
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Data</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Data not found</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Data</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <?php
                                        } 
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js');?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>

<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js');?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js');?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('asset/adminlte/plugins/select2/js/select2.full.min.js');?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js');?>">
</script>
<!-- InputMask -->
<script src="<?php echo base_url('asset/adminlte/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js');?>"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/daterangepicker/daterangepicker.js');?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js');?>">
</script>
<!-- Tempusdominus Bootstrap 4 -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js');?>">
</script>
<!-- page script -->
<script>
$(function() {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "oLanguage": {
            "sSearch": "ค้นหา : "
        }
    });

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L',
    });
    $('#searchdate').val("<?php echo $template['searchDate']; ?>");

});
</script>