<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo $template['link']; ?>">
                                <?php echo $template['parent']; ?>
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <?php echo $template['page']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">


                    </br>

                    <div class="card card-primary card-outline card-outline-tabs">
                        <div class="card-header p-0 border-bottom-0">

                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-four-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-four-show" role="tabpanel"
                                    aria-labelledby="custom-tabs-four-show-tab">
                                    <div class="col-md-12">
                                        <form method="POST" action="reportMonth">
                                            <div class="form-group">
                                                <div class="row">
                                                    Search:&nbsp;&nbsp;&nbsp;
                                                    <div class="col-md-3">
                                                        <?php
$MonthArray = array(
    "01" => "January", "02" => "February", "03" => "March", "04" => "April",
    "05" => "May", "06" => "June", "07" => "July", "08" => "August",
    "09" => "September", "10" => "October", "11" => "November", "12" => "December",
);
?>
                                                        <select id="month" name="month" class="form-control select2"
                                                            style="width: 100%;">
                                                            <?php

foreach ($MonthArray as $monthNum => $month) {
    $selected = (isset($getMonth) && $getMonth == $monthNum) ? 'selected' : '';
    //Uncomment line below if you want to prefix the month number with leading 0 'Zero'
    //$monthNum = str_pad($monthNum, 2, "0", STR_PAD_LEFT);
    echo '<option ' . $selected . ' value="' . $monthNum . '">' . $month . '</option>';
}

/*for ($i = 0; $i <= 11; ++$i) {
$time = strtotime(sprintf('%d months', $i - 1));
$Monthdecimalvalue = date('m', $time);
$MonthName = date('F', $time);
echo "<option value='$Monthdecimalvalue'>$MonthName</option>";
}*/
?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select id="year" name="year" class="form-control select2"
                                                            style="width: 100%;">
                                                            <?php $year = (int) date('Y');?>
                                                            <option value="<?php echo $year; ?>" selected="true">
                                                                <?php echo $year; ?></option>
                                                            <?php
                                                        for ($year = 2020; $year < date("Y"); $year++) {
                                                            $selected = (isset($getYear) && $getYear == $year) ? 'selected' : '';
                                                            echo "<option value=$year $selected>$year</option>";
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="submit" class="btn btn-block btn-info"> Search
                                                        </button>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                    <table id="example2" class="table table-bordered table-striped">
                                        <?php if (is_null($data)) {?>
                                        <thead>
                                            <tr>
                                                <th style="text-align:center">Data</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="text-align:center">Data not found</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th style="text-align:center">Data</th>
                                            </tr>
                                        </tfoot>
                                        <?php } else {?>
                                        <thead>
                                            <tr>
                                                <?php foreach ($data['result'][0] as $key => $value) {?>
                                                <th><?php echo $key; ?></th>
                                                <?php }?>
                                                <!-- <th>Delete</th> -->
                                            </tr>
                                        </thead>
                                        <tbody><?php foreach ($data['result'] as $key => $value) {?>
                                            <tr>
                                                <?php foreach ($value as $key2 => $value2) {?>
                                                <td><?php echo $value2; ?></td>
                                                <?php }?>
                                            </tr>
                                            <?PHP }?>
                                        </tbody>
                                        <tfoot>
                                            <tr><?php foreach ($data['result'][0] as $key => $value) {?>
                                                <th><?php echo $key; ?></th>
                                                <?php }?>
                                            </tr>
                                        </tfoot>
                                        <?php }?>
                                    </table>
                                </div>
                               
                            </div>
                        </div>


                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('asset/adminlte/plugins/select2/js/select2.full.min.js'); ?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js'); ?>">
</script>
<!-- InputMask -->
<script src="<?php echo base_url('asset/adminlte/plugins/moment/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js'); ?>"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>">
</script>
<!-- Tempusdominus Bootstrap 4 -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?>">
</script>
<!-- page script -->
<script>
$(function() {
    $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
        "searching": false,

    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });



});

$(function() {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
    })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', {
        'placeholder': 'mm/dd/yyyy'
    })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY hh:mm A'
        }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month')
                    .endOf('month')
                ]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
        },
        function(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
        format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
        $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function() {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
})



$('#year').val("<?php echo $template['year']; ?>");
$('#month').val("<?php echo $template['month']; ?>");
</script>