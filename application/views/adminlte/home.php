<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $template['header'];?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                <a href="<?php echo $template['link'];?>">
                  <?php echo $template['parent'];?>
                </a>
              </li>
              <li class="breadcrumb-item active">
                <?php echo $template['page'];?>
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div id = "card" class="small-box bg-danger">
              <div class="inner">
                <h3 id="invoice">
                  0 บาท
                      <!-- <i class="fas fa-arrow-up"></i> -->
                </h3>  
                <p>ยอดขายภายใน 1 เดือน</p>
              </div>
              <div class="icon">
                <i class="fas fa-money-bill-wave"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div id = "card" class="small-box bg-warning">
              <div class="inner">
                <h3 id = "showCapacity"> 
                    0 บาท
                </h3>
                <p>ปริมาณน้ำมันที่ขายไปภายใน 1 เดือน</p>
              </div>
              <div class="icon">
                <i class="fas fa-sort-numeric-up-alt"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div id = "card" class="small-box bg-info">
              <div class="inner">
                <h3 id = "showoilstations">0 ตู้</h3>
                <p>จำนวนตู้น้ำมัน</p>
              </div>
              <div class="icon">
                <i class="fas fa-database"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
        </div>
       
        <div class="row">
          <!-- Left col -->
          <div  class="col-9"  >
            <div  class="container-fluid">
            <iframe src="http://grafana.wealthvending.co.th/d-solo/sYTodLZGz/stations?orgId=1&from=1594691908411&to=1594713508411&theme=light&panelId=2" width="100%" height="340" frameborder="0"></iframe>                   
              </div>
            </div>
            <!-- /.card -->
          <div class="col-3">
            <div class="col-12">
            <!-- small box -->
            <div id = "card" class="small-box bg-success" style=" height: 160px;">
              <div class="inner">
                <p>จำนวนตู้น้ำมัน (online)</p>
                <!-- <div class = "row"> -->
                <h3 id = "demo"  style="font-size:40px ;"> 0 ตู้</h3> 
                <!-- <h3>ตู้</h3>
                </div> -->
                <!-- <h3 id = "demo"> </h3> -->
                <div class="icon" style = "position: absolute; bottom: 110px; right: 10px;" >
                <i class="fas fa-database"></i>
                </div>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
              <!-- /.card-body -->
            </div>
            <!-- การ์ดตู้น้ำมันออนไลน์ -->
            <div class="col-12">
            <!-- small box -->
            <div id = "card" class="small-box bg-primary" style=" height: 160px;">
              <div class="inner">
                <p>จำนวนตู้น้ำมัน (offline)</p>
                <h3 class = "quotes" id = "demo2" style="font-size:40px ;">
                0 ตู้
                </h3>
                <div class="icon" style = "position: absolute; bottom: 110px; right: 10px;" >
                <i class="fas fa-database"></i>
                </div>
              </div>  
               <!-- การ์ดตู้น้ำมันออฟไลน์ -->
            </div>
          </div>
            <!-- <div id = "testchart"> </div> -->
            <!-- /.card-body -->
         </div>
        </div>
        
          <!-- Left col -->
          <div class="col-12" >
            <div id="columnChart" style="height: 500px; width: 100%;"></div>
          </div>
        
        
        <div class="col-12">
         <!-- <div id = "testchart"> </div> -->
         <div id="chartContainer" style="height: 300px; width: 100%;"></div>
        </div>

        <div class="col-12">
         <!-- <div id = "testchart"> </div> -->
         <div id="chartContainerAday" style="height: 300px; width: 100%;"></div>
        </div>

    </section>

   

    <!-- Main content -->
    <!-- โค้ดเก่าดึง Grafana -->
    <!-- <section class="content">
      <div class="container-fluid">
        <iframe src="http://grafana.wealthvending.co.th/d/swWzOYZGk/invoice?orgId=1&from=1594691514977&to=1594713114977&theme=light&kiosk" width="100%" height="600" frameborder="0"></iframe>
    </section> -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <?php
        $dataPoints2 = array();
        $d1 = array();
        $d2 = array();
        $d3 = array();
        $d4 = array();
      //   foreach($lineChart as $row){
      //   array_push($dataPoints2, array("y"=> $row->value, "label"=> $row->time));
      // }
      $nameLine = array();
        $t1 = 'WVS001';
        $t2 = 'WVS002';
        $t3 = 'WVS003';
        $t4 = 'WVS004';
        array_push($nameLine,$t1,$t2,$t3,$t4);
        foreach ($lineChart as $row) {
                if( $row->CODE == $t1){
                  array_push($d1, array("y"=> $row->value, "label"=> $row->time));   
                }else  if( $row->CODE == $t2){
                  array_push($d2, array("y"=> $row->value, "label"=> $row->time));   
                }else  if( $row->CODE == $t3){
                  array_push($d3, array("y"=> $row->value, "label"=> $row->time));   
                }else  if( $row->CODE == $t4){
                  array_push($d4, array("y"=> $row->value, "label"=> $row->time));   
                }
          }
          array_push($dataPoints2,$d1,$d2,$d3,$d4);     
    
    // var_dump($dataPoints2);exit();

    $dataPoints = array();
    foreach($columnChart as $row){
        array_push($dataPoints, array("y"=> $row->Amount, "label"=> $row->numDay));
    }

    $dataDay = array();
    $i=-1;
    foreach($lineChartDay as $row){
      $i++;
      array_push($dataDay, array("y"=> $row->value));
    }
    //var_dump($dataPoints2);exit();

    ?> 

  <!-- jQuery -->
  <script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js');?>"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <!-- DataTables -->
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
  <script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js');?>"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url('asset/adminlte/dist/js/demo.js');?>"></script>

  <script>
   // set var
  function body(){
      var c1 = 1 ;
      var res3;
      var cShowonline = 1;
      var cShowoffline = 1;
      var cSumcapacity = 1;
      var cCountoilstation = 1;
      var  res , res1 ,  res2 , res4;
  //animate count
   $.myjQueryCount = function(res , _id , unit) {
    $(function () {
        var fx = function fx() {
          //var res = 10;
          var dfd = $(_id).map(function (i, el) {
              var props = {
                "from": {
                    "count": 0
                },
                "to": {
                    "count": res
                }
              };
            return $(props.from).animate(props.to, {
                duration: 1000 * 1,
                step: function (now, fx) {
                    $(el).text(Math.ceil(now)+ unit);
                },
                complete: function() {
                   $(el).text(addCommas(res) + unit);
                }
            }).promise();
        }).toArray();
            return $.when.apply($, dfd);
        };
        fx()
       });
         };
  //show oline machine
  function showonlineMachine() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if( cShowonline == 1){
        res = this.responseText;
        $.myjQueryCount(res ,"#demo" ," ตู้");
        cShowonline++;
      }else if (res != this.responseText){
        res = this.responseText;
        $.myjQueryCount(res , "#demo" ," ตู้");
      }else if (res == this.responseText){
        document.getElementById("demo").innerHTML = this.responseText + " ตู้";
      }
      cShowonline++;
    }
  };
      xmlhttp.open("GET", "../ajax/ajexcountonline", true);
      xmlhttp.send();
  }
  //show offline machine
  function showofflineMachine() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if(cShowoffline == 1){
        res1 = this.responseText;
        $.myjQueryCount(res1 ,"#demo2" ," ตู้");
        cShowoffline++;
      }else if (res1 != this.responseText){
        res1= this.responseText;
        $.myjQueryCount(res1 , "#demo2" ," ตู้");
      }else if (res1 == this.responseText){
        document.getElementById("demo2").innerHTML = this.responseText + " ตู้" ;
      }
    }
      };
      xmlhttp.open("GET", "../ajax/ajexcountoffline", true);
      xmlhttp.send();
  }
  function sumCapacityOneMonth() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if(cSumcapacity  == 1){
        res2 = this.responseText;
        $.myjQueryCount(parseFloat(res2).toFixed(2) , "#showCapacity" , " บาท");
        cSumcapacity ++;
      }else if (res2 != this.responseText){
        res2 = this.responseText;
        $.myjQueryCount(parseFloat(res2).toFixed(2) , "#showCapacity" , " บาท");
      }else if (res2 == this.responseText){
        var num = parseFloat(this.responseText);
        var e = num.toFixed(2);
        document.getElementById("showCapacity").innerHTML =  addCommas(e) + " บาท";
      }
    }
      };
      xmlhttp.open("GET", "../ajax/sumCapacityOneMonth", true);
      xmlhttp.send();
  }
  function countNumOilStation() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if(cCountoilstation  == 1){
        res4 = this.responseText;
        $.myjQueryCount(res4 , "#showoilstations" , " ตู้");
        cCountoilstation ++;
      }else if (res4!= this.responseText){
        res4 = this.responseText;
        $.myjQueryCount(res4 , "#showoilstations" , " ตู้");
      }else if (res4 == this.responseText){
        document.getElementById("showoilstations").innerHTML = this.responseText + " ตู้";
      }
    }
      };
      xmlhttp.open("GET", "../ajax/countNumOilStation", true);
      xmlhttp.send();
  }
  function showHint() {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          if(c1 == 1){
        res3 = this.responseText;
        // document.getElementById("invoice").innerHTML = this.responseText;
        $.myjQueryCount(parseFloat(res3).toFixed(2) , "#invoice" , " บาท");
        c1++;
      }else if (res3 != this.responseText){
        res3 = this.responseText;
        $.myjQueryCount(parseFloat(res3).toFixed(2) , "#invoice" , " บาท");
      }else if (res3 == this.responseText){
        var num = parseFloat(this.responseText);
        var e = num.toFixed(2);
        document.getElementById("invoice").innerHTML =  addCommas(e) + " บาท";
        // document.getElementById("invoice").innerHTML = this.responseText;
      }
        }
      };
      xmlhttp.open("GET", "../ajax/sumValueOneMonth", true);
      xmlhttp.send();
    }

  window.setInterval(callAllfn, 1000);
  function callAllfn(){
    countNumOilStation();
    sumCapacityOneMonth();
    showofflineMachine();
    showonlineMachine();
    showHint();
  }
  function addCommas(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
  }
   }
   //next step
   $i = 0 ;
   $data1= [];
   $linechartday = [];  
   $d=[];
   $n = 29;
  function lineGraph() {
      var jArrayname= <?php echo json_encode($nameLine); ?>;
      var jArray= <?php echo json_encode($dataPoints2); ?>;
              for ( var i = 0 ; i<jArray.length; i++){
                $data1.push({
                        type: "spline",
                        name: jArrayname[i]+'',
		                    showInLegend: true,
                        xValueFormatString: "DD MMM",
                        yValueFormatString: "฿##0.00",
                        dataPoints: jArray[i] ,
                      });
                }
            var options = {
              title: {
                text: "แสดงยอดขายรายเดือน"
              },
              axisX:{
                    title: "วันที่",
                    valueFormatString: "DD MMM",
                    crosshair: {
                        enabled: true,
                        snapToDataPoint: true,
                        labelFormatter: function(e) {
                          //return "฿ " + CanvasJS.formatNumber(e.value, "##0.00");
                        }
                      }
                  },
              axisY: {
                    title: "ยอดขาย (บาท)",
                    valueFormatString: "฿ ##0.00",
                  },
              animationEnabled: true,
              exportEnabled: true,
              data: $data1,
            }
       $("#chartContainer").CanvasJSChart(options);
   }

   function columnChart() {
    var chart = new CanvasJS.Chart("columnChart", {
      animationEnabled: true,
      exportEnabled: true,
      theme: "light1", // "light1", "light2", "dark1", "dark2"
      title:{
        text: "Account Invoice"
      },
      axisY: {
        title: "ยอดขาย(บาท)",
        //includeZero: false,
      },
      data: [{        
        //type: "column",  
        showInLegend: true, 
        //legendMarkerColor: "grey",
        legendText: "รายได้ต่อวัน",
        indexLabel: "฿ {y}",
        indexLabelPlacement: "inside",
        indexLabelFontWeight: "bolder",
        indexLabelFontColor: "white",
        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK);?>
      }]
    });
    chart.render();
  }
  function Graphfor_Aday() {
    
    $d.push(
         {
          x:$i, 
          y:Math.floor(Math.random() * 10),
        }
        );

                // $linechartday.push({
                //         type: "spline",
                //         name: '',
		            //         showInLegend: true,
                //         dataPoints: $d,
                //       });

            var options = {
              title: {
                text: "แสดงยอดขายรายวัน"
              },
              axisX:{
                    title: "เวลา",
                  },
              axisY: {
                    title: "ยอดขาย (บาท)",
                  },
              animationEnabled: true,
              exportEnabled: true,
              data:[{
                type: "spline",
                        name: '',
		                    showInLegend: true,
                        dataPoints: $d,
              }],
            }
            setTimeout(Graphfor_Aday, 6000);
       $("#chartContainerAday").CanvasJSChart(options);
       $i++;
   }
   //window.setInterval(Graphfor_Aday, 6000);
   function getData_Graph(){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        alert(this.responseText);
    }
      };
      xmlhttp.open("GET", "../ajax/ajexgetdataLine", true);
      xmlhttp.send();
   }
  window.onload = function () {
    body();
    lineGraph();
    columnChart();
    Graphfor_Aday();
    //getData_Graph();
    } 
  </script>
  <style>
  #card:hover {
  transform: scale(0.97, 0.97);
  box-shadow: 2px 2px 10px 10px rgba(0,0,0,0.1), 
    -5px -5px 30px 15px rgba(0,0,0,0.1);
  }
  </style>

<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery-ui.1.11.2.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<link href="https://canvasjs.com/assets/css/jquery-ui.1.11.2.min.css" rel="stylesheet" />
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

 