<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header'];?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo $template['link'];?>">
                                <?php echo $template['parent'];?>
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <?php echo $template['page'];?>
                        </li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="col-12">



                            <!-- <div class="card-header p-0 border-bottom-0">  -->
                            <!-- <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-four-show-tab" data-toggle="pill" href="#custom-tabs-four-show" role="tab" aria-controls="custom-tabs-four-show" aria-selected="true">Show</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-four-create-tab" data-toggle="pill" href="#custom-tabs-four-create" role="tab" aria-controls="custom-tabs-four-create" aria-selected="false">Create</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-four-edit-tab" data-toggle="pill" href="#custom-tabs-four-edit" role="tab" aria-controls="custom-tabs-four-edit" aria-selected="false">Edit</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-four-delete-tab" data-toggle="pill" href="#custom-tabs-four-delete" role="tab" aria-controls="custom-tabs-four-delete" aria-selected="false">Delete</a>
                      </li>
                    </ul> -->
                            <!-- </div> -->
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade show active" id="custom-tabs-four-show" role="tabpanel"
                                        aria-labelledby="custom-tabs-four-show-tab">
                                        <?php 
                                        if(isset($data['result']) && !is_null($data['result']) )
                                        {
                                        ?>
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <?php 
                                foreach ($data['result'][0] as $key => $value) {
                                  if($key <> 'rowId'){
                              ?>
                                                    <th><?php echo $key; ?></th>
                                                    <?php
                                  } else{
                              ?>
                                                    <th>ข้อมูล</th>
                                                    <?php
                                  }
                                }
                              ?>
                                                    <!-- <th>Delete</th> -->
                                                </tr>
                                            </thead>
                                            <tbody><?php foreach ($data['result'] as $key => $value) { ?>
                                                <tr>
                                                    <?php 
                              foreach ($value as $key2 => $value2) { 
                                if($key2 <> 'rowId'){
                            ?>
                                                    <td style="vertical-align: middle;"><?php echo $value2;?></td>
                                                    <?php
                                  } else {
                              ?>
                                                    <td>
                                                        <a href="setcost?id=<?php echo $value2;?>">
                                                            <input type="button" class="form-control btn-warning"
                                                                value="แก้ไข">
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?PHP 
                                  } 
                                }
                              }
                            ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <?php 
                                foreach ($data['result'][0] as $key => $value) {
                                  if($key <> 'rowId'){
                              ?>
                                                    <th><?php echo $key; ?></th>
                                                    <?php
                                  } else{
                              ?>
                                                    <th>ข้อมูล</th>
                                                    <?php
                                  }
                                }
                              ?>
                                                </tr>
                                            </tfoot>
                                        </table> <?php
                                        }else{
                                        ?>
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>ข้อมูล</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>ไม่มีข้อมูล</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>ข้อมูล</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <?php
                                        } 
                                        ?>
                                    </div>
                                    <!-- <div class="tab-pane fade" id="custom-tabs-four-create" role="tabpanel" aria-labelledby="custom-tabs-four-create-tab">
                          Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut ligula tempor, et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas sollicitudin, nisi a luctus interdum, nisl ligula placerat mi, quis posuere purus ligula eu lectus. Donec nunc tellus, elementum sit amet ultricies at, posuere nec nunc. Nunc euismod pellentesque diam. 
                      </div>
                      <div class="tab-pane fade" id="custom-tabs-four-edit" role="tabpanel" aria-labelledby="custom-tabs-four-edit-tab">
                        Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna. 
                      </div>
                      <div class="tab-pane fade" id="custom-tabs-four-delete" role="tabpanel" aria-labelledby="custom-tabs-four-delete-tab">
                        Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis. 
                      </div> -->

                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js');?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>

<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js');?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js');?>"></script>
<!-- page script -->
<script>
$(function() {
    // $("#example1").DataTable({
    //   "responsive": true,
    //   "autoWidth": false,
    //   "searching": false,

    // });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,

    });
});
</script>