<?php
$modeEdit = false;
if (isset($data) && !is_null($data)) {
    $modeEdit = true;
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo $template['header']; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">ผู้ใช้งาน</a></li>
                        <li class="breadcrumb-item active">ตั้งค่าข้อมูลผู้ใช้งาน</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">อัพเดทข้อมูลผู้ใช้งาน</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="editkey" method="POST" role="form" name="AddStation">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="col-12">
                                                <div class="card-body">
                                                    <label for="keyname">KeyGroupName</label>

                                                    <input type="text" class="form-control" name="keyname" id="keyname"
                                                        value=<?php echo $data['result'][0]['groupname']; ?>>

                                                    <input type="text" class="form-control" name="groupkey"
                                                        id="groupkey" value=<?php echo $_GET['id']; ?>
                                                        style="display: none">

                                                    <br>
                                                    <?php foreach ($data['key'] as $key => $value) {?>
                                                    <div style="width:180px; float:left; ">
                                                        <label>
                                                            <input type="checkbox"
                                                                id="<?php echo 'key[' . $value['rowId'] . ']'; ?>"
                                                                name="<?php echo 'key[' . $value['rowId'] . ']'; ?>"
                                                                value="1" <?php

    if (in_array($value['Name'], $data['result']['key'])) {
        ?> checked="checked" <?php
}
    ?> <?php

    if ($value['Name'] == 'Home') {
        ?> onclick="this.checked=!this.checked;" <?php
}
    ?>>
                                                            <?php echo $value['Name']; ?>
                                                        </label>
                                                    </div>
                                                    <?php }?>

                                                </div>
                                                <div>
                                                    <button type="submit"
                                                        class="form-control btn btn-success">Submit</button>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                        </form>

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('asset/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('asset/adminlte/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>">
</script>
<script src="<?php echo base_url('asset/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>">
</script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('asset/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('asset/adminlte/dist/js/demo.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('asset/adminlte/plugins/select2/js/select2.full.min.js'); ?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js'); ?>">
</script>
<!-- InputMask -->
<script src="<?php echo base_url('asset/adminlte/plugins/moment/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js'); ?>"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>">
</script>
<!-- Tempusdominus Bootstrap 4 -->
<script
    src="<?php echo base_url('asset/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?>">
</script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url('asset/adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
<!-- page script -->
<script>
$(function() {
    // $("#example1").DataTable({
    //     "responsive": true,
    //     "autoWidth": false,
    //     "searching": false,
    // });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});

var latitude = document.getElementById("Latitude");
var longitude = document.getElementById("Longitude");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    // alert(position.coords.latitude);
    longitude.value = position.coords.longitude;
    latitude.value = position.coords.latitude;
    //   x.innerHTML = "Latitude: " + position.coords.latitude +
    //longitude.value = position.coords.longitude;
}
</script>