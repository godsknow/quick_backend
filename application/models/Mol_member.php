<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_member extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happywallet', false, true );
    }

    public function selectMemberAll( array $data = null )
 {
     $this->load->database( 'happywallet', false, true );
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset']   = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }
            }
        }

        $this->db->select( 'WLT.TelNumber as เบอร์โทรศัพท์,WLT.Value as เงินคงเหลือ,WLT.CustomerId as ลูกค้า,WLT.TelNumber as rowId,' );
        // $this->db->select( 'WLT.id AS "rowId"' );
        $this->db->from( 'Wallet WLT' );
        $this->db->where( 'WLT.DeleteFlag =', 0 );
        $this->db->order_by( 'WLT.id', 'ASC' );

        $sum_row = clone $this->db;
        $rowAll  = $sum_row->count_all_results();

        if ( $rowAll > 0 ) 
 {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows  = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

}