<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_loglogin extends CI_Model
 {

    private $result = array();

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    //Select All data for APi

    public function AddLogLogIn( array $data = null ) {
        $res = false;
        if ( $this->db->insert( 'LogLogIn', $data ) ) {
            $res = true;
        }
        return $res;
    }

    public function GetLogLoginByToken( $data ) {
        $this->db->select( '*' );
        $this->db->from( 'LogLogin' );
        $this->db->where( 'UserId = '.$data['userid'].'OR RouteManId ='.$data['routemanid'].'AND UserName ='.$data['username'] );
        $this->db->where( 'DeleteFlag = 0' );
        $this->db->order_by( 'Id', 'DESC' );

        $query = $this->db->get();

        return $query->result();
    }

    public function GetLogLoginOld( $data ) {
        $this->db->select( '*' );
        $this->db->from( 'LogLogin' );
        $this->db->where( 'CustomerId = '.$data['New'][0]->CustomerId.'AND UserName = '.$data['New'][0]->UserName );
        $this->db->where( 'DeleteFlag = 0' );
        $this->db->order_by( 'Id', 'DESC' );

        $query = $this->db->get();

        return $query->result();
    }

    public function DeleteLogLogin( $data ) {
        $res = false;
        $strQuery = 'UPDATE LogLogin SET DeleteFlag = 0 , UpdateDate = \''.date( 'Y-m-d H:i:s' ).'\' WHERE Id = '.$data->Id;
        if ( $this->db->query( $strQuery ) ) {
            $res = true;
        }
        return $res;
    }

}