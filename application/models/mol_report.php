<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_report extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        //$this->load->database( 'happystation', FALSE, TRUE );
        //echo '<br/>testdb'.$this->load->db_connect( TRUE );
        $db_obj = $this->load->database( 'happystation', false );
        //$this->db_connect( TRUE );
        //if ( $db_obj->conn_id ) {
        //do something
        //} else {
        //echo 'Unable to connect with database with given db details.';
        //}
    }

    public function selectAccountInvoice( array $data = null )
 {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                    // var_dump( $arrData[$key] );
                }
            }
        }
        $this->db->select( 'iv.id,iv.InvoiceNo as รหัสใบเสร็จ,iv.Qty as ปริมาตร,iv.Amount as เงินรวม' );
        $this->db->select( 'DATE_FORMAT(`iv`.`PaymentedDate`,\'%Y-%m-%d\') as วันที่จ่าย' );
        $this->db->from( 'AccountInvoice iv' );
        $this->db->order_by( 'iv.ID', 'ASC' );
        $sum_row = clone $this->db;
        //$rowAll = $sum_row->count_all_results();
        $rowAll = 1;

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            //echo 'sql ='.$this->db->last_query();
            //var_dump( $query );

            //$rows = $query->num_rows();

            if ( $query->result_array() > 0 ) {
                $i = 0;
                foreach ( $query->result_array() as $row ) {
                    $result['result'][$i] = $row;
                    $i++;
                }
            }
        }
        return $result;
    }

    public function selectAccountInvoiceMonth( array $data = null )
 {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                    // $this->db->where( $key ) >= ( $value )and( $key ) <= ( $value );

                } else {
                    $arrData[$key] = $value;
                    // var_dump( $arrData[$key] );
                }
            }
        }
        $this->db->select( 'iv.Id,iv.InvoiceNo,CAST(iv.Qty as float) as Qty,CAST(iv.Amount as float) as Amount
        ,DATE_FORMAT( iv.PaymentedDate, \'%m/%d/%Y\') as PaymentedDate' );
        $this->db->from( 'AccountInvoice AS iv' );

        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();
        // echo '<br/>sql='.$this->db->last_query();
        $this->db->order_by( 'iv.ID', 'ASC' );

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }
        return $result;
    }
}