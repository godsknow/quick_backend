<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mol_stockmove extends CI_Model
{

 public function __construct()
 {
  parent::__construct();
  // $this->load->database();
  $this->load->database('happystation', false, true);
 }

 //Select All data

 public function SelectStockmMoveAll()
 {

  $this->db->select('SM.Id,ST.Name as Status,STT.StationCode as Station,SM.Direction');
  $this->db->select('SL.Name as Location,SM.Capacity,OT.Name as Oil,SM.Description,MC.Name as Machine');
  $this->db->from('StockMove as SM');
  $this->db->join('Status as ST ', 'ST.Id = SM.StatusId', 'Left');
  $this->db->join('Station as STT', ' STT.Id = SM.StationId', 'Left');
  $this->db->join('OilType AS OT ', ' OT.Id = SM.OilTypeId', 'Left');
  $this->db->join('Machine as MC ', ' MC.Id = SM.MachineId', 'Left');
  $this->db->join('StationLocation as SL', 'SL.Id = STT.LocationId', 'Left');
  $this->db->order_by('SM.Id ASC');
  $query = $this->db->get();
  // echo $this->db->last_query();
  // exit();
  // print_r( $query );
  // var_dump( $query->result() );
  // exit();
  return $query->result();
 }

 public function SelectStockmMove($data)
 {

  $this->db->select('SM.Id,ST.Name as Status,STT.StationCode as Station,SM.Direction,SL.Name as Location');
  $this->db->select('SL.Name as Location,SM.Capacity,OT.Name as Oil,SM.Description,MC.Name as Machine');
  $this->db->from('StockMove as SM');
  $this->db->join('Status as ST ', 'ST.Id = SM.StatusId', 'Left');
  $this->db->join('Station as STT', ' STT.Id = SM.StationId', 'Left');
  $this->db->join('OilType AS OT ', ' OT.Id = SM.OilTypeId', 'Left');
  $this->db->join('Machine as MC ', ' MC.Id = SM.MachineId', 'Left');
  $this->db->join('StationLocation as SL', 'SL.Id = ST.LocationId', 'Left');
  $this->db->where('SM.Id', $data);
  $this->db->order_by('SM.Id ASC');
  $query = $this->db->get();
  // print_r( $query );
  // var_dump( $query->result() );
  // exit();
  return $query->result();
 }

 public function InsertStockMove(array $data = null)
 {
  $res = false;
  // var_dump( $data );
  // exit;
  if ($this->db->insert('StockMove', $data)) {
   $res = true;
  }

  return $res;
 }

 public function SelectStockOne()
 {
  $strSQL = 'select  sm.id,sm.CreateDate,st.Name as StationName,mc.Name as MachineName,mc.Remaining from StockMove sm
       JOIN Station st ON st.Id = sm.StationId
       JOIN Machine mc ON mc.StationId = st.Id
       Where mc.StatusId = 1
       order by CreateDate desc Limit 1';
  $data = $this->db->query($strSQL);

  return $data->result();
 }

 public function GetStock(){

  $this->db->select('sm.Id as ลำดับ,sta.Name as สถานะ,st.Name as ชื่อตู้น้ำมัน,ot.Name as ชื่อน้ำมัน , mc.Name as ชื่อเครื่อง , mc.BoardNumber as หมายเลขเครื่อง, sm.Direction as ทิศทางน้ำมัน,sm.Capacity as ปริมาตร,sm.Description as รายละเอียด');
  $this->db->from('StockMove sm');
  $this->db->join('OilType ot ', 'ot.Id = sm.OilTypeId');
  $this->db->join('Station st ', 'st.Id = sm.StationId');
  $this->db->join('Machine mc ', 'mc.Id = sm.MachineId');
  $this->db->join('Status sta  ', 'sta.Id = sm.StatusId');

  $query = $this->db->get();
  $rows = $query->num_rows();
  if ($rows > 0) {
   for ($i = 0; $i < $rows; $i++) {
    $result['result'][$i] = $query->row_array($i);
   }
  }

  return $result;

 }
 public function GetStockByDate(array $data = null){
     $result = null;
     $arrData['duration'] = 1;
     $arrData['offset'] = 0;

     if ( !is_null( $data ) ) {
         foreach ( $data as $key => $value ) {
             if ( $key != 'duration' && $key != 'offset' ) {
                 $this->db->where( $key, $value );
             } else {
                 $arrData[$key] = $value;
                 // var_dump( $arrData[$key] );
             }
         }
     }
     
     $this->db->select('sm.Id as ลำดับ,sta.Name as สถานะ,st.Name as ชื่อตู้น้ำมัน,ot.Name as ชื่อน้ำมัน , mc.Name as ชื่อเครื่อง , mc.BoardNumber as หมายเลขเครื่อง, sm.Direction as ทิศทางน้ำมัน,sm.Capacity as ปริมาตร,sm.Description as รายละเอียด');
     $this->db->from('StockMove sm');
     $this->db->join('OilType ot ', 'ot.Id = sm.OilTypeId');
     $this->db->join('Station st ', 'st.Id = sm.StationId');
     $this->db->join('Machine mc ', 'mc.Id = sm.MachineId');
     $this->db->join('Status sta  ', 'sta.Id = sm.StatusId');

     $sum_row = clone $this->db;
        //$rowAll = $sum_row->count_all_results();
        $rowAll = 1;

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            //echo 'sql ='.$this->db->last_query();
            //var_dump( $query );

            //$rows = $query->num_rows();

            if ( $query->result_array() > 0 ) {
                $i = 0;
                foreach ( $query->result_array() as $row ) {
                    $result['result'][$i] = $row;
                    $i++;
                }
            }
        }
        return $result;
 }

}