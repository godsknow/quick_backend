<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Mol_noti extends CI_Model {

    function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', FALSE, TRUE );
    }

    //Select All data

    public function AddNoti( $data ) {
        // var_dump( $data );
        // exit;
        $res = false;
        $arr = array(
            'CreateDate' => date( 'Y-m-d H:i:s' ),
            'CreateUserId' => 1,
            'Deleteflag' => 1,
            'Ordering' => 0,
            'StatusId' => 1,
            'Title' => $data['title'],
            'Message' =>  $data['message'],
            'To' => $data['to'],
            'Complete' => $data['complete'],
        );

        if ( $this->db->insert( 'Notification', $arr ) ) {
            $res = true;
        }
        return $res;
    }

    public function GetNotiByRouteManId( $data ) {

        $this->db->select( '*' );
        $this->db->from( 'Notification' );
        $this->db->where( ' DeleteFlag = 0' );
        $this->db->where( 'SendToId', $data );
        $this->db->where( 'ToStatus = \'RouteMan\'' );
        $this->db->order_by( 'Id', 'DESC' );
        $query = $this->db->get();

        return $query->result();
    }

    public function GetNoti( $data ) {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                    // var_dump( $arrData[$key] );
                }
            }
        }
        $this->db->select( 'Id as ไอดี,Title as หัวข้อ,Message as ข้อความ,To as ถึง,SendToId as ส่งไปทีไอดี,ToStatus as สถานะคนที่จะส่ง,Complete as สถานะการส่ง' );
        $this->db->from( 'Notification' );

        $sum_row = clone $this->db;
        //$rowAll = $sum_row->count_all_results();
        $rowAll = 1;

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            //echo 'sql ='.$this->db->last_query();
            //var_dump( $query );

            //$rows = $query->num_rows();

            if ( $query->result_array() > 0 ) {
                $i = 0;
                foreach ( $query->result_array() as $row ) {
                    $result['result'][$i] = $row;
                    $i++;
                }
            }
        }
        return $result;

    }
}
?>