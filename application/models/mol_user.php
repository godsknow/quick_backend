<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_user extends CI_Model
 {

    private $result = array();

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    //Select All data for APi

    public function GetUser( $data )
 {

        $this->db->select( 'Id,customerId,username,password' );
        $this->db->from( 'Users' );
        $this->db->where( 'UPPER(Username)', $data );
        $query = $this->db->get();
        // echo 'sql = '.$this->db->last_query();

        // $rows = $query->num_rows();

        // if ( $rows > 0 ) {
        //  for ( $i = 0; $i < $rows; $i++ ) {
        //   $result['result'][$i] = $query->row_array( $i );
        //  }
        // }
        // $d = $query->result();
        // var_dump( $query->result() );
        // echo '\n\n row1 ='.$result->row_array( 0 );
        // exit();
        return $query->result();
    }

    //Select All data for backend

    public function GetLogin( $data )
 {

        $this->db->select( 'username,password,customerid,Id' );
        $this->db->from( 'Users' );
        $this->db->where( 'UPPER(Username)', $data['username'] );
        $query = $this->db->get();
        // $rows = intval( $query->num_rows() );

        // if ( $rows > 0 ) {
        //     for ( $i = 0; $i < $rows; $i++ ) {
        //         $result['result'][$i] = $query->row_array( $i );
        //     }
        // }
        return $query->result();
    }

    public function InsertUser( $data )
    {

        $t = $this->db->insert( 'Users', $data );
        if ( isset( $t ) ) {
            return true;
        } else {
            return false;
        }

    }

    public function UpdatePasswordUser( $data ) {
        $res = false;
        $strSQL = 'UPDATE Users SET Password = N\''.$data['enpass'].'\' WHERE UserName = N\''.$data['username'].'\'';
        if ( $this->db->query( $strSQL ) ) {
            $res = true;
        }
        return $res ;
    }

    public function GetTokenByUserName() {
        $this->db->select( 'token' );
        $this->db->from( 'Users' );
        $this->db->where();
    }

    public function UpdateTokenByUserName( $data ) {
        if ( $data == null ) {
            throw new Exception( 'DATA IS NULL', 1 );
        }
        $res = false;
        $strQuery = 'UPDATE USERS SET Token = N\''.$data['Token'].'\' Where Username = N\''.$data['Username'].'\'';
        if ( $this->db->query( $strQuery ) ) {
            $res = true;
        }
        return $res;
    }

    //user list
    public function selectUser( array $data = null )
    {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset']   = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }

        $this->db->select( 'sl.Id,sl.Name as LocationName,sl.Latitude,sl.Longitude,st.StationCode,mc.BoardNumber' );
        $this->db->from( 'StationLocation sl' );
        $this->db->join( 'Station st', 'st.LocationId = sl.Id' );
        $this->db->join( 'Machine mc', 'mc.StationId = st.Id' );
        $this->db->order_by( 'Id', 'ASC' );

        $sum_row = clone $this->db;
        $rowAll  = $sum_row->count_all_results();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows  = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

}