<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_machine extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    public function selectMachineOne( $data )
 {

        $this->db->select( 'Id,Name,BoardNumber,OilTypeId' );
        $this->db->from( 'Machine ' );
        $this->db->where( 'BoardNumber', $data );
        $this->db->order_by( 'Id', 'ASC' );

        $query = $this->db->get();
        // var_dump( $query->result() );
        //exit();

        return $query->result();
    }

    public function selectMachineByName( $data )
 {

        $this->db->select( 'Id,Name,BoardNumber,OilTypeId' );
        $this->db->from( 'Machine ' );
        $this->db->where( 'Name', $data );
        $this->db->order_by( 'Id', 'ASC' );

        $query = $this->db->get();
        // var_dump( $query->result() );
        // exit();

        return $query->result();
    }

    public function selectMachineAll( $data )
 {

        $this->db->select( 'mc.Id,mc.Name,mc.BoardNumber,mc.StationId,st.stationCode,' );
        $this->db->select( 'mc.OilTypeId,ot.Name as OilType' );
        $this->db->from( 'Machine mc' );
        $this->db->join( 'station st', 'mc.stationId = st.Id', 'LEFT' );
        $this->db->join( 'OilType ot', 'ot.Id = mc.OilTypeId', 'LEFT' );
        $this->db->where( 'mc.BoardNumber', $data );
        $this->db->order_by( 'mc.Id', 'ASC' );

        $query = $this->db->get();
        // var_dump( $query->result() );
        // exit();

        return $query;
    }

    public function selectMachine()
 {

        $this->db->select( 'Id,Name' );
        $this->db->from( 'Machine' );
        $this->db->order_by( 'Id', 'ASC' );

        $query = $this->db->get();

        return $query->result();
    }

    public function selectMachineRemain()
 {
        $this->db->select( 'Id,Name,Capacity,remaining' );
        $this->db->from( 'Machine' );
        $this->db->order_by( 'Id', 'ASC' );

        $query = $this->db->get();
        return $query->result();
    }

    public function SelectMachineOilPrice()
 {
        $this->db->distinct();
        $this->db->select( 'mc.Id as MachineId,mc.Name as MachineName,ot.Id as OilTypeId,ot.Name as OilName ,pl.Value as OilValue ,pld.Id as PricelistDetailId' );
        $this->db->from( 'Machine mc' );
        $this->db->join( 'OilType ot', 'ot.Id = mc.OilTypeId' );
        $this->db->join( 'Pricelist pl', 'ot.Id  = pl.OilTypeId' );
        $this->db->join( 'PricelistDetail pld', 'pld.PricelistId = pl.Id ' );
        $this->db->where( 'pl.IsDefault = 1' );
        $query = $this->db->get();
        return $query->result();
    }

    public function locationMachine()
 {
        $this->db->select( 'mc.Id,mc.Name as MachineName ,stu.Name as Status,sl.Name as LocationName,' );
        $this->db->select( 'CASE WHEN stu.Name = \'Enable\' THEN N\'กำลังทำงาน\'
        WHEN stu.Name = \'Disable\' THEN N\'ขัดข้อง\'
        WHEN stu.Name = \'Draft\' THEN N\'รอการยืนยัน\'
        END AS StatusThai
        ' );
        $this->db->from( 'Machine mc ' );
        $this->db->join( 'Station st ', 'st.Id = mc.StationId' );
        $this->db->join( 'Status stu', ' stu.Id = mc.StatusId' );
        $this->db->join( 'StationLocation sl', 'sl.Id = st.LocationId' );
        $this->db->where( 'mc.statusId BETWEEN 1 AND 2' );
        $query = $this->db->get();
        //var_dump( $query->result() );
        //exit;
        return $query->result();
    }

    public function InsertBoard( $data ) {
        $t = $this->db->insert( 'Machine', $data );
        //var_dump( $t );
        // exit();
        if ( isset( $t ) ) {
            return TRUE;
        } else
        return FALSE;

    }

    public function showDraftMachineAll()
 {
        $this->db->select( 'Name,BoardId,BoardNumber,CreateDate,Capacity' );
        $this->db->select( 'Id AS rowId' );
        $this->db->from( 'Machine' );
        $this->db->where( 'DeleteFlag', '0' );
        $this->db->where( 'StatusId', '3' );

        $this->db->order_by( 'CreateDate', 'DESC' );
        $query = $this->db->get();

        $rows  = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function SelectBoardById( $data )
 {
        $this->db->select( 'Name AS "BoardName",BoardId,BoardNumber,CreateDate,Capacity,Id AS "rowId"' );
        $this->db->from( 'Machine' );
        $this->db->where( 'DeleteFlag', '0' );
        $this->db->where( 'StatusId', '3' );

        $this->db->where( 'Id', $data['id'] );
        $query = $this->db->get();
        // var_dump( $data );
        // exit;
        return $query->result();
    }

    public function EditBoardById( $data )
 {
        $res = false;
        $this->db->set( 'Name', $data['BoardName'] );
        $this->db->set( 'BoardId', $data['BoardId'] );
        $this->db->set( 'BoardNumber', $data['BoardNumber'] );
        //$this->db->set( 'deleteFlag', 1 );
        $this->db->set( 'UpdateDate', $data['UpdateDate'] );
        $this->db->where( 'Id', $data['Id'] );
        $res = $this->db->update( 'Machine' );
        // echo '<br/>sql'.$this->db->last_query();
        // echo '<br/>res = ';
        // var_dump( $res );
        // exit();
        //var_dump( $res );
        // exit();

        return $res;
    }

    public function showDashboardStation()
 {
        $this->db->select( 'sa.StationCode as รหัสตู้,ot.Name AS น้ำมัน,mc.Name AS ชื่อเครื่อง,mc.Capacity as ปริมาตร,mc.Balance as เงินคงเหลือ,mc.Remaining as น้ำมันคงเหลือ,st.Name AS สถานะ' );
        $this->db->from( 'Machine mc' );
        $this->db->join( ' OilType ot', 'ot.Id = mc.OilTypeId' );
        $this->db->join( ' Status st', 'st.Id = mc.StatusId' );
        $this->db->join( 'Station sa', 'sa.Id = mc.StationId' );
        $this->db->order_by( 'mc.CreateDate', 'ASC' );

        $query = $this->db->get();

        $rows  = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function countNumOilStation()
 {
        $this->db->Select( 'Count(Id)' );
        $this->db->from( 'Machine' );
        $query = $this->db->get();
        //var_dump( $query->result() );
        //exit;
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function countStationonline()
 {
        $this->db->Select( 'Count(mc.Id)' );
        $this->db->from( 'Machine mc' );
        $this->db->where( 'mc.StatusId = 1' );
        $query = $this->db->get();
        //var_dump( $query->result() );
        //exit;
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function countStationoffline()
 {
        $this->db->Select( 'Count(mc.Id)' );
        $this->db->from( 'Machine mc' );
        $this->db->where( 'mc.StatusId = 2' );
        $query = $this->db->get();
        //var_dump( $query->result() );
        //exit;
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function getNameStationonline()
 {
        $this->db->Select( 'mc.Name AS "name"' );
        $this->db->from( 'Machine mc' );
        $this->db->where( 'mc.StatusId = 1' );
        $query = $this->db->get();
        //var_dump( $query->result() );
        //exit;
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function setPrice( $data ) {

        $res = false;

        $strSQL = 'UPDATE PricelistDetail  set value = '.$data['price'];
        $strSQL .= ' WHERE Id = '.$data['Id'];
        // $this->db->query( $strSQL );
        if ( $this->db->query( $strSQL ) ) {
            $res = true;
        }

        return $res;
    }
    public function selectpriceByBoardNumber($data){
        if($data == null){
            return NULL;
        }
        // var_dump($data);exit;
        $this->db->select('m.Name as MachineName,ot.Name as OilName,pd.Value');
        $this->db->from('PricelistDetail pd');
        $this->db->join('MachinePricelist mpl','mpl.PricelistId = pd.PricelistId');
        $this->db->join('Machine m','m.Id = mpl.MachineId');
        $this->db->join('OilType ot','ot.Id = m.OilTypeId');
        $this->db->where('m.BoardNumber',$data);
        $this->db->where('mpl.DeleteFlag = 00000000000000000000');
        
        $query = $this->db->get();

        return $query->result();
    }

}