<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mol_cost extends CI_Model
{
 public $resultSQL = false;

 public function __construct()
 {
  parent::__construct();
  // $this->load->database();
  $this->load->database('happystation', false, true);
 }

 public function selectCostAll($data)
 {
  if (!is_null($data)) {
   foreach ($data as $key => $value) {
    if ($key != 'duration' && $key != 'offset') {
     $this->db->where($key, $value);
    } else {
     $arrData[$key] = $value;
    }
   }
  }

  $this->db->select('mc.Name AS ชื่อเครื่อง,st.Name AS ชื่อสถานีน้ำมัน,c.Value as ราคาน้ำมัน,c.Id AS "rowId"');
  $this->db->from('Cost c');
  $this->db->join(' Machine mc', 'mc.Id = c.MachineId');
  $this->db->join(' Station st', 'st.Id = c.StationId');
  $this->db->order_by('c.Id', 'ASC');
  $query = $this->db->get();

  $rows = $query->num_rows();

  if ($rows > 0) {
   for ($i = 0; $i < $rows; $i++) {
    $result['result'][$i] = $query->row_array($i);
   }
  }

  return $result;
 }

 public function SelectCostById($data)
 {
  $this->db->select('mc.Name AS "MachineName",st.Name AS StationName,c.Value,c.Id AS "rowId"');
  $this->db->from('Cost c');
  $this->db->join(' Machine mc', 'mc.Id = c.MachineId');
  $this->db->join(' Station st', 'st.Id = c.StationId');
  $this->db->where('c.Id', $data['id']);
  $query = $this->db->get();
  // var_dump( $data );
  // exit;
  return $query->result();
 }

 public function EditCostById($data)
 {
  $res = false;
  // $this->db->set( 'Value', $data['Value'] );
  $this->db->set('deleteFlag', 1);
  $this->db->set('UpdateDate', $data['UpdateDate']);
  $this->db->where('Id', $data['Id']);
  $res = $this->db->update('Cost');
  // echo '<br/>sql'.$this->db->last_query();
  // echo '<br/>res = ';
  // var_dump( $res );
  // exit();

  return $res;
 }

 public function InsertCost($data = array())
 {
  // $t =   $this->db->delete( 'Cost', array( 'Id' => $data['Id'] ) );

  // $res = $this->db->insert( 'Cost', $data );
  // $data = array( 'name' => $name, 'email' => $email, 'url' => $url );
  // $data['DeleteFlag'] = 0;
  $strSQL = ' INSERT INTO Cost (DeleteFlag, CreateUserId, CreateDate,Ordering,CustomerId,StatusId,StationId,OilTypeId,MachineId,Value) ';
  $strSQL .= ' SELECT 0, CreateUserId, CreateDate,Ordering,CustomerId,1,StationId,OilTypeId,MachineId,' . $data['Value'];
  $strSQL .= ' FROM Cost AS CCT';
  $strSQL .= ' WHERE CCT.Id =' . $data['Id'];

  $query = $this->db->query($strSQL);

  // var_dump( $query );
  // exit;
  if (isset($query)) {
   $resultSQL = true;
  } else {
   return false;
  }

  return $resultSQL;
 }

 public function selectCostHistory(array $data = null)
 {
  $result = null;
  $arrData['duration'] = 1;
  $arrData['offset'] = 0;

  if (!is_null($data)) {
   foreach ($data as $key => $value) {
    if ($key != 'duration' && $key != 'offset') {
     $this->db->where($key, $value);
    } else {
     $arrData[$key] = $value;
    }
   }
  }
  $this->db->select('mc.Name AS ชื่อเครื่อง,st.Name AS ชื่อสถานีน้ำมัน,c.Value as ราคาน้ำมัน');
  $this->db->select('DATE_FORMAT(c.UpdateDate, \'%d/%m/%y %H:%i:%s\') as วันที่อัพเดท ');
  $this->db->from('Cost c');
  $this->db->join(' Machine mc', 'mc.Id = c.MachineId');
  $this->db->join(' Station st', 'st.Id = c.StationId');
  //  $this->db->order_by( $oBy, 'desc' );
  $this->db->order_by('c.UpdateDate', 'DESC');
  $query = $this->db->get();

  $rows = $query->num_rows();

  if ($rows > 0) {
   for ($i = 0; $i < $rows; $i++) {
    $result['result'][$i] = $query->row_array($i);
   }
  }

  return $result;

 }

}