<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_pricelist extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    //Select Default price

    public function selectDefaultPrice( array $data = null )
 {
        $result = null;

        $this->db->select( '\'Default\' AS รหัสตู้,\'-\' AS วันที่เริ่ม,\'-\' AS วันที่จบ' );
        $this->db->select( 'OTE.Name as น้ำมัน,PDL.Amount AS ราคาน้ำมัน' );
        // $this->db->select( '"PLT".StartDate', 'PLT'.EndDate );
        $this->db->from( 'Pricelist AS PLT' );

        $this->db->join( 'PricelistDetail AS PDL', 'PDL.PricelistId = PLT.Id' );
        $this->db->join( 'OilType AS OTE', 'OTE.Id = PLT.OilTypeId' );
        $this->db->where( 'PLT.DeleteFlag = 0' );
        $this->db->where( 'PLT.StatusId = 1' );
        $this->db->where( 'PLT.Name', 'UnitPerLite' );
        $this->db->where( 'PLT.IsDefault = 1' );

        $query = $this->db->get();
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {

                $result['result'][$i] = $query->row_array( $i );
            }
        }
        // var_dump( $result );
        // exit();
        return $result;
    }

    //Select All data

    public function selectStationPrice( array $data = null )
 {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }
            }
        }
        // $this->db->select( '\'Default\' AS StationName,\'-\' AS StartDate,\'-\' AS EndDate' );
        $this->db->select( 'STN.StationCode as StationCode,' );
        $this->db->select( 'DATE_FORMAT(PLT.StartDate, "%d/%m/%Y") as \'StartDate\'' );
        $this->db->select( 'DATE_FORMAT(PLT.EndDate, "%d/%m/%Y") as \'EndDate\'' );
        $this->db->select( 'OTE.Name,PDL.Amount AS "Price"' );
        // $this->db->select( '"PLT".StartDate', 'PLT'.EndDate );
        $this->db->from( 'Pricelist AS PLT' );

        $this->db->join( 'PricelistDetail AS PDL', 'PDL.PricelistId = PLT.Id AND PDL.DeleteFlag=0', 'LEFT' );
        $this->db->join( 'OilType AS OTE', 'OTE.Id = PLT.OilTypeId', 'LEFT' );
        $this->db->join( 'StationPricelist AS SPT', 'SPT.PricelistId = PLT.Id', 'LEFT' );
        $this->db->join( 'Station AS STN', 'STN.Id = SPT.StationId', 'LEFT' );
        $this->db->where( 'PLT.DeleteFlag = 0' );
        $this->db->where( 'PLT.StatusId = 1' );
        $this->db->where( 'PLT.Name', 'UnitPerLite' );
        $this->db->where( 'PLT.IsDefault = 0' );

        // echo '<br/>sql'.$this->db->get_compiled_select();
        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {

                    $result['result'][$i] = $query->row_array( $i );
                }
            }
            // $result['result'][0] = array( 'Date'=>'12/07/2020', 'StationName'=>'Default', 'OilType'=>'95', 'value'=>'35' );
            // $result['result'][1] = array( 'Date'=>'12/07/2020', 'StationName'=>'Test-001', 'OilType'=>'95', 'value'=>'37' );
            // $result['result'][2] = array( 'Date'=>'12/07/2020', 'StationName'=>'Test-004', 'OilType'=>'95', 'value'=>'35' );

        }
        // var_dump( $result );
        // exit();
        return $result;
    }

    //Select data By Id

    public function selectPriceById( $id )
 {

        $this->db->where( 'USER_ID', intval( $id ) );
        $query = $this->db->get( 'user' );
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            $result['result'][0] = $query->row_array( 0 );
        }

        return $result;
    }

    //Select price for API

    public function selectPriceAPI( array $data = null )
 {

        $result = null;
        $dateNow = date( 'Y-m-d' );

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }
            }
        }

        $this->db->select( '"PDL"."Id" AS "id","MHE"."Name","MHE".BoardNumber,' );
        $this->db->select( '"OTE"."Name" AS "oilName", "PDL"."Amount" AS "values" ' );
        $this->db->from( 'Pricelist AS "PLT"' );

        $this->db->join( 'PricelistDetail AS "PDL"', 'PDL.PricelistId = "PLT".Id AND "PDL"."DeleteFlag"=0', 'left' );
        $this->db->join( 'OilType AS "OTE"', 'OTE.Id = PLT.OilTypeId', 'left' );
        $this->db->join( 'StationPricelist AS "SPT"', 'SPT.PricelistId = PLT.Id', 'left' );
        $this->db->join( 'Station AS "STN"', 'STN.Id = SPT.StationId', 'left' );
        $this->db->join( 'Machine AS "MHE"', 'MHE.StationId = STN.Id AND MHE.OilTypeId = "PLT".OilTypeId', 'left' );
        $this->db->where( '"PLT".DeleteFlag = 0' );
        $this->db->where( '"PLT".StatusId = 1' );
        $this->db->where( 'PLT.StartDate <=', $dateNow );
        $this->db->where( 'PLT.EndDate >= ', $dateNow );
        $this->db->where( '"PLT".Name', 'UnitPerLite' );

        $query = $this->db->get();
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {

                $result['data'] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function UpdateOilvalue( $data )
 {
        // var_dump( $data['Value'] );
        // exit;
        $res = false;
        $this->db->set( 'Value', $data['Value'] );
        $this->db->where( 'OilTypeId', $data['id'] );
        if ( $this->db->Update( 'Pricelist' ) ) {
            $res = true;
        }
        return $res;
    }

    public function SelectPrice( $data ) {

        $this->db->select( 'pld.Name,' );
        $this->db->from( 'PricelistDetail as pld' );
        $this->db->join( 'Pricelist as pl', 'pl.Id = pld.PricelistId' );
        $this->db->where( 'PricelistId', $data );

        $query = $this->db->get();

        return $query->result();
    }

}