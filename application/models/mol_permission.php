<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_permission extends CI_Model
 {

    private $result = array();

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    //Select All data for APi

    public function GetUserPermission( array $data = null ) {
        $this->db->select( '*' );
        $this->db->from( 'UserPermission' );
        $this->db->where( 'UserId', $data );
        $query = $this->db->get();

        return $query->result();
    }

}