<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mol_status extends CI_Model
{

    private $result = array();

    public function __construct()
    {
        parent::__construct();
        // $this->load->database();
        $this->load->database('happystation', false, true);
    }

    //Select All data for APi
    public function GetStatus($data)
    {
        $this->db->select('Id,Name');
        $this->db->from('Status');
        $this->db->where('Name', $data);
        $query = $this->db->get();
      
        return $query->result();
    }
}