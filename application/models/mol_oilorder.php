<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_oilorder extends CI_Model
 {
    public $resultSQL = false;

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    public function selectOilOrder()
 {
        $this->db->select( '*' );
        $this->db->from( 'OilOrder' );
        $query = $this->db->get();

        return $query->result();
    }

    public function ApproveUpdate( $data )
 {
        $res = false;
        if ( $data != null ) {
            $this->db->set( 'ApproveDate', date( 'Y-m-d H:i:s' ) );
            $this->db->set( 'ApproveId', 'AP -'.date( 'YmdHis' ) );

            $this->db->set( 'UpdateDate', date( 'Y-m-d H:i:s' ) );
            $this->db->where( 'PurchaseNo', $data['po'] );
            $res = $this->db->update( 'OilOrder' );
        }
        return $res;
    }

    public function RefillUpdate( $data )
 {
        $res = false;
        if ( $data != null ) {
            $this->db->set( 'RefillDate', date( 'Y-m-d H:i:s' ) );
            $this->db->set( 'RefillId', 'RF -'.date( 'YmdHis' ) );
            $this->db->set( 'RouteManId', $data['rmid'] );
            $this->db->set( 'UpdateDate', date( 'Y-m-d H:i:s' ) );
            $this->db->where( 'PurchaseNo', $data['po'] );
            $res = $this->db->update( 'OilOrder' );
        }
        return $res;
    }

    public function ConfirmUpdate( $data )
 {
        $res = false;
        if ( $data != null ) {
            $this->db->set( 'ConfirmDate', date( 'Y-m-d H:i:s' ) );
            $this->db->set( 'ConfirmId', 'CF -'.date( 'YmdHis' ) );
            $this->db->set( 'UpdateDate', date( 'Y-m-d H:i:s' ) );
            $this->db->where( 'PurchaseNo', $data );
            $res = $this->db->update( 'OilOrder' );
        }
        return $res;
    }

    public function showOilOrder( $data )
 {

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }
            }
        }

        $this->db->select( 'st.Name AS ชื่อตู้ , ot.Name AS น้ำมัน , or.QTY AS ปริมาณ ' );
        $this->db->select( ' or.Cost as ราคา , or.Id AS rowId' );
        $this->db->from( 'OilOrder or' );
        $this->db->join( 'OilType ot', 'ot.Id = or.OilTypeId', 'left' );
        $this->db->join( 'Machine MH', 'MH.Id = or.MachineId', 'left' );
        $this->db->join( 'Station st', 'st.Id = MH.StationId', 'left' );
        $this->db->where( 'or.StatusId', 3 );
        $this->db->where( 'or.DeleteFlag', 0 );
        $this->db->order_by( 'or.Id', 'ASC' );

        $query = $this->db->get();
        $rows = $query->num_rows();
        // echo '<br/>sql='.$this->db->last_query();
        // exit();
        $result = null;
        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function ConfirmOilOrder( $data )
 {

        $res = false;
        $this->db->set( 'StatusId', 4 );

        $this->db->set( 'StatusId', $data['StatusId'] );
        $this->db->set( 'ConfirmDate', $data['ConfirmDate'] );
        $this->db->where( 'Id', $data['Id'] );

        $res = $this->db->update( 'OilOrder' );
        //var_dump( $data['StatusId'] );
        // exit();
        return $res;
    }

    public function selectListOrderHistory()
 {
        $result = null;

        $this->db->select( 'st.Name AS ชื่อตู้ , ot.Name AS น้ำมัน , or.QTY AS ปริมาณ , or.Cost  as ราคา, or.ConfirmDate as วันที่รับทราบ' );
        //$this->db->select( 'FORMAT (or.UpdateDate, \'dd-MM-yy HH:mm:ss\') ' );
        $this->db->from( 'OilOrder or' );
        $this->db->join( 'OilType ot', 'ot.Id = or.OilTypeId' );
        $this->db->join( 'Machine MH', 'MH.Id = or.MachineId', 'left' );
        $this->db->join( 'Station st', 'st.Id = MH.StationId', 'left' );
        $this->db->where( 'or.DeleteFlag', 0 );
        $this->db->where( 'or.StatusId > ', 3, false );
        $this->db->order_by( 'or.ConfirmDate', 'DESC' );

        $query = $this->db->get();
        $rows = $query->num_rows();
        // echo '<br/>sql='.$this->db->last_query();
        // exit();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function selectListOrderHistoryExcel()
 {
        $result = null;

        $this->db->select( 'st.Name AS ชื่อตู้ , ot.Name AS น้ำมัน , or.QTY AS ปริมาณ , or.Cost  as ราคา, or.ConfirmDate as วันที่รับทราบ' );
        //$this->db->select( 'FORMAT (or.UpdateDate, \'dd-MM-yy HH:mm:ss\') ' );
        $this->db->from( 'OilOrder or' );
        $this->db->join( 'OilType ot', 'ot.Id = or.OilTypeId' );
        $this->db->join( 'Machine MH', 'MH.Id = or.MachineId', 'left' );
        $this->db->join( 'Station st', 'st.Id = MH.StationId', 'left' );
        $this->db->where( 'or.DeleteFlag', 0 );
        $this->db->where( 'or.StatusId > ', 3, false );
        $this->db->order_by( 'or.ConfirmDate', 'DESC' );

        $query = $this->db->get();

        return $query->result_array();
    }

    public function InsertOilOrder( array $data = null )
 {
        $this->db->select( 'COUNT(PurchaseNo) as PO_Num' );
        $this->db->from( 'OilOrder' );
        $query = $this->db->get();

        $res = false;
        if ( $data != null ) {
            foreach ( $query->result() as $item ) {

                $result = array(
                    'CreateDate' => date( 'Y-m-d H:i:s' ),
                    'CreateUserId' => 1,
                    'DeleteFlag' => 0,
                    'Ordering' => 0,
                    'StatusId' => $data['statusId'],
                    'PurchaseNo' => $this->invoice_num( $item->PO_Num, 6, 'PO-' ),
                    'MachineId' => $data['Machine'],
                    'OilTypeId' => $data['oiltype'],
                    'Qty' => $data['qty'],
                    'Cost' => $data['cost'],
                );
                $this->db->Insert( 'OilOrder', $result );

                $res = true;
            }
        }
        return $res;
    }

    public function invoice_num( $input, $pad_len, $prefix = null )
 {

        if ( $pad_len <= strlen( $input ) ) {
            trigger_error( '<strong>$pad_len</strong> cannot be less than or equal to the length of <strong>$input</strong> to generate invoice number', E_USER_ERROR );
        }

        if ( is_string( $prefix ) ) {
            return sprintf( '%s%s', $prefix, str_pad( $input, $pad_len, '0', STR_PAD_LEFT ) );
        }

        return str_pad( $input, $pad_len, '0', STR_PAD_LEFT );
    }

    public function SelectOilOrderByStationId( $data )
 {
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }
        $this->db->select( 'oo.*,st.stationCode' );
        $this->db->from( 'OilOrder oo' );
        $this->db->join( 'Machine mc', ' mc.Id = oo.MachineId' );
        $this->db->join( 'Station st', ' st.Id = mc.StationId' );
        $this->db->where( 'oo.RefillId IS NOT NULL' );
        $this->db->where( 'mc.StationId', $data['stationid'] );
        $this->db->order_by( 'oo.id', 'desc' );

        $query = $this->db->get();

        return $query->result();
    }

    public function SelectOrderByNo( $data )
 {
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }
        $this->db->select( 'oo.id,oo.DeleteFlag,oo.DeliveryDate,oo.ApproveId,oo.ApproveDate,mc.Name as MachineName ,st.StationCode,rg.Name as RouteName,
        rg.[Desc] as detail' );
        $this->db->from( 'oilorder oo' );
        $this->db->join( 'Machine mc', 'mc.id = oo.MachineId' );
        $this->db->join( 'Station st', 'st.Id = mc.StationId' );
        $this->db->join( 'RouteGroup rg', 'rg.id = st.RouteGroupId' );
        $this->db->join( 'RouteMan rm', 'rm.RouteGroupId = rg.id' );
        $this->db->where( 'oo.PurchaseNo', $data );
        $query = $this->db->get();

        return $query->result();
    }

    public function SelectOrderByRouteManId( $data )
 {
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );

        }
        $this->db->select( 'oo.PurchaseNo,ot.Name as OilName,oo.Qty, mc.Name as MachineName , st.StationCode, st.RouteGroupId,rg.Name as RouteGroupName,stl.Name as LocationName,stl.Latitude ,stl.Longitude' );
        $this->db->from( 'OilOrder oo' );
        $this->db->join( 'Machine mc', 'mc.Id = oo.MachineId', 'LEFT' );
        $this->db->join( 'Station st', 'st.Id = mc.StationId', 'LEFT' );
        $this->db->join( 'RouteMan rm', 'rm.RouteGroupId = st.RouteGroupId', 'LEFT' );
        $this->db->join( 'OilType ot', ' ot.id = oo.OilTypeId', 'LEFT' );
        $this->db->join( 'RouteGroup rg', 'rg.Id = st.RouteGroupId', 'LEFT' );
        $this->db->join( 'StationLocation stl', 'stl.Id = st.LocationId', 'LEFT' );
        $this->db->where( 'oo.DeleteFlag = 0' );
        $this->db->where( ' oo.ApproveId IS NOT NULL' );
        $this->db->where( ' oo.RouteManId ', $data['routemanid'] );
        $query = $this->db->get();

        return $query->result();
    }

    // public function InsertOilOrder( $data )
    // {
    //     $strSQL  = ' INSERT INTO OilOrder (DeleteFlag, CreateUserId, CreateDate,Ordering,PurchaseNo,StationId,OilTypeId,Qty,Cost,ApproveDate,RefillDate,ConfirmDate,StatusId) ';
    //     $strSQL .= ' SELECT 0, CreateUserId, CreateDate,Ordering,PurchaseNo,StationId,OilTypeId,Qty,Cost,ApproveDate,RefillDate,ConfirmDate,'.$data['StatusId'];
    //     $strSQL .= ' FROM OilOrder AS CCT';
    //     $strSQL .= ' WHERE CCT.Id ='.$data['Id'];

    //     $query = $this->db->query( $strSQL );

    //     // var_dump( $query );
    // exit;
    //     if ( isset( $query ) ) {
    //         $resultSQL = TRUE;
    //     }
    //     else
    //         return FALSE;

    //     return $resultSQL;
    // }

}