<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_customer extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    //Select All data

    public function GetCustomer( $data )
 {

        $this->db->select( 'id,CompanyName,CusCode' );
        $this->db->from( 'Customers' );
        $this->db->where( 'id', $data );
        $query = $this->db->get();

        // print_r( $query );
        // var_dump( $query );
        // exit();
        return $query->result();
    }

    public function GetCustomerByCusCode( $data )
 {

        $this->db->select( 'id,CompanyName,CusCode' );
        $this->db->from( 'Customers' );
        $this->db->where( 'CusCode', $data );
        $query = $this->db->get();

        return $query->result();
    }

    public function GetCustomerlist()
 {

        $this->db->select( 'id,CompanyName,CusCode' );
        $this->db->from( 'Customers' );
        $query = $this->db->get();
        return $query->result();
    }

    public function ShowCustomer( array $data = null )
 {

        // $query = $this->db->get();

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'Id as ไอดี,CompanyName as ชื่อบริษัท,FirstName as ชื่อ,LastName as นามสกุล,Tel as เบอร์โทรศัพท์,Email as อีเมล,Id AS rowId' );
        $this->db->from( 'Customers' );
        $this->db->order_by( 'Id', 'ASC' );

        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        // $query = $this->db->get();
        // $rows = $query->num_rows();
        // echo 'sql = '.$this->db->last_query();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;

    }

    public function InsertCustomer( array $data = null )
 {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $t = $this->db->insert( 'Customers', $data );
        if ( isset( $t ) ) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function getCustomerById( $data )
 {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'Id,CompanyName,FirstName,LastName,Tel,Email,CusCode,Address1,Id AS rowId' );
        $this->db->from( 'Customers' );
        $this->db->where( 'Id', $data['Id'] );
        $query = $this->db->get();
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        // var_dump( $result );
        // exit();
        return $result;
    }

    public function UpdateCustomer( $data )
 {
        $res = false;
        $this->db->set( 'UpdateDate', $data['UpdateDate'] );
        $this->db->set( 'CompanyName', $data['CompanyName'] );
        $this->db->set( 'FirstName', $data['FirstName'] );
        $this->db->set( 'LastName', $data['LastName'] );
        $this->db->set( 'Address1', $data['Address1'] );
        $this->db->set( 'Email', $data['Email'] );
        $this->db->set( 'Tel', $data['Tel'] );
        $this->db->set( 'CusCode', $data['CusCode'] );
        $this->db->where( 'Id', $data['Id'] );
        $res = $this->db->update( 'Customers' );
        return $res;
    }

}