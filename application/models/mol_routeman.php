<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_routeman extends CI_Model
 {
    public function __construct()
 {
        parent::__construct();

        $this->load->database( 'happystation', false, true );
    }

    public function selectAllRouteMan()
 {
        $result = null;

        $this->db->select( 'FirstName + \' \' + LastName as RouteMan,Tel' );
        $this->db->from( 'RouteMan' );
        $this->db->where( 'StatusId = 1' );
        $this->db->order_by( 'Id', 'ASC' );
        $query = $this->db->get();
        $rows  = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function selectRouteManById( $data )
 {
        if ( $data == null )
 {
            throw new Exception( 'Error Processing Request', 1 );
        }

        $this->db->select( 'Id,FirstName,LastName,Tel' );
        $this->db->from( 'RouteMan' );
        $this->db->where( 'Id', $data );
        $this->db->order_by( 'Id', 'ASC' );

        $query = $this->db->get();
        $rows  = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function GetUserRouteMan( $data ) 
 {
        //$result = null;

        $this->db->select( 'Id,CustomerId,username,password,FirstName , LastName' );
        $this->db->from( 'RouteMan' );
        $this->db->where( 'DeleteFlag = 0' );
        $this->db->where( 'UPPER(username)', $data );

        $query = $this->db->get();
        // var_dump( $query );
        // exit;
        // $rows  = $query->num_rows();

        // if ( $rows > 0 ) {
        //     for ( $i = 0; $i < $rows; $i++ ) {
        //         $result['result'][$i] = $query->row_array( $i );
        //     }
        // }

        return $query->result();
    }

    public function UpdatePasswordRouteMan( $data ) {

        $res = false;
        $strSQL = 'UPDATE RouteMan SET Password = N\''.$data['enpass'].'\' WHERE UserName = N\''.$data['username'].'\'';
        if ( $this->db->query( $strSQL ) ) {
            $res = true;
        }
        return $res ;
    }

    public function UpdateTokenRouteManByUserName( $data ) {
        if ( $data == null ) {
            throw new Exception( 'DATA IS NULL', 1 );
        }
        $res = false;
        $strQuery = 'UPDATE RouteMan SET Token = N\''.$data['Token'].'\' Where Username ='.$data['Username'];
        if ( $this->db->query( $strQuery ) ) {
            $res = true;
        }
        return $res;
    }

}