<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_station extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    public function selectStationAll( array $data = null )
 {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset']   = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }
            }
        }
        $this->db->select( 'sn.StationCode as รหัสตู้,st.Name as สถานะ,sl.Name as สถานที่,rg.Name as เส้นทาง' );
        $this->db->select( 'sn.Remaining as น้ำมันที่เหลือ,sn.MaxCapacity as ปริมาณ,' );
        //$this->db->select( 'DATE_FORMAT(sn.LastRefill,\'%d/%m/%Y %H:%i:%S \') AS LastRefill' );
        $this->db->select( 'sn.Balance as เงินคงเหลือ ,sn.id AS "rowId"' );
        $this->db->from( 'Station sn' );
        $this->db->join( 'Status st', 'st.Id = sn.StatusId', 'left' );
        $this->db->join( 'StationLocation sl', 'sl.Id = sn.LocationId ', 'left' );
        $this->db->join('RouteGroup rg','rg.Id = sn.RouteGroupId');
        $this->db->where( 'sn.DeleteFlag =', 0 );
        $this->db->order_by( 'sn.id', 'ASC' );

        $sum_row = clone $this->db;
        $rowAll  = $sum_row->count_all_results();

        // $query = $this->db->get();
        // $rows = $query->num_rows();
        // echo 'sql = '.$this->db->last_query();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows  = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function SelectStation( $data )
 {
        foreach ( $data as $key => $value ) {
            $this->db->select( 'sn.id,st.Name as Status,sn.StationCode,sn.Name,sl.Name as Location,sn.Remaining,sn.MaxCapacity,sn.LastRefill,sn.Balance' );
            $this->db->from( 'station sn' );
            $this->db->join( 'Status st', 'st.Id = sn.StatusId', 'left' );
            $this->db->join( 'StationLocation sl', 'sl.Id = sn.LocationId ', 'left' );
            $this->db->where( 'sn.id', $value );
            $this->db->or_where( 'sn.StationCode', $value );
            $this->db->order_by( 'sn.id', 'ASC' );
            $query = $this->db->get();
        }
        return $query;
    }

    public function SelectStationStockMove( $data )
 {
        // var_dump( $data );
        //exit();

        $this->db->select( 'Id' );
        $this->db->from( 'Station' );
        $this->db->where( 'StationCode', $data );
        $query = $this->db->get();
        // var_dump( $query->result() );
        // exit();
        return $query;
    }

    //Select All data

    public function selectStationList( array $data = null )
 {
        $result = null;

        $arrData['duration'] = 1;
        $arrData['offset']   = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 's.Id as รหัส,s.StationCode as รหัสตู้,s.MaxCapacity as ปริมาณ,s.Balance as เงินคงเหลือ' );
        $this->db->select( ' DATE_FORMAT(s.LastUpdate, \'%d-%m-%Y\') as อัพเดทล่าสุด,ss.Name as สถานะ' );
        $this->db->from( 'Station s' );
        $this->db->join( 'Status ss', 'ss.Id = s.StatusId', 'LEFT' );
        $this->db->order_by( 's.Id', 'ASC' );
        // $this->db->join( 'generation B', 'B.GENERATION_ID = A.GENERATION_ID', 'left' );
        // $this->db->where( '"STATUS"', 'ENABLED' );
        $sum_row = clone $this->db;
        $rowAll  = $sum_row->count_all_results();

        // $query = $this->db->get();
        // $rows = $query->num_rows();
        //echo 'sql = '.$this->db->last_query();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows  = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function get_last_ten_entries()
 {
        $query = $this->db->get( 'Station' );
        return $query->result();
    }

    public function selectStationLocation( array $data = null )
    {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset']   = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }

        $this->db->select( 'sl.Id,sl.Name as LocationName,sl.Latitude,sl.Longitude,st.StationCode,mc.BoardNumber' );
        $this->db->from( 'StationLocation sl' );
        $this->db->join( 'Station st', 'st.LocationId = sl.Id' );
        $this->db->join( 'Machine mc', 'mc.StationId = st.Id' );
        $this->db->order_by( 'Id', 'ASC' );

        $sum_row = clone $this->db;
        $rowAll  = $sum_row->count_all_results();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows  = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function SelectLocation( $data )
 {
        foreach ( $data as $key => $value ) {
            $this->db->select( 'sl.Id,sl.Name as LocationName,sl.Latitude,sl.Longitude,st.StationCode,mc.BoardNumber' );
            $this->db->from( 'StationLocation sl' );
            $this->db->join( 'Station st', 'st.LocationId = sl.Id' );
            $this->db->join( 'Machine mc', 'mc.StationId = st.Id' );
            $this->db->where( 'sl.id', $value );
            $query = $this->db->get();
        }
        return $query;
    }

    public function SelectLocationByName( array $data = null )
 {
        $this->db->select( 'Id,Name' );
        $this->db->from( 'StationLocation ' );
        $this->db->where( 'Name', $data['location'] );
        $query = $this->db->get();

        return $query;
    }

    public function selectAccountInvoice( array $data = null )
 {

        $result              = null;
        $arrData['duration'] = 1;
        $arrData['offset']   = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }
            }
        }
        $this->db->select( 'iv.id,iv.InvoiceNo,iv.Qty,iv.Amount,iv.PaymentedDate' );
        $this->db->from( 'accountinvoice iv' );
        $this->db->order_by( 'ID', 'ASC' );
        $sum_row = clone $this->db;
        $rowAll  = $sum_row->count_all_results();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows  = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }
        return $result;
    }

    public function InsertStation( $data )
 {
        $res = false;
        // echo '<br/>data ='.print_r( $data );
        // echo '<br/>sql ='.$this->db->get_compiled_select();
        if ( $this->db->insert( 'Station', $data ) )
 {
            $res = true;
        }

        return $res;
    }

    public function InsertLocation( $data )
 {
        $res = false;

        if ( $this->db->insert( 'StationLocation', $data ) )
 {
            $res = true;
        }
        return $res;
    }

    public function SelectStationById( $data )
 {
        $this->db->select( 'sn.StationCode,sn.Name AS StationName,sl.Name  AS LocationName,sn.Remaining,sn.MaxCapacity,sn.LastRefill,sn.Balance ,sn.id ,sn.LocationId AS LocationId' );
        $this->db->from( 'Station sn' );
        $this->db->join( 'Status st', 'st.Id = sn.StatusId', 'left' );
        $this->db->join( 'StationLocation sl', 'sl.Id = sn.LocationId ', 'left' );
        $this->db->where( 'sn.id', $data['id'] );
        $this->db->order_by( 'sn.id', 'ASC' );
        $query = $this->db->get();

        $rows  = $query->num_rows();
        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function SelectStationByUserId( $data )
 {
        $this->db->select( 'sn.StationCode,sn.Name AS StationName,sl.Name  AS LocationName,sn.Remaining,sn.MaxCapacity,sn.LastRefill,sn.Balance ,sn.id ,sn.LocationId AS LocationId' );
        $this->db->from( 'Station sn' );
        $this->db->join( 'Status st', 'st.Id = sn.StatusId', 'left' );
        $this->db->join( 'StationLocation sl', 'sl.Id = sn.LocationId ', 'left' );
        $this->db->where( 'sn.CreateUserId', $data );
        $this->db->order_by( 'sn.id', 'ASC' );
        $query = $this->db->get();

        $rows  = $query->num_rows();
        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function EditStationById( $data )
 {
        $res = false;
        //$this->db->set( 'deleteFlag', 1 );
        $this->db->set( 'Name', $data['Name'] );
        $this->db->set( 'StationCode', $data['StationCode'] );
        $this->db->set( 'Remaining', $data['Remaining'] );
        $this->db->set( 'MaxCapacity', $data['MaxCapacity'] );
        $this->db->where( 'Id', $data['Id'] );
        $res = $this->db->update( 'Station' );
        //var_dump( $res );
        // exit();
        return $res;
    }

    public function EditLocationById( $data ) {
        $res = false;
        // $this->db->set( 'deleteFlag', 1 );
        $this->db->set( 'Name', $data['LocationName'] );
        $this->db->where( 'Id', $data['LocationId'] );
        $res = $this->db->update( 'StationLocation' );
        //var_dump( $res );
        // exit();
        return $res;
    }

    public function SelectBalanceStationById( $data ) {
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }
        $this->db->select( 'id,stationCode,balance' );
        $this->db->from( 'Station' );
        $this->db->where( 'Id', $data['stationid'] );

        $query =  $this->db->get();

        return $query->result();
    }

    public function CountStation() {
        $this->db->select( 'COUNT(*) as CountNo' );
        $this->db->from( 'AccountInvoice' );
        $this->db->where( 'Deleteflag = 0' );
        $query = $this->db->get();

        return $query->result();
    }
}