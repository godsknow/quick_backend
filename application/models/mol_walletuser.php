<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_Walletuser extends CI_Model
 {

    private $result = array();

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happywallet', false, true );
    }

    //Select All data for APi

    public function GetWalletUser( $data ) {
        if ( $data == null ) {
            throw new Exception( 'Data Is Null', 1 );
        }
        $this->db->select( 'Id,CreateUserId,StatusId,customerId,username,password,PhoneNumber,Email,LastActive' );
        $this->db->from( 'WalletUser' );
        $this->db->where( 'DeleteFlag = 0' );
        $query = $this->db->get();
        return $query->result();
    }

    public function GetWalletUserByUserName( $data ) {
        $this->load->database( 'happywallet', false, true );

        if ( $data == null ) {
           return null;
        }
        
        $this->db->select( 'Id,CreateUserId,StatusId,customerId,username,password,PhoneNumber,Email,LastActive' );
        $this->db->from( 'WalletUser' );
        $this->db->where( 'DeleteFlag = 0' );
        $this->db->where( 'UserName', $data );
        $query = $this->db->get();
        // var_dump($query->result());exit;
        return $query->result();

    }

    public function GetWalletUserById($data) {
        if ( $data == null ) {
            throw new Exception( 'Data Is Null', 1 );
        }
        $this->db->select( 'StatusId,username,PhoneNumber,Email,LastActive' );
        $this->db->from( 'WalletUser' );
        $this->db->where( 'DeleteFlag = 0' );
        $this->db->where( 'Id', $data );
        $query = $this->db->get();
        return $query->result();

    }

    public function UpdateWalletUser( $data ) {

        if ( $data == null ) {
            throw new Exception( 'Data Is NULL', 1 );
        }
        $res = false;
        $strQuery = 'UPDATE WalletUser SET PhoneNumber = '.$data['Phone'].'Email = '.$data['Email'].' where Id ='.$data['Id'].'AND DeleteFlag = 0';

        if ( $this->db->query( $strQuery ) ) {
            $res = true;
        }
        return $res;
    }

    public function UpdatePassword( $data ) {
        $this->load->database( 'happywallet', false, true );
        if ( $data == null ) {
            throw new Exception( 'Data Is NULL', 1 );
        }
        $res = false;
        //$strQuery = 'UPDATE WalletUser SET Password = '.$data['enpass'].' where Id = '.$data['Id'][0]->Id.' AND DeleteFlag = 0';
        $this->db->set('Password',$data['enpass']);
        $this->db->set('LastActive' ,date('Y-m-d H:i:s'));
        $this->db->where('Id',$data['Id'][0]->Id);
        $this->db->where('DeleteFlag = 0');
        if($this->db->update('WalletUser')){
            $res =true;
        }
        return $res;
    }

    public function InsertWalletUser( array $data = null ) {
        $this->load->database( 'happywallet', false, true );
      
        if ( $data == null ) {
            throw new Exception( 'Data Is NULL', 1 );
        }
        $res = false;
        $array = array(
            'DeleteFlag' => 0,
            'CreateUserId' => 1,
            'CreateDate' => date( 'Y-m-d H:i:s' ),
            'Ordering' => 0,
            'StatusId' => 1,
            'UserName' => $data['UserName'],
            'Password' => $data['EnPassword'],
            'PhoneNumber' => $data['UserName'],
        );

        if ( $this->db->Insert( 'WalletUser', $array ) ) {
            $res = true;
        }

        return $res;
    }

    public function DeleteWalletUser( $data ) {

        if ( $data == null ) {
            throw new Exception( 'Data Is NULL', 1 );
        }
        $res = false;
        $strQuery = 'UPDATE WalletUser SET DeleteFlag = 0   where Id ='.$data['Id'];

        if ( $this->db->query( $strQuery ) ) {
            $res = true;
        }
        return $res;
    }

}