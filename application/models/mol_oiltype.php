<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_oiltype extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    public function selectOilTypeAll( $data )
 {

        $this->db->select( 'Id,Name' );
        $this->db->from( 'OilType' );
        $this->db->where( 'Name', $data );
        $this->db->order_by( 'Id', 'ASC' );

        $query = $this->db->get();

        return $query;
    }

    public function selectSetOilType( array $data = null )
 {

        $this->db->select( 'ot.Id,ot.Name,pl.Value' );
        $this->db->from( 'OilType ot' );
        $this->db->join( 'Pricelist pl', 'pl.OiltypeId = ot.Id', 'LEFT' );
        $this->db->where( 'ot.Id', $data['id'] );
        $this->db->where( 'pl.IsDefault', true );
        $this->db->order_by( 'ot.Id', 'ASC' );

        $query = $this->db->get();
        // var_dump( $query->result() );
        // exit;
        return $query;
    }

}