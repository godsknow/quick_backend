<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_routegroup extends CI_Model
 {
    public function __construct()
 {
        parent::__construct();

        $this->load->database( 'happystation', false, true );
    }

    //Select All data

    public function InsertRouteGroup( $data )
 {
        $t = $this->db->insert( 'RouteGroup', $data );

        if ( isset( $t ) ) {
            return TRUE;
        } else
        return FALSE;
    }

    public function SelectRouteGroup()
 {
        $result = null;

        $this->db->select( 'Name AS เส้นทาง,Description AS รายละเอียด ,Id AS "rowId"' );
        $this->db->from( 'RouteGroup' );
        $query = $this->db->get();

        $rows  = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        //var_dump( $result );
        // exit;
        return $result;
    }

    public function SelectRouteGroupById( $data )
 {
        $result = null;

        $this->db->select( 'Name AS RouteName,Description AS Description ,Id' );
        $this->db->from( 'RouteGroup' );
        $this->db->where( 'Id', $data['id'] );
        $query = $this->db->get();

        $rows  = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        //var_dump( $result );
        // exit;
        return $result;
    }

    public function EditRouteGroup( $data ) 
 {
        $res = false;
        // $this->db->set( 'deleteFlag', 1 );
        $this->db->set( 'Name', $data['Name'] );
        $this->db->set( 'Description', $data['Desc'] );
        $this->db->where( 'Id', $data['Id'] );
        $res = $this->db->update( 'RouteGroup' );
        //var_dump( $res );
        // exit();
        return $res;
    }

    public function SelectNameRouteGroup()
 {
        $result = null;

        $this->db->select( 'Name AS RouteName ,Id AS RouteGroupId' );
        $this->db->from( 'RouteGroup' );
        $query = $this->db->get();
        $rows  = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }
}