<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class mol_machineError extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    public function selectMachineError( $data = false )
 {
        $this->db->select( '*' );
        $this->db->from( 'MachineError' );
        $this->db->where( 'DeleteFlag', $data );
        $this->db->order_by( 'Id', 'DESC' );
        $this->db->limit(1);
        $query = $this->db->get();
        // var_dump( $query );
        // exit();
        return $query->result();
    }

    public function selectMachineErrorByRemain( $data )
 {
        // var_dump( $data );
        // exit();
        $this->db->select( '*' );
        $this->db->from( 'MachineError' );
        $this->db->where( 'Oil_Remaining', $data );
        $this->db->order_by( 'Id', 'Desc' );
        $this->db->limit(1);
        $query = $this->db->get();
//  var_dump( $query->result());exit;
        return $query->result();
    }

    public function selectMachineErrorByFuel()
 {
        // var_dump( $data );
        // exit();
        $this->db->select( '*' );
        $this->db->from( 'MachineError' );
        $this->db->order_by( 'Id', 'Desc' );
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->result();
    }

}