<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mol_sms extends CI_Model
{

    private $result = array();

    public function __construct()
    {
        parent::__construct();
        // $this->load->database();
        $this->load->database('happystation', false, true);
    }

    //Select All data for APi
    public function addLogsms(array $data = null)
    {
        $this->load->database('happystation', false, true);
        if($data == null){
            throw new Exception("Error Processing Request", 1);
        }
        $res = false;
        $array=array(
            'CreateDate' => date('Y-m-d H:i:s' ),
            'CustomerId' => 1,
            'UserId' => $data['UserId'],
            'Tel' => $data['phone'],
            'Sms' =>$data['msg'],
            'Type' => $data['type'],
            'Status' => $data['result']->QUEUE->Status, 
        );
        if($this->db->insert('LogSms',$array)){
            $res = true;
        }
      
        return $res;
    }
}