<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_manage_user extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        //$this->load->database( 'happystation', FALSE, TRUE );
        //echo '<br/>testdb'.$this->load->db_connect( TRUE );
        $db_obj = $this->load->database( 'happystation', false );
        //$this->db_connect( TRUE );
        //if ( $db_obj->conn_id ) {
        //do something
        //} else {
        //echo 'Unable to connect with database with given db details.';
        //}
    }

    public function selectUserdata( array $data = null )
 {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'c.CompanyName,kg.Name as RoleName,u.UserName' );
        $this->db->from( 'Users u' );
        $this->db->join( 'Customers c', 'c.Id = u.CustomerId', 'LEFT' );
        $this->db->join( 'UserPermission up', 'up.UserId = u.Id', 'LEFT' );
        $this->db->join( 'KeyGroup kg', 'kg.Id = up.KeyGroupId', 'LEFT' );
        // $this->db->where( '"STATUS"', 'ENABLED' );
        // $this->db->order_by( 'ID', 'ASC' );

        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        // $query = $this->db->get();
        // $rows = $query->num_rows();
        // echo 'sql = '.$this->db->last_query();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function selectRoleAll( array $data = null )
 {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'Name,UpdateDate AS LastUpdate' );
        $this->db->select( 'Id AS rowId' );
        $this->db->from( 'KeyGroup u' );
        // $this->db->join( 'Customers c', 'c.Id = u.CustomerId', 'LEFT' );
        // $this->db->join( 'UserPermission up', 'up.UserId = u.Id', 'LEFT' );
        // $this->db->join( 'KeyGroup kg', 'kg.Id = up.KeyGroupId', 'LEFT' );
        $this->db->where( 'DeleteFlag', '0' );
        $this->db->order_by( 'ID', 'DESC' );

        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        // $query = $this->db->get();
        // $rows = $query->num_rows();
        // echo 'sql = '.$this->db->last_query();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function selectKeyAll( array $data = null )
 {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'Name,UpdateDate AS LastUpdate' );
        $this->db->select( 'Id AS rowId' );
        $this->db->from( 'Key' );
        // $this->db->join( 'Customers c', 'c.Id = u.CustomerId', 'LEFT' );
        // $this->db->join( 'UserPermission up', 'up.UserId = u.Id', 'LEFT' );
        // $this->db->join( 'KeyGroup kg', 'kg.Id = up.KeyGroupId', 'LEFT' );
        $this->db->where( 'DeleteFlag', '0' );
        $this->db->order_by( 'ID', 'DESC' );

        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        // $query = $this->db->get();
        // $rows = $query->num_rows();
        // echo 'sql = '.$this->db->last_query();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function SelectGroupRole( array $data = null )
 {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'kg.name as ชื่อ,kg.Id as รหัส' );
        $this->db->from( 'KeyGroup kg' );
        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function SelectGroupRolebyId( $Id )
 {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        $this->db->select( 'kg.Id AS "groupid",kg.name AS "groupname"' );
        // $this->db->select( 'k.Name AS "keyname"' );
        $this->db->from( 'KeyGroup kg' );
        // $this->db->join( 'KeyPermission kp', 'kp.GroupKey = kg.Id' );
        // $this->db->join( 'Key k', 'k.id = kp.KeyId' );
        $this->db->where( 'kg.Id', $Id );

        $query = $this->db->get();

        $rowAll = $query->num_rows();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            // $query = $this->db->get();
            // $rows = $query->num_rows();

            if ( $rowAll > 0 ) {
                for ( $i = 0; $i < $rowAll; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function SelectKeybyId( $Id )
 {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        // $this->db->select( 'kg.Id AS "groupid",kg.name AS "groupname"' );
        $this->db->select( 'k.Name AS "keyname"' );
        $this->db->from( 'KeyGroup kg' );
        $this->db->join( 'KeyPermission kp', 'kp.GroupKey = kg.Id' );
        $this->db->join( 'Key k', 'k.id = kp.KeyId' );
        $this->db->where( 'kg.Id', $Id );

        $query = $this->db->get();
        $rowAll = $query->num_rows();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            // $query = $this->db->get();
            // $rows = $query->num_rows();

            if ( $rowAll > 0 ) {
                for ( $i = 0; $i < $rowAll; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function GetRolePermissionbyId( $Id )
 {
        $this->db->select( 'KE.Name' );
        $this->db->from( 'UserPermission UPN' );
        $this->db->join( 'KeyGroup KGP', 'KGP.Id=UPN.KeyGroupId' );
        $this->db->join( 'KeyPermission KPN', 'KPN.GroupKey = KGP.Id' );
        $this->db->join( 'Key KE', 'KE.Id = KPN.KeyId' );
        $this->db->where( 'UPN.UserId ', $Id );

        $query = $this->db->get();
        return $query->result();
    }

    public function GetPermission( array $data = null )
 {
        $this->db->select( 'up.Id,u.UserName,kg.Name as RoleName' );
        $this->db->from( 'UserPermission up' );
        $this->db->join( 'Users u', 'u.Id = up.UserId' );
        $this->db->join( 'KeyGroup kg', 'kg.Id = up.KeyGroupId', 'LEFT' );
        $this->db->where( 'u.UserName', $data );
        $query = $this->db->get();

        return $query->result();
    }

    public function GetRole()
 {
        $this->db->select( 'Id,Name' );
        $this->db->from( 'KeyGroup' );
        $query = $this->db->get();

        return $query->result();
    }

    public function GetRoleById( $data )
 {
        $this->db->select( 'Id,Name' );
        $this->db->from( 'KeyGroup' );
        $this->db->where( 'Id', $data );
        $query = $this->db->get();
        //   var_dump( $data );
        exit;
        return $query->result();
    }

    public function GetRoleByName( $data )
 {
        $this->db->select( 'Id,Name' );
        $this->db->from( 'KeyGroup' );
        $this->db->where( 'Name', $data );
        $query = $this->db->get();
        return $query->result();
    }

    public function selectgroupkey( array $data = null )
 {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'Id,Name,UpdateDate' );
        $this->db->from( 'KeyGroup' );

        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function InsertKeyGroup( $data )
 {
        $res = false;
        if ( $data != null ) {
            $arr = array(
                'Deleteflag' => 0,
                'CreateUserId' => 1,
                'CreateDate' => date( 'Y-m-d' ),
                'Ordering' => 0,
                'CustomerId' => 1,
                'Name' => $data,
            );

            $this->db->insert( 'KeyGroup', $arr );
            $res = true;
        }
        return $res;

    }

    public function InsertKeyPermission( $x, $y )
 {
        $res = false;
        if ( $x != null && $y != null ) {
            $arr = array(
                'Deleteflag' => 0,
                'CreateUserId' => 1,
                'CreateDate' => date( 'Y-m-d' ),
                'Ordering' => 0,
                'CustomerId' => 1,
                'GroupKey' => $x,
                'KeyId' => $y,
            );
            $this->db->insert( 'KeyPermission', $arr );
            $res = true;
        }
        return $res;
    }

    public function InsertGroupkey( $data = array() )
 {
        // $t =   $this->db->delete( 'Cost', array( 'Id' => $data['Id'] ) );
        // $res = $this->db->insert( 'Cost', $data );
        // $data = array( 'name' => $name, 'email' => $email, 'url' => $url );
        // $data['DeleteFlag'] = 0;
        $strSQL = ' INSERT INTO Cost (DeleteFlag, CreateUserId, CreateDate,Ordering,CustomerId,StatusId,StationId,OilTypeId,MachineId,Value) ';
        $strSQL .= ' SELECT 0, CreateUserId, CreateDate,Ordering,CustomerId,1,StationId,OilTypeId,MachineId,' . $data['Value'];
        $strSQL .= ' FROM Cost AS CCT';
        $strSQL .= ' WHERE CCT.Id =' . $data['Id'];

        $query = $this->db->query( $strSQL );

        // var_dump( $query );
        exit;
        if ( isset( $query ) ) {
            $resultSQL = true;
        } else {
            return false;
        }

        return $resultSQL;
    }

    public function Deletekeyingroup( $groupkey )
 {
        $this->db->where( 'GroupKey', $groupkey );
        $this->db->delete( 'KeyPermission' );
    }

    public function UpdateGroupkey( $groupkey, $newname )
 {

        $res = false;
        $this->db->set( 'Name', $newname );
        $this->db->where( 'Id', $groupkey );
        if ( $this->db->update( 'KeyGroup' ) ) {
            $res = true;
        }
        return $res;
    }

    public function selectuser( array $data = null )
 {

        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    // $this->db->where( $key, $value );
                } else {
                    // $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'CTM.CompanyName as ชื่อบริษัท,USR.Username as ชื่อเข้าใช้งาน,' );
        $this->db->select( 'USR.FirstName as ชื่อ,USR.LastName as นามสกุล,USR.UpdateDate as วันที่อัพเดท,' );
        $this->db->select( 'USR.Id AS "rowId"' );
        $this->db->from( 'Users AS USR' );
        $this->db->join( 'Customers AS CTM', 'CTM.Id = USR.CustomerId' );
        // $this->db->join( 'Machine mc', 'mc.StationId = st.Id' );
        $this->db->where( 'USR.DeleteFlag = 0'  );
        $this->db->where( 'CTM.DeleteFlag = 0'  );

        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();
        // $rowAll = 1;
        
        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function GetUserById( $Id )
 {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        /*  select us.Id as Id, us.UserName as us, c.CusCode
        from Users us
        join Customers c ON c.Id = us.Id
        where us.Id = 2;
        */

        // $this->db->select( 'kg.Id AS "groupid",kg.name AS "groupname"' );
        $this->db->select( 'us.CustomerId,us.UserName,c.CusCode,us.FirstName,us.LastName' );
        $this->db->from( 'Users us' );
        $this->db->join( 'Customers c', 'c.Id = us.CustomerId' );
        $this->db->where( 'us.Id', $Id );

        $query = $this->db->get();
        $rowAll = $query->num_rows();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            // $query = $this->db->get();
            // $rows = $query->num_rows();

            if ( $rowAll > 0 ) {
                for ( $i = 0; $i < $rowAll; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function UpdatePassword( $id, $newpassword, $date, $userid )
 {
        $res = false;
        $this->db->set( 'Password', $newpassword );
        $this->db->set( 'UpdateDate', $date );
        $this->db->set( 'UpdateUserId', $userid );
        $this->db->where( 'Id', $id );
        if ( $this->db->update( 'Users' ) ) {
            $res = true;
        }
        return $res;
    }

    public function UpdateName( $id, $firstname, $lastname, $date )
 {
        $res = false;
        $this->db->set( 'UpdateDate', $date );
        $this->db->set( 'FirstName', $firstname );
        $this->db->set( 'LastName', $lastname );

        $this->db->where( 'Id', $id );
        if ( $this->db->update( 'Users' ) ) {
            $res = true;
        }
        return $res;
    }

}