<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_trigger extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    public function selectTrigger( array $data = null )
 {
        $result = null;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }
            }
        }

        $this->db->select( 'Id,Prefix,Interval,Length' );
        $this->db->from( 'Trigger' );
        $this->db->where( '"DeleteFlag"', 0 );

        $query = $this->db->get();
        $rows = $query->num_rows();
        // echo 'sql = '.$this->db->last_query();

        if ( $rows > 0 ) {
            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }

        return $result;
    }

    public function EditTriggerById( array $data = null )
 {
        $result = false;
        //$this->db->set( 'deleteFlag', 1 );
        $this->db->set( 'UpdateUserId', 1 );
        $this->db->set( 'UpdateDate', date( 'Y-m-d H:i:s' ) );

        $this->db->set( 'Interval', $data['Interval'] );
        $this->db->where( 'Id', $data['Id'] );
        $res = $this->db->update( 'Trigger' );
        // echo 'sql = '.$this->db->last_query();
        // var_dump( $res );
        // exit();
        return $result;
    }

    public function addTrigger( $data )
 {
        $result = 0;

        if ( $this->db->insert( 'Trigger', $data ) )
 {
            $result = $this->db->insert_id();
        }
        return $result;
    }

}