<?php
defined('BASEPATH') or exit('No direct script access allowed');

class mol_accountinvoice extends CI_Model
{

 public function __construct()
 {
  parent::__construct();
  // $this->load->database();
  $this->load->database('happystation', false, true);
 }

 public function selectAccountInvoice()
 {
  $this->db->select('SUM(AmountNet) as SumValue');
  $this->db->from('AccountInvoice');
  $this->db->where('EXTRACT(MONTH  FROM  CreateDate) = EXTRACT(MONTH FROM DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL -1 MONTH))');
  $this->db->where('EXTRACT(YEAR FROM  CreateDate) = EXTRACT(YEAR FROM DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL -1 MONTH))');
  $query = $this->db->get();

  return $query->result();
 }

 public function columnChart()
 {
  $this->db->select('SUM(Amount) as Amount, numDay');
  $this->db->from('(SELECT Amount,DATE_FORMAT(CreateDate,\'%d/%m\') as numDay FROM AccountInvoice WHERE DATE_FORMAT(PaymentedDate, \'%m\') = DATE_FORMAT(CURRENT_TIMESTAMP(), \'%m\')) as Temp');
  $this->db->group_by('numDay');
  $query = $this->db->get();
  //var_dump( $query->result() );
  // exit;
  $rows = $query->num_rows();
  if ($rows > 0) {
   for ($i = 0; $i < $rows; $i++) {
    $result['result'][$i] = $query->row_array($i);
   }
  }
  return $query->result();
 }

 public function SelectInvoice($data)
 {
  if ($data == null) {
   throw new Exception('Error Processing Request', 1);
  }
  $this->db->select('AmountNet');
  $this->db->from('AccountInvoice');
  $this->db->where('StationId', $data['stationid']);
  $this->db->order_by('Id', 'DESC');
  $this->db->limit(1);

  $query = $this->db->get();

  return $query->result();
 }
}