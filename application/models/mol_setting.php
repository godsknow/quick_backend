<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_setting extends CI_Model
 {
    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }
    //Select All data

    public function GetKey( $data)
 {
    $this->load->database( 'happystation', false, true );
        $this->db->select( 'Value' );
        $this->db->from( 'Settings' );
        $this->db->where( 'Key', $data );
        $query = $this->db->get();
        // var_dump($query->result());exit;
        return $query->result();
    }

    public function GetKeySMS( $data )
 {
    $this->load->database( 'happystation', false, true );
        $this->db->select( 'Value' );
        $this->db->from( 'Settings' );
        $this->db->where( 'Key', $data );
        $this->db->where( 'Section = \'sms\'' );

        $query = $this->db->get();
        // print_r( $query );
        // var_dump( $query->result() );
        // exit();
        return $query->result();
    }

    public function GetKeyRouteMan( $data, $cus )
 {

        $this->db->select( 'Value' );
        $this->db->from( 'Settings' );
        $this->db->where( 'Key', $data );
        $this->db->where( 'customerId', $cus );
        $query = $this->db->get();
        // print_r( $query );
        // var_dump( $query->result() );
        // exit();
        return $query->result();
    }

    public function selectSettingAll()
 {
        $this->db->select( 'CustomerId,Section,Key,Value,CreateDate,Id AS "rowId"' );
        $this->db->from( 'Settings' );
        $this->db->order_by( 'Id', 'ASC' );
        $query = $this->db->get();

        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function selectSettingDefault()
 {
        $this->db->select( 'UpdateDate as วันที่อัพเดท,Key as รหัสเฉพาะ,Value as ค่าที่ตั้ง,Value as หน่วย,' );
        $this->db->select( 'Id AS "rowId"' );
        $this->db->from( 'Settings' );
        $this->db->or_where( 'Key', 'OilPrice' );
        $this->db->or_where( 'Key', 'OilMin' );
        $this->db->or_where( 'Key', 'BankMax' );
        $this->db->order_by( 'Id', 'ASC' );
        $query = $this->db->get();

        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function getSettingDefaultById( $data )
 {
        $this->db->select( 'UpdateDate,Key,Value,' );
        $this->db->select( 'Id' );
        $this->db->from( 'Settings' );
        $this->db->or_where( 'Id', $data['Id'] );
        $query = $this->db->get();

        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function UpdateSetting( $data ) {
        $res = false;
        $this->db->set( 'Value',  $data['Value'] );
        $this->db->set( 'UpdateDate', $data['UpdateDate'] );
        $this->db->where( 'Id', $data['Id'] );
        $res = $this->db->update( 'Settings' );
        return $res;

    }

    public function selectNotice()
 {
        $result = null;

        $this->db->select( ' DATE_FORMAT(CreateDate,"%d-%m-%Y %H:%i:%s") AS CreateDate ,' );
        $this->db->select( 'Title,Message,' );
        // $this->db->select( 'Id AS "rowId"' );
        $this->db->from( 'Notification' );
        $this->db->where( 'DeleteFlag', 1 );
        $this->db->where( 'Complete', 1 );
        // $this->db->or_where( 'Key', 'BankMax' );
        $this->db->order_by( 'Id', 'DESC' );

        $query = $this->db->get();
        // var_dump( $this->db->last_query() );
        // exit();
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $result;
    }

    public function GetSettingBySection( $data ) {

        $this->db->select( 'Section,Key,Value' );
        $this->db->from( 'Settings' );
        $this->db->where( 'Section', $data );
        $query = $this->db->get();

        return $query->result();

    }

}