<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class mol_logdaily extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    public function selectLogdaily( $data = false )
 {
        $this->db->select( 'SUM(Value) as Value' );
        $this->db->from( 'logdaily' );
        $this->db->where( 'DeleteFlag', $data );

        $query = $this->db->get();

        return $query->result();
    }

    public function Logdaily()
 {
        $this->db->select( 'Id,DATE_FORMAT(CreateDate ,\'%d/%m/%y\') as Date,Value' );
        $this->db->from( 'LogDaily' );
        $this->db->where( 'DeleteFlag', false );
        // $this->db->group_by( 'FORMAT (CreateDate, \'dd-MM-yy\')' );

        $query = $this->db->get();

        return $query->result();
    }

    public function ChkLogdialy()
 {
        $this->db->select( 'Id,DATE_FORMAT(CreateDate,\'%Y-%m-%d\') as Date,Value,Capacity' );
        $this->db->from( 'LogDaily' );
        $this->db->where( 'DATE_FORMAT(CreateDate,"%Y-%m-%d")', date( 'Y-m-d' ) );
        $this->db->order_by( 'CreateDate', 'DESC' );
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

    public function DailyMachine( $data )
 {
        $this->db->select( 'ld.Id,ld.CreateDate,ld.Value,mc.Id as MachineId,mc.Name as MachineName' );
        $this->db->from( 'LogDaily ld' );
        $this->db->join( ' Machine mc', 'mc.Id = ld.MachineId' );
        $this->db->where( 'ld.CreateDate LIKE \'%'. $data['date'].'%\'' );
        $this->db->order_by( 'ld.Id', 'ASC' );

        $query = $this->db->get();

        return $query->result();
    }

    public function DailyTest()
 {
        $this->db->Select( 'Count(*) as countNum' );
        $this->db->from( 'Logdaily' );
        $query = $this->db->get();
        //var_dump( $query->result() );
        // exit;
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $query->result();
    }

    public function sumValueOneMonth()
 {
        $this->db->Select( 'Sum(Value)' );
        $this->db->from( 'LogDaily' );
        $this->db->where( ' CreateDate > DATE_SUB(now(), INTERVAL 1 MONTH)' );
        // $this->db->where( 'DATEPART(MONTH,CreateDate) - (DATEPART(MONTH,CURRENT_TIMESTAMP)) = 0' );
        //$this->db->groupBy( 'FORMAT(CONVERT(date,CreateDate),"MM/yyyy")' );
        $query = $this->db->get();
        //var_dump( $query->result() );
        // exit;
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function sumCapacityOneMonth()
 {
        $this->db->Select( 'Sum(Capacity)' );
        $this->db->from( 'LogDaily' );
        $this->db->where( ' CreateDate > DATE_SUB(now(), INTERVAL 1 MONTH)' );
        //$this->db->where( ' DATE_FORMAT( CURRENT_DATE - INTERVAL 1 MONTH, CreateDate )' );
        // $this->db->where( 'DATE_FORMAT(Month,CreateDate) - (DATE_FORMAT(MONTH,CURRENT_TIMESTAMP)) = 0' );
        //$this->db->groupBy( 'FORMAT(CONVERT(date,CreateDate),"MM/yyyy")' );
        $query = $this->db->get();
        // var_dump( $query->result() );
        // exit;
        $rows = $query->num_rows();

        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        return $result;
    }

    public function selectInVoice( array $data = null )
 {
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        if ( !is_null( $data ) ) {
            foreach ( $data as $key => $value ) {
                if ( $key != 'duration' && $key != 'offset' ) {
                    $this->db->where( $key, $value );
                } else {
                    $arrData[$key] = $value;
                }

            }
        }
        $this->db->select( 'ST.StationCode as รหัสตู้,M.Name AS ชื่อเครื่อง,L.CreateDate as วันที่,STT.Name AS สถานะ,L.Value as เงินที่รับ,L.Capacity as ปริมาณ' );
        //$this->db->select( 'L.Value,L.Capacity' );
        $this->db->from( 'LogDaily L' );
        $this->db->join( 'Status STT', 'STT.Id = L.StatusId' );
        $this->db->join( 'Station ST', 'ST.Id = L.StationId' );
        $this->db->join( 'Machine M', 'M.Id = L.MachineId' );
        $this->db->limit( 10 );
        //$query = $this->db->get();
        //  var_dump( $query->result() );
        //  exit();
        // $row  = $query->num_rows();
        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        if ( $rowAll > 0 ) {
            $result['pageinfo']['allrecord'] = $rowAll;
            if ( $arrData['duration'] == 1 ) {
                $result['pageinfo']['pageall'] = $arrData['duration'];
            } else {
                $result['pageinfo']['pageall'] = ceil( $rowAll / $arrData['duration'] );
                $this->db->limit( $arrData['duration'], $arrData['offset'] );
            }

            $query = $this->db->get();
            $rows = $query->num_rows();

            if ( $rows > 0 ) {
                for ( $i = 0; $i < $rows; $i++ ) {
                    $result['result'][$i] = $query->row_array( $i );
                }
            }
        }
        return $result;
    }

    public function lineChart()
 {
        $this->db->select( 'MHE.Name as CODE,DATE_FORMAT(LDY.CreateDate, \'%d\') as time,LDY.Value as value' );
        $this->db->from( 'LogDaily as LDY' );
        $this->db->join( 'Machine MHE', 'LDY.MachineId = MHE.Id', 'LEFT' );

        $this->db->where( 'EXTRACT(MONTH FROM LDY.CreateDate) - (EXTRACT(MONTH FROM CURRENT_TIMESTAMP())) = 0' );
        $this->db->order_by( 'time', 'ASC' );
        $query = $this->db->get();
        $rows = $query->num_rows();
        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        //var_dump( $query->result() );
        // exit;
        return $query->result();
    }

    public function lineChartDay()
 {
        $this->db->Select( 'Sum(Value) AS "value"' );
        $this->db->from( 'LogDaily' );
        $this->db->where( 'EXTRACT(DAY FROM CreateDate) - (EXTRACT(DAY FROM CURRENT_TIMESTAMP())) = 0' );
        $query = $this->db->get();
        $rows = $query->num_rows();
        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }
        // var_dump( $query->result() );
        // exit;
        return $query->result();
    }

    public function lineChart_Day()
 {
        $this->db->Select( 'Sum(Value)' );
        $this->db->from( 'LogDaily' );
        $this->db->where( 'EXTRACT(DAY FROM CreateDate) - (EXTRACT(DAY FROM CURRENT_TIMESTAMP())) = 0' );
        $query = $this->db->get();
        //var_dump( $this->db->get() );
        // exit;
        $rows = $query->num_rows();
        if ( $rows > 0 ) {
            for ( $i = 0; $i < $rows; $i++ ) {
                $result['result'][$i] = $query->row_array( $i );
            }
        }

        return $query->result();
    }
}