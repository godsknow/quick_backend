<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_qrcode extends CI_Model
 {

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        //$this->load->database( 'happystation', FALSE, TRUE );
        //echo '<br/>testdb'.$this->load->db_connect( TRUE );
        $db_obj = $this->load->database( 'happystation', false );
        //$this->db_connect( TRUE );
        //if ( $db_obj->conn_id ) {
        //do something
        //} else {
        //echo 'Unable to connect with database with given db details.';
        //}
    }

    public function Addqrcode( array $data = null )
 {
        $res = false;
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }

        $arr = array(
            'DeleteFlag' => 0,
            'CreateDate' => date( 'Y-m-d H:i:s' ),
            'CreateUserId' => 1,
            'Ordering' => 0,
            'StatusId' => 1,
            'Text' => $data['data'],
            'ImageName' => $data['qr_image'],
            'Expired_Date' => date( 'Y-m-d H:i:s', strtotime( '+5 minutes', strtotime( date( 'Y-m-d H:i:s' ) ) ) )
        );

        if ( $this->db->Insert( 'QrCode', $arr ) )
 {
            $res = true;
        }
        return $res;

    }

    public function SelectQrCode( array $data = null )
 {
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }

        $this->db->select( 'ImageName,Expired_Date' );
        $this->db->from( 'QrCode' );
        $this->db->where( 'text', $data['text'] );
        $query = $this->db->get();

        return $query->result();

    }
}