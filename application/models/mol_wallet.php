<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_Wallet extends CI_Model
 {

    private $result = array();

    public function __construct()
 {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happywallet', false, true );
    }

    //Select All data for APi

    public function GetWallet( $data ) {
        if ( $data == null ) {
            throw new Exception( 'Data Is Null', 1 );
        }
        $this->db->select( 'Id,CreateUserId,StatusId,customerId,CardNumber,StartDate,EndDate,Value,WalletUserId' );
        $this->db->from( 'Wallet' );
        $this->db->where( 'DeleteFlag = 0' );
        $query = $this->db->get();
        return $query->result();
    }

    // public function GetWalletByUserName( $data ) {
    //     if ( $data == null ) {
    //         throw new Exception( 'Data Is Null', 1 );
    //     }
    //     $this->db->select( 'Id,CreateUserId,StatusId,customerId,username,password,PhoneNumber,Email,LastActive' );
    //     $this->db->from( 'Wallet' );
    //     $this->db->where( 'DeleteFlag = 0' );
    //     $this->db->where( 'UserName', $data );
    //     $query = $this->db->get();
    //     return $query->result();

    // }

    public function GetWalletUserByUserId() {
        if ( $data == null ) {
            throw new Exception( 'Data Is Null', 1 );
        }
        $this->db->select( 'Id,CreateUserId,StatusId,customerId,CardNumber,StartDate,EndDate,Value' );
        $this->db->from( 'Wallet' );
        $this->db->where( 'DeleteFlag = 0' );
        $this->db->where( 'WalletUserId', $data );
        $query = $this->db->get();
        return $query->result();

    }

    public function UpdateWalletUser( $data ) {

        if ( $data == null ) {
            throw new Exception( 'Data Is NULL', 1 );
        }
        $res = false;
        $strQuery = 'UPDATE Wallet SET UpdateDate = '.date( 'Y-m-d H:i:s' ).' Value = '.$data['Value'].' where WalletUserId ='.$data['Id'].'AND DeleteFlag = 0';

        if ( $this->db->query( $strQuery ) ) {
            $res = true;
        }
        return $res;
    }

    public function InsertWallet( array $data = null ) {

        if ( $data == null ) {
            throw new Exception( 'Data Is NULL', 1 );
        }
        $res = false;
        $array = array(
            'DeleteFlag' => 0,
            'CreateUserId' => 1,
            'CreateDate' => date( 'Y-m-d H:i:s' ),
            'Ordering' => 0,
            'StatusId' => 1,
            'CardNumber' => $data['CardNumber'],
            'StartDate' => date( 'Y-m-d H:i:s' ),
            'EndDate' => date( 'Y-m-d H:i:s', strtotime( '+1 years', date() ) ),
            'Value' => $data['Value'],
            'WalletUserId' => $data['WallUserId'][0]->Id,
        );

        if ( $this->db->Insert( 'Wallet', $array ) ) {
            $res = true;
        }

        return $res;
    }

    public function DeleteWalletUser( $data ) {

        if ( $data == null ) {
            throw new Exception( 'Data Is NULL', 1 );
        }
        $res = false;
        $strQuery = 'UPDATE Wallet SET DeleteFlag = 0   where Id ='.$data['Id'];

        if ( $this->db->query( $strQuery ) ) {
            $res = true;
        }
        return $res;
    }
}