<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Mol_requestrouteman extends CI_Model
 {
    public function __construct() {
        parent::__construct();
        // $this->load->database();
        $this->load->database( 'happystation', false, true );
    }

    public function selectRequest( array $data = null ) {
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }
        $this->db->select( 'Id' );
        $this->db->from( 'RequestRouteMan' );
        $this->db->where( 'Action', $data );
        $this->db->order_by( 'Id', 'Desc' );
        $this->db->limit( 1 );
        $query = $this->db->get();
        var_dump( $query->result() );
        exit;
        return $query->result();
    }

    public function InsertRequest( $data ) {
        $res = false;
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }

        $arr = array(
            'DeleteFlag' => 0,
            'CreateDate' => date( 'Y-m-d H:i:s' ),
            'Ordering' => 0,
            'CreateUserId' => 1,
            'CustomerId' => 1,
            'StatusId' => 1,
            'Expired_Date' => date( 'Y-m-d H:i:s', strtotime( '+5 minutes', strtotime( date( 'Y-m-d H:i:s' ) ) ) ),
            'Approve_Date' => date( 'Y-m-d H:i:s' ),
            'Action' => $data['Action'],
            'MachineId' => $data['MachineId'],
            'RouteManId' => $data['RoutemanId'],
        );
        if ( $this->db->Insert( 'RequestRouteMan', $arr ) ) {
            $res = true;
        }

        return $res;
    }

    public function InsertLogRouteMan( $data ) {
        $res = false;
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }

        $arr = array(
            'DeleteFlag' => 0,
            'CreateDate' => date( 'Y-m-d H:i:s' ),
            'Ordering' => 0,
            'CreateUserId' => 1,
            'CustomerId' => 1,
            'StatusId' => 1,
            'StationId' => 1,
            'ActionDate' => date( 'Y-m-d H:i:s' ),
            'IsApprove' => 0,
            'RequestRouteManId' => $data,
        );

        if ( $this->db->Insert( 'LogRouteMan', $arr ) ) {
            $res = true;
        }
        return $res;
    }

    public function selectRequestByToken( $data ) {
        if ( $data == null ) {
            throw new Exception( 'Error Processing Request', 1 );
        }
        $this->db->select( 'Token,Expired_Date' );
        $this->db->from( 'RequestRouteMan' );
        $this->db->where( 'MachineId', $data['machineid'] );
        $this->db->where( 'Token ', $data['token'] );
        $this->db->where( 'Expired_Date >= \''.date( 'Y-m-d H:i:s' ).'\'' );
        $query = $this->db->get();
        return $query->result();
    }

}