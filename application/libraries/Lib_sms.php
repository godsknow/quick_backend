<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Lib_sms {
    public function __construct()
 {
        // Do something with $params
        // $this->load->library( 'session' );
        //   $this->CI->load->library( 'session' );
        $this->CI = &get_instance();
        $this->CI->load->model( array( 'mol_setting' ) );

    }

    public function OTP() {
        $Parameter['url'] = 'https://otp.thaibulksms.com/v1/otp/request';
        $Parameter['header'] = array(
            'accept: application/json',
            'content-type: application/x-www-form-urlencoded'
        );

        $secret = '';
        $key = '';
        $destination = '';

    }

    public function SMS( $data ) {

        $Parameter['url'] = 'https://www.thaibulksms.com/sms_api.php';
        $Parameter['header'] = array(
            'accept :application/xml',
            'Content-Type: application/x-www-form-urlencoded',
        );
        $user = $this->CI->mol_setting->GetKeySMS( 'UserName' );
        $pass = $this->CI->mol_setting->GetKeySMS( 'Password' );
        $sender = $this->CI->mol_setting->GetKeySMS( 'Sender' );

        $Username	 = $user[0]->Value;
        $Password	 = $pass[0]->Value;
        $PhoneList	 = $data['phone'];
        $Message	 =  $data['msg'] ;
        $Sender		 = $sender[0]->Value;
        $date = date( 'ymdhi' );

        $Parameter['data']	 = 	"username=$Username&password=$Password&msisdn=$PhoneList&message=$Message&sender=$Sender&ScheduledDelivery=$date";
        return $this->Send( $Parameter );
    }

    public function Send( $data ) {

        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $data['url'] );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $data['header'] );
        // curl_setopt( $ch, CURLOPT_HEADER, $headers );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        // Disabling SSL Certificate support temporarly
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_POSTFIELDS,   $data['data'] );

        $result = curl_exec( $ch );

        if ( $result === false ) {
            //
            die( 'Curl failed: ' . curl_error( $ch ) );
        }

        // Close connection
        curl_close( $ch );

        return $this->smstojson( $result );
    }

    public function smstojson( $data ) {

        $fileContents =  $data ;
        $fileContents = str_replace( array( '\n', '\r', '\t' ), '', $fileContents );
        $fileContents = trim( str_replace( '"', "'", $fileContents ) );
        $simpleXml = simplexml_load_string( $fileContents );
        $json = json_encode( $simpleXml );
        $dataResult =  json_decode( $json );

        return $dataResult;
    }
}
?>