<?php
defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Lib_template
 {

    public function __construct( $params )
 {
        // Do something with $params
    }

    public function getTemplateVar()
 {
        // $arrTemplate['footer']['Copyright'] = '<strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.';
        $arrTemplate['footer']['Copyright'] = 'Copyright © 2019-2020 Wealth Vending Solution Co.,Ltd. All rights reserved.';
        $arrTemplate['footer']['Version'] = '1.1.0';

        return $arrTemplate;
    }

    public function getPageHome()
 {
        $strHome = 'dashboard/dashboardinvoice';
        return $strHome;
    }

    public function getTemplateMenu()
 {
        $arrMenu = array(
            array(
                'icon' => 'fas fas fa-home',
                'name' => 'หน้าหลัก',
                'parent' => null,
                'permissiom' => 'Home',
                'link' => '',
            ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'Real-time Dashboard',
                'parent' => 'หน้าหลัก',
                'permissiom' => 'Home',
                'link' => 'homeDashboard/dashboard' ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'จัดการแจ้งเตือน',
                'parent' => 'หน้าหลัก',
                'permissiom' => 'Home',
                'link' => 'Notification/GetNotification' 
            ),
            array(
                'icon' => 'fas fa-gas-pump',
                'name' => 'ตู้น้ำมัน',
                'parent' => null,
                'permissiom' => 'Station',
                'link' => '',
            ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'เส้นทางเติมน้ำมัน',
                'parent' => 'ตู้น้ำมัน',
                'permissiom' => 'Station',
                'link' => 'routegroup/showrouteGroup' 
            ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'ตารางข้อมูลตู้น้ำมัน',
                'parent' => 'ตู้น้ำมัน',
                'permissiom' => 'Station',
                'link' => 'station/StationData'
                ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'ยืนยันการสั่งน้ำมัน',
                'parent' => 'ตู้น้ำมัน',
                'permissiom' => 'Station',
                'link' => 'station/showListOrder' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ประวัติการสั่งน้ำมัน',
            'parent' => 'ตู้น้ำมัน',
            'permissiom' => 'Station',
            'link' => 'station/ListOrderHistory'
            ),
            array(
            'icon' => 'fas fa-tachometer-alt',
            'name' => 'กระดานแสดงข้อมูล',
            'parent' => null,
            'permissiom' => 'Dashboard',
            'link' => '' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ข้อมูลผลประกอบการ',
            'parent' => 'กระดานแสดงข้อมูล',
            'permissiom' => 'Dashboard',
            'link' => 'dashboard/dashboardinvoice' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ข้อมูลตู้น้ำมัน',
            'parent' => 'กระดานแสดงข้อมูล',
            'permissiom' => 'Dashboard',
            'link' => 'dashboard/dashboardstation'
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ข้อมูลรายได้ต่อเดือน',
            'parent' => 'กระดานแสดงข้อมูล',
            'permissiom' => 'Dashboard',
            'link' => 'dashboard/dashboardprice' 
            ),
            array(
            'icon' => 'fas fa-user',
            'name' => 'ต้นทุนน้ำมัน',
            'parent' => null,
            'permissiom' => 'COST',
            'link' => '' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'แสดงต้นทุนน้ำมัน',
            'parent' => 'ต้นทุนน้ำมัน',
            'permissiom' => 'COST',
            'link' => 'cost/stationcost' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ประวัติข้อมูลปริมาณน้ำมัน',
            'parent' => 'ต้นทุนน้ำมัน',
            'permissiom' => 'COST',
            'link' => 'cost/costhistory' 
            ),
            array(
            'icon' => 'fas fa-money-check-alt',
            'name' => 'ราคาน้ำมัน',
            'parent' => null,
            'permissiom' => 'PRICE',
            'link' => '' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'แสดงราคาน้ำมัน',
            'parent' => 'ราคาน้ำมัน',
            'permissiom' => 'PRICE',
            'link' => 'price/stationprice' 
            ),
            array(
            'icon' => 'fas fa-user',
            'name' => 'สมาชิก',
            'parent' => null,
            'permissiom' => 'MEMBER',
            'link' => '' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ข้อมูลสมาชิก',
            'parent' => 'สมาชิก',
            'permissiom' => 'MEMBER',
            'link' => 'member/memberlist' 
            ),
            array(
            'icon' => 'fas fa-user',
            'name' => 'การจัดการน้ำมัน',
            'parent' => null,
            'permissiom' => 'STOCK',
            'link' => '' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ทิศทางน้ำมัน',
            'parent' => 'การจัดการน้ำมัน',
            'permissiom' => 'STOCK',
            'link' => 'StockMove/GetStockMove' 
            ),
            array(
            'icon' => 'fas fa-users',
            'name' => 'รายงาน',
            'parent' => null,
            'permissiom' => 'REPORT',
            'link' => '' ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'รายงานประจำวัน',
            'parent' => 'รายงาน',
            'permissiom' => 'REPORT',
            'link' => 'report/reportdaily' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'รายงานประจำเดือน',
            'parent' => 'รายงาน',
            'permissiom' => 'REPORT',
            'link' => 'report/reportMonth' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'รายงานยอดขายเงินสด',
            'parent' => 'รายงาน',
            'permissiom' => 'REPORT',
            'link' => 'report/reportCash' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'รายงานยอดขาย QR-code',
            'parent' => 'รายงาน',
            'permissiom' => 'REPORT',
            'link' => 'report/reportQr' 
            ),
            array(
            'icon' => 'fas fa-user',
            'name' => 'ผู้ใช้งาน',
            'parent' => null,
            'permissiom' => 'USER',
            'link' => '' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ข้อมูลผู้ใช้งาน',
            'parent' => 'ผู้ใช้งาน',
            'permissiom' => 'USER',
            'link' => 'manageuser/Userlist' 
            ),
            array(
            'icon' => 'fas fa-cogs',
            'name' => 'ตั้งค่าข้อมูล',
            'parent' => null,
            'permissiom' => 'SETTING',
            'link' => '' 
            ),
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ข้อมูลลูกค้า',
            'parent' => 'ตั้งค่าข้อมูล',
            'permissiom' => 'SETTING',
            'link' => 'customer/ShowCustomer' 
            ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'ข้อมูลพื้นฐานตู้',
                'parent' => 'ตั้งค่าข้อมูล',
                'permissiom' => 'SETTING',
                'link' => 'customer/showSettingDefault' 
            ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'ตั้งค่าข้อมูลผู้ใช้งาน',
                'parent' => 'ตั้งค่าข้อมูล',
                'permissiom' => 'SETTING',
                'link' => 'Manageuser/Userlist' 
            ),            
            array(
            'icon' => 'fas fa-angle-right',
            'name' => 'ข้อมูลเบื้องต้น',
            'parent' => 'ตั้งค่าข้อมูล',
            'permissiom' => 'SETTING',
            'link' => 'station/stationlist' 
            ),

            array(
            'icon' => 'fas fa-cogs',
            'name' => '--------',
            'parent' => null,
            'permissiom' => 'Development',
            'link' => '' 
            ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'แสดงการตั้งค่าข้อมูล',
                'parent' => '--------',
                'permissiom' => 'Development',
                'link' => 'customer/showsettingdata' 
            ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'ตั้งค่ากลุ่มผู้ใช้งาน',
                'parent' => '--------',
                'permissiom' => 'Development',
                'link' => 'manageuser/GetRole' 
            ),
            array(
                'icon' => 'fas fa-angle-right',
                'name' => 'แสดงบอร์ด',
                'parent' => '--------',
                'permissiom' => 'Development',
                'link' => 'station/showboard' 
            ),
        );
        return $arrMenu;
    }
}