<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lib_crypted {
    private $CI;
    public function __construct()
    {
        // Do something with $params

        //Encryption
        $this->CI = &get_instance();
        $this->CI->load->library('encrypt');   
    }
    public function Encrypt_password($password,$key){

        $plaintext = $password;
		$cipher = "AES-128-CBC";
		$key = $key;
		$options=OPENSSL_RAW_DATA;
		$ivlen = openssl_cipher_iv_length($cipher);
		$iv = openssl_random_pseudo_bytes($ivlen);
		$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options, $iv);
		$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

        return $ciphertext;
     }
     public function Decrypt_password($password,$key){
        $cipher = "AES-128-CBC";
        $options=OPENSSL_RAW_DATA;
        $c = base64_decode($password);
		$ivlen = openssl_cipher_iv_length($cipher);
		$iv = substr($c, 0, $ivlen);
		$hmac = substr($c, $ivlen, $sha2len=32);
		$ciphertext_raw = substr($c, $ivlen+$sha2len);
		$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        return $original_plaintext;
     }

    // public function genEncrypted($pword,$strKey)
    // {
    //     $Enpassword = $this->CI->encrypt->encode($pword,$strKey) ;

    //     return $Enpassword;
    // }

    // public function genDncrypted($pword,$strKey)
    // {
    //     $data = $this->CI->encrypt->decode($pword,$strKey) ;
    //     $da = $this->CI->encrypt->decode($data,$strKey);
    //     var_dump($da);
    //     exit();
    //     return $data;
    // }
}

?>