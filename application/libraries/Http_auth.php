<?php
if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}
/**
 * Http_auth class
 *
 * Handles http authentication check.
 *
 * @package     Project
 * @subpackage  Application
 * @category    Libraries
 * @author      Author
 * @link        void
 */
class Http_auth {
    public $CI;
    public $username;
    public $password;
    public $realm;
public function __construct($config)
    {
        // Initialize the username and password against which authentication
        // check is to be done.
        $this->username = $config['username'];
        $this->password = $config['password'];
        $this->realm = isset($config['realm']) ? $config['realm'] : 'Restricted area';
        $this->CI = & get_instance();
    }
/**
     * Authenticates user.
     *
     * @access public
     * @param  void
     * @return void
     */
    public function authenticate()
    {
        $php_auth_digest = $this->CI->input->server('PHP_AUTH_DIGEST');
if ( ! $php_auth_digest
// Analyze the PHP_AUTH_DIGEST variable.
            || ! ($data = $this->_http_digest_parse()) || $this->username !== $data['username']
// Validate auth data.
            || ! $this->_validate_auth_response($data))
        {
            $this->_send_authentication_headers();
        }
    }
/**
     * Validates authentication info entered.
     *
     * @access private
     * @param  array $data
     * @return bool
     */
    private function _validate_auth_response($data)
    {
        // Generate the valid response.
        $A1 = md5($data['username'] . ':' . $this->realm . ':' . $this->password);
        $A2 = md5($this->CI->input->server('REQUEST_METHOD') . ':' . $data['uri']);
        $valid_response = md5($A1 . ':' . $data['nonce'] . ':' . $data['nc']
            . ':' . $data['cnonce'] . ':' . $data['qop'] . ':' . $A2);
return ($data['response'] !== $valid_response) ? FALSE : TRUE;
    }
/**
     * Send authentication headers to display authenticaltion dialog.
     *
     * @access private
     * @param  void
     * @return void
     */
    private function _send_authentication_headers()
    {
        header('HTTP/1.1 401 Unauthorized');
        header('WWW-Authenticate: Digest realm="' . $this->realm . '",qop="auth",nonce="'
            . uniqid() . '",opaque="' . md5($this->realm) . '"');
// Control comes to below part if cancel button is clicked.
        exit('Cancel action selected.');
    }
/**
     * Function to parse the http auth header.
     *
     * @access private
     * @param  void
     * @return mixed
     */
    private function _http_digest_parse()
    {
        $txt = $this->CI->input->server('PHP_AUTH_DIGEST');
// Protect against missing data.
        $needed_parts = [
            'nonce' => 1,
            'nc' => 1,
            'cnonce' => 1,
            'qop' => 1,
            'username' => 1,
            'uri' => 1,
            'response' => 1
        ];
        $data = [];
        $keys = implode('|', array_keys($needed_parts));
        preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@',
            $txt, $matches, PREG_SET_ORDER);
foreach ($matches as $match)
        {
            $data[$match[1]] = $match[3] ? $match[3] : $match[4];
            unset($needed_parts[$match[1]]);
        }
return $needed_parts ? FALSE : $data;
    }
}
/* End of file Http_auth.php */
/* Location: ./application/libraries/Http_auth.php */