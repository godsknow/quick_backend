<?php

class lib_postcurl {

    public function postCURL( $_url, $_param ) {

        $postData = '';
        //create name value pairs seperated by &
        foreach ( $_param as $k => $v ) {

            $postData .= $k . '='.$v.'&';

        }
        rtrim( $postData, '&' );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $_url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
        curl_setopt( $ch, CURLOPT_HEADER, false );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $postData );

        $output = curl_exec( $ch );

        curl_close( $ch );

        return $output;
    }
}
?>