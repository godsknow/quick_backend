<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lib_trigger
{

    public $flagRes = false;

    public function __construct()
    {
    // Do something with $params
    // $this->load->library('session');
    $this->CI = &get_instance();
    //   $this->CI->load->library('session');
    $this->CI->load->model('mol_trigger');
    }

    public function getTriggerPurchase($arrdata = array())
    {
        $arrdata['Type'] = 'purchase';
        $arrdata['Month'] = date("m");
        $arrdata['Year'] = date("y");
        // $this->db->where( '"Month"',date("m")  );
        // $this->db->where( '"Year"',date("y")  );

        $arrRes = $this->CI->mol_trigger->selectTrigger($arrdata);
        if (!is_null($arrRes) ) 
        {
            $triggernumber = '0000000000'.strval($arrRes['result'][0]['Interval']);
            $triggernumber = substr($triggernumber, -1*intval($arrRes['result'][0]['Length']) );
            $trigger = $arrRes['result'][0]['Prefix'].date("y").date("m").$triggernumber;
            // echo '<br/>purchase = '.$trigger;
            
            $arrUpdate['Id'] = $arrRes['result'][0]['Id'];
            $arrUpdate['Interval'] = intval($arrRes['result'][0]['Interval'])+1;
            $flagRes = $this->CI->mol_trigger->EditTriggerById($arrUpdate);

            // echo '<br/>resUpdate = '.$flagRes;exit();
        }else{
            $trigger = $this->addTriggerPurchase();
        }

        return $trigger;

    }

    private function addTriggerPurchase($arrdata = array())
    {
        $trigger = '-';

        $arrdata['Type'] = 'purchase';
        $arrdata['Month'] = date("m",strtotime("-1 month") );
        $arrdata['Year'] = date("y");

        $arrRes = $this->CI->mol_trigger->selectTrigger($arrdata);

        if (!is_null($arrRes) ) 
        {
            $arrAdd['Prefix'] = $arrRes['result'][0]['Prefix']; 
            $arrAdd['Length'] = $arrRes['result'][0]['Length']; 
            $arrAdd['DeleteFlag'] = 0; 
            $arrAdd['CreateUserId'] = 1;
            $arrAdd['CreateDate'] = date("Y-m-d H:i:s");
            $arrAdd['Ordering'] = 0;
            $arrAdd['Interval'] = 2;
            $arrAdd['Type'] = 'purchase';
            $arrAdd['Month'] = date("m");
            $arrAdd['Year'] = date("y");            

            $insertID = $this->CI->mol_trigger->addTrigger($arrAdd);
            if(intval($insertID) > 0)
            {
                $triggernumber = substr('00000000001', -1*intval($arrRes['result'][0]['Length']) );
                $trigger = $arrRes['result'][0]['Prefix'].date("y").date("m").$triggernumber;  
            }
          
        }
        unset($arrdata);
        unset($arrRes);
        unset($arrAdd);

        return $trigger;
    }

    
}