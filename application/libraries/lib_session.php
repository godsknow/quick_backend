<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lib_session
{

 public $flagRes = false;

 public function __construct()
 {
  // Do something with $params
  // $this->load->library('session');
  $this->CI = &get_instance();
  $this->CI->load->library('session');
  $this->CI->load->model('mol_manage_user');
 }

    public function fakeSession($arrSession = array())
    {
        $arrPermission = $this->CI->mol_manage_user->GetRolePermissionbyId(1);

        if (count($arrSession) == 0) 
        {
            $newSession = [
            'username'  => 'admin',
            'email'     => 'rapee.wvs@hmail.com',
            'customer'  => '1',
            'key'       => $arrPermission,
            'logged_in' => true,
            ];
        }
        // echo '<br/>arrPermission=';
        // var_dump($newSession);
        // exit();          
        $flagRes = $this->setSession($newSession);

        return $flagRes;

    }

 public function setSession($arrSession = array())
 {
  if (isset($arrSession)) 
  {
   $this->CI->session->set_userdata($arrSession);
   $flagRes = true;

   ///  var_dump($arrSession);
   // exit();
  }
  return $flagRes;

 }

 public function getSession()
 {
  // $arrTemplate['footer']['Copyright'] = '<strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
  // reserved.';
  // $arrTemplate['footer']['Version'] = '3.0.5';

  // return $arrTemplate;
 }

 public function delSession()
 {
  $flagLogin = $this->CI->session->userdata('logged_in');
  if ($flagLogin) {
   // $session->destroy();
   $flagRes = $this->CI->session->sess_destroy();
   // $flagRes = TRUE;
  }

  return $flagRes;
 }

 public function chkSession()
 {
  // var_dump($this->CI->session->has_userdata('logged_in'));exit();
  if ($this->CI->session->has_userdata('logged_in')) {
   $flagRes = true;
  } else {
   redirect('permission/login', 'refresh');
  }
  return $flagRes;
 }

}