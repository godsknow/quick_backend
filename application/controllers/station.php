<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Station extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
		//load lib CI
		$this->load->helper('url');

		//load lib custom
		$arrBlank = array();
		$this->load->library(array('lib_session','lib_trigger') );
		$this->load->library('lib_template',$arrBlank);

		//Load model 
		$this->load->model(array('mol_station','mol_oiltype','mol_machine','mol_oilorder','mol_routegroup'));

		//check session
		$this->lib_session->chkSession();		

    }	 	
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function stationprice()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_station->selectStationPrice();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array('header'=>'StationLocation');
		$arrFooter['footer'] = $setTemplate['footer'];

		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/content',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function stationcost()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_station->selectStationCost();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array('header'=>'StationLocation');
		$arrFooter['footer'] = $setTemplate['footer'];

		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/content',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function stationlist()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'SETTING';
		$arrMenu['page'] = 'ข้อมูลเบื้องต้น';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_station->selectStationList();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ข้อมูลเบื้องต้น',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'station/StationData',
											'page'=>'ข้อมูลเบื้องต้น'
										);
		$arrFooter['footer'] = $setTemplate['footer'];

		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/content',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function stationlocation()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_station->selectStationList();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array('header'=>'StationLocation');
		$arrFooter['footer'] = $setTemplate['footer'];

		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/content',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function stationaddform()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();

		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_station->selectStationList();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array('header'=>'stationaddform');
		$arrFooter['footer'] = $setTemplate['footer'];
		
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/form',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function StationData(array $data = null)
	{		
		
		$arrContent['check'] = $data;	
		
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'Station';
		$arrMenu['page'] = 'ตารางข้อมูลตู้น้ำมัน';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_station->selectStationAll();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ตู้น้ำมัน',
											'parent'=>'ตู้น้ำมัน',
											'link'=>'station/StationData',
											'page'=>'ตารางข้อมูลตู้น้ำมัน'
										);

		$arrFooter['footer'] = $setTemplate['footer'];

		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/station',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}
	
	public function AddStation()
	{
		$location['location'] = $this->input->post('LocationName');	
		$location['lat'] = $this->input->post('Latitude');
		$location['lng'] = $this->input->post('Longitude');

		$arrlocation = array(
			'CreateDate' => date('Y-m-d'),
			'DeleteFlag' => false,
			'CreateUserId' => 1,
			'Ordering' => 0,
			'CustomerId' => 1,
			'StatusId' => 1,
			'Name' => $location['location'],
			'Latitude' => $location['lat'],
			'Longitude' => $location['lng']
		);

		$check['location'] = $this->mol_station->InsertLocation($arrlocation);
		// if($check['location'] != false){
			
			$data['StationName'] = $this->input->post('StationName');
			$data['StationCode'] = $this->input->post('StationCode');
			$data['Ip_Address'] = $this->input->post('Ip_Address');
			$data['Remaining'] = $this->input->post('Remaining');
			$data['SectionId'] = $this->input->post('SectionId');
			$data['AppReleaseVersion'] = $this->input->post('AppReleaseVersion');
			$data['AppCurrentVersion'] = $this->input->post('AppCurrentVersion');
			$data['MaxCapacity'] = $this->input->post('MaxCapacity');
			$data['Balance'] = $this->input->post('Balance'); 
			$data['RouteGroupId'] = $this->input->post('grouproute');
			// var_dump($data['RouteGroupId'] ); exit();
			$locationresult = $this->mol_station->SelectLocationByName($location)->result();
			// var_dump($locationresult); exit;
			foreach ($locationresult as $key) {
						
					$arrStation = array(
						'CreateDate' => date('Y-m-d'),
						'DeleteFlag' => false,
						'CreateUserId' => 1,
						'Ordering' => 0,
						'CustomerId' => 1,
						'StatusId' => 1,
						'StationCode' => $data['StationCode'],
						'Name' => $data['StationName'],
						'IP_Address' => $data['Ip_Address'],
						'LocationId' => $key->Id,
						'LastRefill' => date('Y-m-d'),
						'Remaining' => $data['Remaining'],
						'SectionId' => $data['SectionId'],
						'StartUpdateTime'=>  date('Y-m-d'),
						'EndUpdateTime'=>  date('Y-m-d'),
						'AppReleaseVersion' => $data['AppReleaseVersion'],
						'AppCurrentVersion' => $data['AppCurrentVersion'],
						'LastUpdate' => date('Y-m-d'),
						'MaxCapacity' => $data['MaxCapacity'],
						'Balance' => 0,
						'RouteGroupId' => $data['RouteGroupId'],
					);
				
					$check['Station'] = $this->mol_station->InsertStation($arrStation);
					// var_dump($key->Id); exit;
				}
				$this->StationData($check);
		// }
	}

	public function setstation()
	{
		$arrContent['nameRoute'] = array();

		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'Station';
		$arrMenu['page'] = 'ตารางข้อมูลตู้น้ำมัน';
		$arrData['duration'] = 20;
		$arrData['offset'] = 0;

		if(isset($_GET['id'])&& intval($_GET['id']) > 0 ) {
			$arrData['id'] = $_GET['id'];
			$arrContent['data'] = $this->mol_oiltype->selectSetOilType($arrData)->result();	
			
		}
		$arrContent['CountNo'] = $this->mol_station->CountStation();
		$arrContent['nameRoute'] = $this->mol_routegroup->SelectNameRouteGroup();	
		//var_dump($arrContent['nameRoute']);exit();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ตู้น้ำมัน',
											'parent'=>'ตู้น้ำมัน',
											'link'=>'station/setstation',
											'page'=>'แสดงข้อมูลน้ำมัน'
										);
		$arrFooter['footer'] = $setTemplate['footer'];
		
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/setstation',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}
	public function editStation()
	{
			$setTemplate = $this->lib_template->getTemplateVar();
			$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
			$arrMenu['permissiom'] = 'Station';
			$arrMenu['page'] = 'ตารางข้อมูลตู้น้ำมัน';
			$arrData['duration'] = 20;
			$arrData['offset'] = 0;
	
			if(isset($_GET['id'])&& intval($_GET['id']) > 0 ) {
				$arrData['id'] = $_GET['id'];
				$arrContent['data'] = $this->mol_oiltype->selectSetOilType($arrData)->result();	
				$arrContent['selectEdit'] = $this->mol_station->SelectStationById($arrData);
				//var_dump($arrContent['selectEdit']);exit();
				$arrContent['id']= $arrData['id'];
			}
			$arrMenu['customer'] = array('happyoil','wealthoil');
			$arrContent['template'] = array(	'header'=>'ตู้น้ำมัน',
												'parent'=>'ตู้น้ำมัน',
												'link'=>'station/editStation',
												'page'=>'แสดงข้อมูลน้ำมัน'
											);
			$arrFooter['footer'] = $setTemplate['footer'];
			
			$this->load->view('adminlte/menu',$arrMenu);
			$this->load->view('adminlte/navbar');
			$this->load->view('adminlte/editstation',$arrContent);
			$this->load->view('adminlte/footer',$arrFooter);
	}
	public function setEditStation()
	{		
			$setTemplate = $this->lib_template->getTemplateVar();
			$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
			if(isset($_POST)){
			//var_dump($_POST['value']);exit();
			$arrData['Id'] = $_POST['row_id'];
			$arrData['Name'] = $_POST['StationName'];
			$arrData['StationCode'] = $_POST['StationCode'];
			$arrData['LocationName'] = $_POST['LocationName'];
			$arrData['MaxCapacity'] = $_POST['MaxCapacity'];
			$arrData['Remaining'] = $_POST['Remaining'];
			$arrData['LocationId'] = $_POST['LocationId'];

			$resUpdate = $this->mol_station->EditStationById($arrData);
			$res = $this->mol_station->EditLocationById($arrData);
			//var_dump($resUpdate); exit();
			// $resUpdate = $this->mol_cost->InsertCost($arrData);
		}
		echo 'redirect';
		redirect('station/StationData', '');
	}
	public function showboard()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'Development';
		$arrMenu['page'] = 'แสดงบอร์ด';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_machine->showDraftMachineAll();

		//$arrContent['data']['linkedit'] = 'createboard';
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'แสดงข้อมูลบอร์ด',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'station/showboard',
											'page'=>'แสดงบอร์ด'
										);
										
		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/datatable',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}
	
	// ยังไม่ได้ต่อ database
	public function createboard()
	{
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'SETTING';
		$arrMenu['page'] = 'แสดงบอร์ด';
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
	//	$arrContent['data'] = $this->mol_machine->selectSettingAll();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'แสดงการสร้างบอร์ด',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'station/createboard',
											'page'=>'แสดงการสร้างบอร์ด'
										);
		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/createboard',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function insertboard()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
	
 		if(isset($_POST)){
			//var_dump($_POST['value']);exit();
			$arrData['BoardId'] = $_POST['BoardId'];
			$arrData['DeleteFlag'] = 0;
			$arrData['CreateDate'] = date("Y-m-d H:i:s");
			$arrData['Ordering'] = 0;
			$arrData['StatusId'] = 3;
			$arrData['StationId'] = 1;
			$arrData['MachineTypeId'] = 1;
			$arrData['OilTypeId'] = 1;
			$arrData['ServicesId'] = 1;
			$arrData['Name'] =  $_POST['Name'];
			$arrData['Capacity'] = 200;
			$arrData['Balance'] = 0;
			$arrData['Remaining'] = 0;
			
			//null 
			$arrData['CreateUserId'] = 1;
			$arrData['CustomerId'] = 1;
			$arrData['BoardNumber'] = $_POST['BoardNumber'];
			
			//InsertBoard
			$resUpdate = $this->mol_machine->InsertBoard($arrData);
		}
		
		redirect('station/createboard', '');
	}

	public function setboard()
	{
		$rowid = 0;
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'SETTING';
		$arrMenu['page'] = 'แสดงบอร์ด';
 
		if(isset($_GET['id'])&& intval($_GET['id']) > 0 ) {
			$arrData['id'] = $_GET['id'];
			$arrContent['data'] = $this->mol_machine->SelectBoardById($arrData);
			$rowid = $arrData['id'];
			//$arrContent['data']['costid'] =  $_GET['id'];	
		}else if(isset($_POST)){
			//var_dump($_POST);exit();
		}
		//var_dump($arrContent['data']);exit;
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'แสดงข้อมูลบอร์ด',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'station/setboard',
											'page'=>'ตั้งค่าข้อมูลบอร์ด'
										);
		$arrFooter['footer'] = $setTemplate['footer'];
		$arrContent['template']['row_id'] = $rowid;
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/setboard',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function changeboard()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
	
 		if(isset($_POST)){
			//var_dump($_POST['value']);exit();
			$arrData['Id'] = $_POST['row_id'];
			$arrData['UpdateDate'] = date("Y-m-d H:i:s");
			$arrData['BoardName'] = $_POST['value_Name'];
			$arrData['BoardId'] = $_POST['value_BoardId'];
			$arrData['BoardNumber'] = $_POST['value_BoardNumber'];

			$resUpdate = $this->mol_machine->EditBoardById($arrData);
		}
		redirect('station/showboard', '');
	}

	public function showListOrder()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'Station';
		$arrMenu['page'] = 'ยืนยันการสั่งน้ำมัน';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		//$arrData['or.StationId'] = 3;
		$arrContent['data'] = $this->mol_oilorder->showOilOrder($arrData);
		//var_dump($arrContent['data'] );exit();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ยืนยันการสั่งน้ำมัน',
											'parent'=>'ยืนยันการสั่งน้ำมัน',
											'link'=>'station/showListOrder',
											'page'=>'Order List'
										);
										
		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/showListOrder',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function ConfirmOrder()
	{ 
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
	
 		if(isset($_POST)){
			
			$arrData['Id'] = $_POST['row_id'];
			$arrData['ConfirmDate'] = date("Y-m-d H:i:s");   
			// $arrData['StatusId'] = 10014; 
			$resUpdate = $this->mol_oilorder->ConfirmOilOrder($arrData);
			// $res = $this->mol_oilorder->InsertOilOrder($arrData);
			//var_dump($resUpdate);exit();
		}
		redirect('station/showListOrder', '');
	}

	public function ListOrderHistory()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'Station';
		$arrMenu['page'] = 'ประวัติการสั่งน้ำมัน';
		//set variable DB
		// $arrData['duration'] = 20;
		// $arrData['offset'] = 0; 
		// $arrData['or.StatusId'] = 10013;
		$arrContent['data'] = $this->mol_oilorder->selectListOrderHistory();

		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ประวัติการสั่งน้ำมัน',
											'parent'=>'ประวัติการสั่งน้ำมัน',
											'link'=>'station/ListOrderHistory',
											'page'=>'History List'
										);
										
		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/datatable',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

}