<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeDashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        //load lib CI
        $this->load->helper('url');
        
        //load lib custom
        $arrBlank = array();
        $this->load->library('lib_session');
        $this->load->library('lib_template',$arrBlank);
        
        //check session
        $this->lib_session->chkSession(); 

        $this->load->model(array('mol_machine','mol_logdaily' ,'mol_setting' , 'mol_accountinvoice'));

    }   
 
    public function index()
    {
        $this->load->view('welcome_message');
    }

    public function dashboard()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'Home';
        $arrMenu['page'] = 'Real-time Dashboard';
        $arrData['L.DeleteFlag'] = 1;
        // Database
        $arrContent['countNumOilStation'] = $this->mol_machine->countNumOilStation();
        $arrContent['sumValueOneMonth'] = $this->mol_logdaily->sumValueOneMonth();
        $arrContent['sumCapacityOneMonth'] = $this->mol_logdaily->sumCapacityOneMonth();
        $arrContent['data'] = $this->mol_machine->showDashboardStation();
        $arrContent['countStationEnable'] = $this->mol_machine->countStationonline();
        $arrContent['countStationDisable'] = $this->mol_machine->countStationoffline();
        $arrContent['columnChart'] = $this->mol_accountinvoice->columnChart(); // กราฟแท่ง
        $arrContent['lineChart'] = $this->mol_logdaily->lineChart(); // กราฟเส้น
        $arrContent['nameMachineonline'] = $this->mol_machine->getNameStationonline();
        $arrContent['lineChartDay'] = $this->mol_logdaily-> lineChartDay(); // กราฟเส้น
       // var_dump($arrContent['lineChartDay'] ); exit();
        $arrMenu['customer'] = array('happyoil','wealthoil');
        $arrContent['template'] = array( 'header'=>'Real-time Dashboard',
                'parent'=>'หน้าหลัก',
                'link'=>'homeDashboard/dashboard',
                'page'=>'Real-time Dashboard'
                );
        $arrFooter['footer'] = $setTemplate['footer'];
        $this->load->view('adminlte/menu',$arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/home.php',$arrContent);
        $this->load->view('adminlte/footer',$arrFooter);
    }

    public function noticelist()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'หน้าหลัก';
		$arrMenu['page'] = 'ข้อมูลการแจ้งเตือน';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_setting->selectNotice();
		// $arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ข้อมูลการแจ้งเตือน',
											'parent'=>'หน้าหลัก ',
											'link'=>'homeDashboard/noticelist',
											'page'=>'ข้อมูลการแจ้งเตือน'
										);
		$arrFooter['footer'] = $setTemplate['footer'];

		// $arrContent['data']['linkadd'] = 'addroutegroup';
		// $arrContent['data']['linkedit'] = 'editRouteGroup';

		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/datatable',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

}