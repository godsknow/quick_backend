<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Price extends CI_Controller
{

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  *         http://example.com/index.php/welcome
  *    - or -
  *         http://example.com/index.php/welcome/index
  *    - or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/user_guide/general/urls.html
  */
 public function __construct()
 {
  parent::__construct();
  // Your own constructor code
  //load lib CI
  $this->load->helper('url');

  //load lib custom
  $arrBlank = array();
  $this->load->library('lib_session');
  $this->load->library('lib_template', $arrBlank);

  //Load model
  $this->load->model('mol_pricelist');
  $this->load->model('mol_oiltype');

  //check session
  $this->lib_session->chkSession();

  $arrContent = array();
 }

 public function index()
 {
  $this->load->view('welcome_message');
 }

 public function stationprice()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  $arrMenu['permissiom'] = 'PRICE';
  $arrMenu['page'] = 'แสดงราคาน้ำมัน';
  //set variable DB
  $arrData['duration'] = 20;
  $arrData['offset'] = 0;
  $arrContent = array();

  $arrContent['data'] = $this->mol_pricelist->selectDefaultPrice();

  $arrList = $this->mol_pricelist->selectStationPrice();
  $numRow = count($arrContent['data']['result']);
  //var_dump($arrList );
  foreach ($arrList['result'] as $key => $value) {
   $arrContent['data']['result'][$numRow] = $value;
   $numRow++;
  }
  // [pageinfo] => Array ( [allrecord] => 1 [pageall] => 1 )
  // echo'<br/>';print_r($arrContent['data']);exit();

  $arrMenu['customer'] = array('happyoil', 'wealthoil');
  $arrContent['template'] = array('header' => 'ราคาน้ำมัน',
   'parent' => 'ราคาน้ำมัน',
   'link' => 'price/stationprice',
   'page' => 'แสดงราคาน้ำมัน',
  );
  $arrFooter['footer'] = $setTemplate['footer'];

  $this->load->view('adminlte/menu', $arrMenu);
  $this->load->view('adminlte/navbar');
  $this->load->view('adminlte/price', $arrContent);
  $this->load->view('adminlte/footer', $arrFooter);
 }

 public function setprice()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  $arrMenu['permissiom'] = 'PRICE';
  $arrMenu['page'] = 'แสดงราคาน้ำมัน';
  $arrData['duration'] = 20;
  $arrData['offset'] = 0;

  if (isset($_GET['id']) && intval($_GET['id']) > 0) {
   $arrData['id'] = $_GET['id'];
   $arrData['Value'] = $this->input->post('Values');
   $arrContent['data'] = $this->mol_pricelist->selectPrice($arrData['id']);
   $arrContent['PriceId'] = $_GET['id'];
   //$arrContent['data'] = $this->mol_oiltype->selectSetOilType($arrData)->result();
   //var_dump($arrData['Value']);exit;
   //$arrContent['data'] = $this->mol_pricelist->UpdateOilvalue($arrData);
  }

  $arrMenu['customer'] = array('happyoil', 'wealthoil');
  $arrContent['template'] = array('header' => 'ตั้งค่าราคาน้ำมัน');
  $arrFooter['footer'] = $setTemplate['footer'];

  $this->load->view('adminlte/menu', $arrMenu);
  $this->load->view('adminlte/navbar');
  $this->load->view('adminlte/setprice', $arrContent);
  $this->load->view('adminlte/footer', $arrFooter);
 }

 public function UpdateOil()
 {
  $data['Id'] = $this->input->get();

 }
 public function changePrice()
 {
  if (isset($_POST)) {
   $arrData['Price'] = $_POST['Price'];
   $arrData['PriceId'] = $_POST['PriceId'];
   //var_dump($arrData['Price']);exit();
   $resUpdate = $this->mol_pricelist->EditpriceById($arrData);

  }
  redirect('price/stationprice', '');
 }

}