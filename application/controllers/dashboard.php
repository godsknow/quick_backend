<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
  //load lib CI
  $this->load->helper('url');
  
  //load lib custom
  $arrBlank = array();
  $this->load->library('lib_session');
  $this->load->library('lib_template',$arrBlank);
  
  //check session
  $this->lib_session->chkSession(); 

  $this->load->model(array('mol_machine','mol_logdaily'));

    }   
 
 public function index()
 {
  $this->load->view('welcome_message');
 }

 public function dashboardinvoice()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  $arrMenu['permissiom'] = 'Dashboard';
  $arrMenu['page'] = 'ข้อมูลผลประกอบการ';
  $arrData['duration'] = 20;
  $arrData['offset'] = 0;
  // Database
  $arrContent['countNumOilStation'] = $this->mol_machine->countNumOilStation();
  $arrContent['sumValueOneMonth'] = $this->mol_logdaily->sumValueOneMonth();
  $arrContent['sumCapacityOneMonth'] = $this->mol_logdaily->sumCapacityOneMonth();
  $arrContent['selectInvoice'] = $this->mol_logdaily->selectInVoice($arrData); 
  //var_dump($arrContent); exit();
  $arrMenu['customer'] = array('happyoil','wealthoil');
  $arrContent['template'] = array( 'header'=>'ข้อมูลผลประกอบการ',
           'parent'=>'กระดานแสดงข้อมูล',
           'link'=>'price/dashboardinvoice',
           'page'=>'ข้อมูลผลประกอบการ'
          );
  $arrFooter['footer'] = $setTemplate['footer'];

  $this->load->view('adminlte/menu',$arrMenu);
  $this->load->view('adminlte/navbar');
  $this->load->view('adminlte/dashboard/dashboardinvoice.php',$arrContent);
  $this->load->view('adminlte/footer',$arrFooter);
    }
    
    public function dashboardstation()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  $arrMenu['permissiom'] = 'Dashboard';
  $arrMenu['page'] = 'ข้อมูลตู้น้ำมัน';
  $arrData['L.DeleteFlag'] = 1;
  $arrContent['data'] = $this->mol_machine->showDashboardStation();
  $arrContent['countStationEnable'] = $this->mol_machine->countStationonline();
  $arrContent['countStationDisable'] = $this->mol_machine->countStationoffline();
  //var_dump($arrContent['countStation']); exit();
  
  $arrMenu['customer'] = array('happyoil','wealthoil');
  $arrContent['template'] = array( 'header'=>'ข้อมูลตู้น้ำมัน',
           'parent'=>'กระดานแสดงข้อมูล',
           'link'=>'price/dashboardinvoice',
           'page'=>'ข้อมูลตู้น้ำมัน'
          );
  $arrFooter['footer'] = $setTemplate['footer'];

  $this->load->view('adminlte/menu',$arrMenu);
  $this->load->view('adminlte/navbar');
  $this->load->view('adminlte/dashboard/dashboardstation.php',$arrContent);
  $this->load->view('adminlte/footer',$arrFooter);
 }

 public function dashboardprice()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  $arrMenu['permissiom'] = 'Dashboard';
  $arrMenu['page'] = 'ข้อมูลรายได้ต่อเดือน';
  $arrMenu['customer'] = array('happyoil','wealthoil');
  $arrContent['template'] = array( 'header'=>'ข้อมูลรายได้ต่อเดือน',
           'parent'=>'กระดานแสดงข้อมูล',
           'link'=>'price/dashboardinvoice',
           'page'=>'ข้อมูลรายได้ต่อเดือน'
          );
  $arrFooter['footer'] = $setTemplate['footer'];
  $this->load->view('adminlte/menu',$arrMenu);
  $this->load->view('adminlte/navbar');
  $this->load->view('adminlte/dashboard/dashboardprice.php',$arrContent);
  $this->load->view('adminlte/footer',$arrFooter);
 }
}