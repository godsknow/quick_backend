<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        //load lib CI
        $this->load->helper('url');

        //load lib custom
        $arrBlank = array();
        $this->load->library('lib_session');
        $this->load->library('lib_template', $arrBlank);

        //Load model
        $this->load->model('mol_report');

        //check session
        $this->lib_session->chkSession();
    }

    public function index()
    {
        $this->load->view('welcome_message');
    }

    public function reportdaily()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'REPORT';
		$arrMenu['page'] = 'รายงานประจำวัน';

        if (isset($_POST['searchdate']) && trim($_POST['searchdate']) != '') {
            $strSearchDate = $_POST['searchdate'];
            $arrPayment = explode("/",strval($_POST['searchdate'])) ;
            $arrData['DATE_FORMAT(`iv`.`PaymentedDate`,\'%Y-%m-%d\') '] = $arrPayment[2].'-'.$arrPayment[0].'-'.$arrPayment[1] ;
            // $arrData['PaymentedDate'] = $_POST['searchdate'];
        } else {
            // $arrData['PaymentedDate'] = date('Y/m/d');
            $arrData['DATE_FORMAT(`iv`.`PaymentedDate`,\'%Y-%m-%d\') '] = date('Y-m-d');
            $strSearchDate = date('m/d/Y');
        }
        $arrContent['data'] = $this->mol_report->selectAccountInvoice($arrData);

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'รายงานประจำวัน',
            'parent' => 'รายงาน',
            'link' => 'report/reportdaily',
            'page' => 'รายงานประจำวัน',
        );
        $arrContent['template']['searchDate'] = $strSearchDate;
        $arrFooter['footer'] = $setTemplate['footer'];
        
        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/report/reportsearchdate', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function reportMonth()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'REPORT';
		$arrMenu['page'] = 'รายงานประจำเดือน';
        // var_dump($_POST);
        if (isset($_POST['month']) && trim($_POST['month']) != '') {
            $arrData['FORMAT(iv.PaymentedDate, \'MM\') ='] = $_POST['month'];
            $arrData['FORMAT(iv.PaymentedDate, \'yyyy\') ='] = $_POST['year'];
            $m = $arrData['FORMAT(iv.PaymentedDate, \'MM\') ='] = $_POST['month'];
            $y = $arrData['FORMAT(iv.PaymentedDate, \'yyyy\') ='] = $_POST['year'];
        } else {
            $arrData['FORMAT(iv.PaymentedDate, \'MM\') = '] = date('m');
            $arrData['FORMAT(iv.PaymentedDate, \'yyyy\') ='] = date('Y');
            $m = $arrData['FORMAT(iv.PaymentedDate, \'MM\') ='] = date('m');
            $y = $arrData['FORMAT(iv.PaymentedDate, \'yyyy\') ='] = date('Y');

            // var_dump($_POST['FORMAT(iv.PaymentedDate, \'MM\') =']);
            //var_dump($_POST['FORMAT(iv.PaymentedDate, \'yyyy\') =']);
            // exit();
        }
        // var_dump($arrData);exit();
        /*  $arrContent['MonthArray'] = array(
        "01" => "มกราคม", "02" => "กุมภาพันธ์", "03" => "มีนาคม", "04" => "เมษายน",
        "05" => "พฤกษาคม", "06" => "มิถุนายน", "07" => "กรกฎาคม", "08" => "สิงหาคม",
        "09" => "กันยายน", "10" => "ตุลาคม", "11" => "พฤศจิกายน", "12" => "ธันวาคม",
        );*/

        // $montharray = array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤกษาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        // $monthname = $montharray[$m];

        $arrContent['data'] = $this->mol_report->selectAccountInvoiceMonth($arrData);
        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'รายงานประจำเดือน',
            'parent' => 'รายงาน',
            'link' => 'report/reportMonth',
            'page' => 'รายงานประจำเดือน',
        );
        $arrContent['template']['month'] = $m;
        $arrContent['template']['year'] = $y;

        $arrFooter['footer'] = $setTemplate['footer'];

        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/report/reportsearchmonth', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function reportCash()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'REPORT';
		$arrMenu['page'] = 'รายงานยอดขายเงินสด';

        if (isset($_POST['searchdate']) && trim($_POST['searchdate']) != '') {
            $strSearchDate = $_POST['searchdate'];
            $arrPayment = explode("/",strval($_POST['searchdate'])) ;
            $arrData['DATE_FORMAT(`iv`.`PaymentedDate`,\'%Y-%m-%d\') '] = $arrPayment[2].'-'.$arrPayment[0].'-'.$arrPayment[1] ;
            // $arrData['PaymentedDate'] = $_POST['searchdate'];
        } else {
            // $arrData['PaymentedDate'] = date('Y/m/d');
            $arrData['DATE_FORMAT(`iv`.`PaymentedDate`,\'%Y-%m-%d\') '] = date('Y-m-d');
            $strSearchDate = date('m/d/Y');
        }
        $arrContent['data'] = $this->mol_report->selectAccountInvoice($arrData);

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'รายงานประจำวัน',
            'parent' => 'รายงาน',
            'link' => 'report/reportCash',
            'page' => 'รายงานยอดขายเงินสด',
        );
        $arrContent['template']['searchDate'] = $strSearchDate;

        $arrFooter['footer'] = $setTemplate['footer'];
        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/report/reportsearchdate', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function reportQr()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'REPORT';
		$arrMenu['page'] = 'รายงานยอดขาย QR-code';

        if (isset($_POST['searchdate']) && trim($_POST['searchdate']) != '') {
            $strSearchDate = $_POST['searchdate'];
            $arrPayment = explode("/",strval($_POST['searchdate'])) ;
            $arrData['DATE_FORMAT(`iv`.`PaymentedDate`,\'%Y-%m-%d\') '] = $arrPayment[2].'-'.$arrPayment[0].'-'.$arrPayment[1] ;
            // $arrData['PaymentedDate'] = $_POST['searchdate'];
        } else {
            // $arrData['PaymentedDate'] = date('Y/m/d');
            $arrData['DATE_FORMAT(`iv`.`PaymentedDate`,\'%Y-%m-%d\') '] = date('Y-m-d');
            $strSearchDate = date('m/d/Y');
        }
        $arrContent['data'] = $this->mol_report->selectAccountInvoice($arrData);

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'รายงานประจำวัน',
            'parent' => 'รายงาน',
            'link' => 'report/reportQr',
            'page' => 'รายงานยอดขาย QR-code',
        );
       $arrContent['template']['searchDate'] = $strSearchDate;
        $arrFooter['footer'] = $setTemplate['footer'];
        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/report/reportsearchdate', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }    
}