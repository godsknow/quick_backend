<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
		//load lib CI
		$this->load->helper('url');

		//load lib custom
		$arrBlank = array();
		$this->load->library('lib_session');
		$this->load->library('lib_template',$arrBlank);

		//Load model 
		$this->load->model(array('mol_station','mol_oiltype','mol_cost'));
		

		
		//check session
		$this->lib_session->chkSession();	
    }	 	
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function stationcost()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'COST';
		$arrMenu['page'] = 'แสดงต้นทุนน้ำมัน';
		//set variable DB
		// $arrData['duration'] = 20;
		// $arrData['offset'] = 0;
		$arrData['c.DeleteFlag'] = 0;
		$arrContent['data'] = $this->mol_cost->selectCostAll($arrData);
		$arrContent['template'] = array(	'header'=>'ต้นทุนน้ำมัน',
											'parent'=>'ต้นทุนน้ำมัน',
											'link'=>'cost/stationcost',
											'page'=>'แสดงต้นทุนน้ำมัน'
										);
		$arrFooter['footer'] = $setTemplate['footer'];

		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/costlist',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function costhistory()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'COST';
		$arrMenu['page'] = 'ประวัติข้อมูลปริมาณน้ำมัน';
		//set variable DB
		// $arrData['duration'] = 20;
		// $arrData['offset'] = 0; 
		$arrData['c.DeleteFlag'] = 1;
		$arrContent['data'] = $this->mol_cost->selectCostHistory();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ประวัติข้อมูลปริมาณน้ำมัน',
											'parent'=>'ต้นทุนน้ำมัน',
											'link'=>'cost/stationcost',
											'page'=>'ประวัติข้อมูลปริมาณน้ำมัน'
										);
		$arrFooter['footer'] = $setTemplate['footer'];
		//var_dump($arrContent['data']); exit();
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/datatablenoaction',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}
	
	public function setcost()
	{
		$rowid = 0;
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'COST';
		$arrMenu['page'] = 'แสดงต้นทุนน้ำมัน';
 
		if(isset($_GET['id'])&& intval($_GET['id']) > 0 ) {
			$arrData['id'] = $_GET['id'];
			$arrContent['data'] = $this->mol_cost->SelectCostById($arrData);
			$rowid = $arrData['id'];
			//$arrContent['data']['costid'] =  $_GET['id'];	
		}else if(isset($_POST)){
			//var_dump($_POST);exit();
			
		}
		//var_dump($arrContent['data']);exit;

		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ตั้งค่าต้นทุนน้ำมัน',
											'parent'=>'ต้นทุนน้ำมัน',
											'link'=>'cost/stationcost',
											'page'=>'ตั้งค่าต้นทุนน้ำมัน'
										);
		$arrFooter['footer'] = $setTemplate['footer'];
		$arrContent['template']['row_id'] = $rowid;
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/setcost',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function changeCost(){
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
	
 		if(isset($_POST)){
			//var_dump($_POST['value']);exit();
			$arrData['Id'] = $_POST['row_id'];
			$arrData['UpdateDate'] = date("Y-m-d H:i:s");
			$arrData['Value'] = $_POST['value'];

			$resUpdate = $this->mol_cost->EditCostById($arrData);
			$resUpdate = $this->mol_cost->InsertCost($arrData);
		}
		redirect('cost/stationcost', '');
	}

    public function selectCostHistory( array $data = null )
 	{
        $result = null;
        $arrData['duration'] = 1;
        $arrData['offset'] = 0;

        $this->db->select( 'iv.id,iv.InvoiceNo,iv.Qty,iv.Amount,iv.PaymentedDate' );
		$this->db->from( 'AccountInvoice iv' );
		$this->db->where( $key, $value );
        $this->db->order_by( 'ID', 'ASC' );
        $sum_row = clone $this->db;
        $rowAll = $sum_row->count_all_results();

        // if ( $rowAll > 0 ) {
        //     $result['pageinfo']['allrecord'] = $rowAll;
        //     if ( $arrData['duration'] == 1 ) {
        //         $result['pageinfo']['pageall'] = $arrData['duration'];
        //     } else {
        //         $result['pageinfo']['pageall'] = ceil( $rowAll/$arrData['duration'] );
        //         $this->db->limit( $arrData['duration'], $arrData['offset'] );
        //     }

        //     $query = $this->db->get();
        //     $rows = $query->num_rows();

        //     if ( $rows > 0 ) {
        //         for ( $i = 0; $i < $rows; $i++ ) {
        //             $result['result'][$i] = $query->row_array( $i );
        //         }
        //     }
        // }
        $result['pageinfo']['allrecord'] = 4;
        $result['result'][0] = array( 'Date' => '12/07/2020', 'StationName' => 'Test-001', 'LocaltionName' => 'WVS Office', 'value' => '25' );
        $result['result'][1] = array( 'Date' => '12/07/2020', 'StationName' => 'Test-002', 'LocaltionName' => 'ลุมพินี พาร์ค รัตนาธิเบศร์ งามวงศ์วาน', 'value' => '23' );
        $result['result'][2] = array( 'Date' => '12/07/2020', 'StationName' => 'Test-003', 'LocaltionName' => 'ยูนิซิตี้ ศูนย์รัตนาธิเบศร์', 'value' => '22' );
        $result['result'][3] = array( 'Date' => '12/07/2020', 'StationName' => 'Test-004', 'LocaltionName' => 'LUMPINI Park', 'value' => '22' );
        $result['result'][4] = array( 'Date' => '08/07/2020', 'StationName' => 'Test-003', 'LocaltionName' => 'LUMPINI Park', 'value' => '25' );
        $result['result'][5] = array( 'Date' => '08/07/2020', 'StationName' => 'Test-001', 'LocaltionName' => 'WVS Office', 'value' => '24' );
        $result['result'][6] = array( 'Date' => '08/07/2020', 'StationName' => 'Test-001', 'LocaltionName' => 'ยูนิซิตี้ ศูนย์รัตนาธิเบศร์', 'value' => '26' );

        return $result;
    }
}