<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
		//load lib CI
		$this->load->helper('url');

		//load lib custom
		$arrBlank = array();
		$this->load->library('lib_session');
		$this->load->library('lib_template',$arrBlank);

		//Load model 
		$this->load->model('mol_station');

		//check session
		$this->lib_session->chkSession();		

    }	 	
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	
}