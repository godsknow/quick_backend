<?php
class pdf extends CI_Controller
{

 public function index($data)
 {
  $mpdf = new \Mpdf\Mpdf(['autoLangToFont' => true,
   'mode'                                   => 'UTF-8',
   'default_font_size'                      => 9,
   'default_font'                           => 'THSarabun']);
  $html = $data;
  $mpdf->SetDisplayMode('fullpage');
  $mpdf->autoLangToFont = true;
  $mpdf->WriteHTML($html);
  $mpdf->Output("test_pdf_00", "I"); // opens in browser
  //$mpdf->Output('invoice.pdf','D'); // it will work as normal download
 }

}