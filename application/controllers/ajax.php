<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
		//load lib CI
		$this->load->helper('url');

		//load lib custom
		$arrBlank = array();
		$this->load->library('lib_session');
		$this->load->library('lib_template',$arrBlank);
		$this->load->model(array('mol_machine','mol_logdaily'));
		//check session
		$this->lib_session->chkSession();	
    }	 	
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function ajexText()
	{
		$arrMenu['Test'] = '20';
    }

	public function sumValueOneMonth()
	{
        // Database
		$arrContent['sumValueOneMonth'] = $this->mol_logdaily->sumValueOneMonth();
        foreach ($arrContent['sumValueOneMonth']['result'][0] as $key => $value) {
            echo $value;
        }
	}
	public function sumCapacityOneMonth()
	{
        // Database
        $arrContent['sumCapacityOneMonth'] = $this->mol_logdaily->sumCapacityOneMonth();
        foreach ($arrContent['sumCapacityOneMonth']['result'][0] as $key => $value) {
			echo $value;
        }
	}
	public function countNumOilStation()
	{
        // Database
        $arrContent['countNumOilStation'] = $this->mol_machine->countNumOilStation();
        foreach ($arrContent['countNumOilStation']['result'][0] as $key => $value) {
			echo $value;
        }
    }

	public function ajexcountonline()
	{
		$arr = 20;
		$arrContent['countStationEnable'] = $this->mol_machine->countStationonline();
		 
		foreach ($arrContent['countStationEnable']['result'][0] as $key => $value) 
		echo $value; 
	}

	public function ajexcountoffline()
	{
		$arr = 20;
		$arrContent['countStationDisable'] = $this->mol_machine->countStationoffline();
		 
		foreach ($arrContent['countStationDisable']['result'][0] as $key => $value) 
		echo $value;
	}
	public function ajexgetdataLine()
	{
		//lineChart_Day
		$arr = 20;
		$arrContent['data'] = $this->mol_logdaily->lineChartDay();
		$dataDay;
		foreach($arrContent['data'] as $row){
				$dataDay = $row->value;
			}
		echo($dataDay);
		  
	}
        
        
}