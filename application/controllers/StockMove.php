<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StockMove extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
		//load lib CI
		$this->load->helper('url');

		//load lib custom
		$arrBlank = array();
		$this->load->library(array('lib_session','lib_trigger') );
		$this->load->library('lib_template',$arrBlank);

		//Load model 
		$this->load->model(array('mol_stockmove'));

		//check session
		$this->lib_session->chkSession();		

    }	 	
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

    public function GetStockMove()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'STOCK';
		$arrMenu['page'] = 'ทิศทางน้ำมัน';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 

		if (isset($_POST['searchdate']) && trim($_POST['searchdate']) != '') {
            $strSearchDate = $_POST['searchdate'];
            $arrPayment = explode("/",strval($_POST['searchdate'])) ;
            $arrData['DATE_FORMAT(`sm`.`CreateDate`,\'%Y-%m-%d\') '] = $arrPayment[2].'-'.$arrPayment[0].'-'.$arrPayment[1] ;
            
        } else {
            $arrData['DATE_FORMAT(`sm`.`CreateDate`,\'%Y-%m-%d\') '] = date('Y-m-d');
            $strSearchDate = date('m/d/Y');
		}
		
		$arrContent['data'] = $this->mol_stockmove->GetStockByDate($arrData);
		
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ทิศทางน้ำมัน',
											'parent'=>'การจัดการน้ำมัน',
											'link'=>'StockMove/GetStockMove',
											'page'=>'ทิศทางน้ำมัน'
										);
		
		$arrFooter['footer'] = $setTemplate['footer'];
		
		// $arrContent['data']['linkadd'] = 'addroutegroup';
		// $arrContent['data']['linkedit'] = 'editRouteGroup';
		
		$arrContent['template']['searchDate'] = $strSearchDate;
		$arrFooter['footer'] = $setTemplate['footer'];

		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/datatable',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}


}