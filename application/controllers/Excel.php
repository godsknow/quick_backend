<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Excel extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model( 'mol_oilorder' );
    }

    public function index() {
        $data['page'] = 'export-excel';
        $data['title'] = 'Export Excel data';

    }

    public function createExcel() {

        $data =  $this->mol_oilorder->selectListOrderHistoryExcel();

        $fileName = date( 'dmyhis' ).'.xlsx';

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $a = 65;
        $i = 1;

        $sheet->setCellValue( 'A1', 'ชื่อตู้' );
        $sheet->setCellValue( 'B1', 'น้ำมัน' );
        $sheet->setCellValue( 'C1', 'ปริมาณ' );
        $sheet->setCellValue( 'D1', 'ราคา' );
        $sheet->setCellValue( 'E1', 'วันที่รับทราบ' );
        $rows = 2;
        foreach ( $data as $val ) {
            // var_dump( $val );
            $sheet->setCellValue( 'A' . $rows, $val['ชื่อตู้'] );
            $sheet->setCellValue( 'B' . $rows, $val['น้ำมัน'] );
            $sheet->setCellValue( 'C' . $rows, $val['ปริมาณ'] );
            $sheet->setCellValue( 'D' . $rows, $val['ราคา'] );
            $sheet->setCellValue( 'E' . $rows, $val['วันที่รับทราบ'] );
            $rows++;

        }
        // exit;

        $writer = new Xlsx( $spreadsheet );
        $writer->save( 'uploads/'.$fileName );
        header( 'Content-Type: application/vnd.ms-excel' );
        redirect( base_url().'/uploads/'.$fileName );
    }

}
?>