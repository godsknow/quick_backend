<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Permission extends CI_Controller
{

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  *         http://example.com/index.php/welcome
  *    - or -
  *         http://example.com/index.php/welcome/index
  *    - or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/user_guide/general/urls.html
  */

public $arrtemplate = array('home' => array('name' => 'stationlist', 'url' => 'station/stationlist'));

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        //load lib CI
        $this->load->helper('url');

        //load lib custom
        $arrBlank = array();
        $this->load->library('lib_session');
        $this->load->library('lib_template', $arrBlank);

        // $this->load->library('lib_crypted');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation', 'lib_crypted'));
        // $this->load->database('happystation');

        //Load model
        $this->load->model(array('mol_user', 'mol_setting', 'mol_customer', 'mol_manage_user'));

    }

    public function index()
    {
        $this->load->view('welcome_message');
    }

    public function Home()
    {
        $this->load->view('adminlte/Home');
    }

    public function login()
    {
        if ($this->session->has_userdata('logged_in')) {
            $strHome = $this->lib_template->getPageHome();
            redirect($strHome, 'Home');
        }

        $arrContent['template'] = array('header' => 'StationLocation');
        $this->load->view('adminlte/login', $arrContent);
    }

 public function register()
 {
  $arrContent['template'] = array('header' => 'StationLocation');
  $arrContent['customer'] = $this->mol_customer->GetCustomer();

  $this->load->view('adminlte/register', $arrContent);
 }

 public function addUser()
 {
  $arrData['name']     = $this->input->post('Name');
  $arrData['email']    = $this->input->post('Email');
  $arrData['password'] = $this->input->post('Password');
  // $arrData['Repassword'] = $this->input->post('RePassword');
  $arrData['agree']   = $this->input->post('terms');
  $arrData['CusCode'] = $this->input->post('CusCode');

  $this->form_validation->set_rules('Name', 'name', 'trim|required');
  $this->form_validation->set_rules('Email', 'email', 'trim|required|valid_email');
  $this->form_validation->set_rules('Password', 'password', 'trim|required', array('required' => 'You must insert password!.'));
  // $this->form_validation->set_rules('RePassword','Password Confirmation','trim|required|matches[password]');

  $keyUser = $this->mol_setting->GetKey('UserKey', $arrData['CusCode']);

  if ($this->form_validation->run() == false) {
   $this->register();
   // redirect(base_url().'permission/login');
  } else {
   foreach ($keyUser as $row) {
    $enPass = $this->lib_crypted->Encrypt_password($arrData['password'], $row->Value);
    $data   = array(
     'DeleteFlag'    => '0',
     'CreateUserId'  => '1',
     'CreateDate'    => date('Y-m-d H:i:s'),
     'Ordering'      => '0',
     'CustomerId'    => '1',
     'UserName'      => $arrData['name'],
     'Password'      => $enPass,
     'UserStatus'    => '0',
     'UserType'      => '0',
     'OTPCreateDate' => date('Y-m-d H:i:s'),
    );
   }
   $flagInsert = $this->mol_user->InsertUser($data);

   if ($flagInsert != false) {
    echo '<script>alert("You Have Successfully updated this Record!");</script>';
    redirect('permission/login', 'refresh');
   } else {
    $this->session->set_flashdata("message", "Record Not Updated!");
    $this->load->view('adminlte/register');
   }
  }
 }

    public function checkPermission()
    {
        $arrData['username'] = strtoupper($this->input->post('username'));
        $arrData['password'] = $this->input->post('password');

        $this->form_validation->set_rules('username', 'UserName', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required',
        array('required' => 'You must insert password!.')
        );
        //get page home
        $strHome = $this->lib_template->getPageHome();

        if ($this->form_validation->run() == false) {
            $this->load->view('adminlte/login');
        } else {
            if ($arrData['username'] == 'ADMIN' && $arrData['password'] == 'admin1234') {
                $resSession = $this->lib_session->fakeSession();
              
                redirect($strHome, 'Home');
            } else {
                 $arrResult = $this->mol_user->GetLogin($arrData);

                foreach ($arrResult as $item) {
                    $key = $this->mol_setting->GetKey('UserKey', $item->customerid);

                    $arrPermission = $this->mol_manage_user->GetRolePermissionbyId($item->Id);
                    // echo '<br/>arrPermission=';
                    // var_dump($arrPermission);
                    // exit();

                    foreach ($key as $row) {
                    $password = $this->lib_crypted->Decrypt_password($item->password, $row->Value);

                    $numResult = count($arrResult);

                    if (count($arrResult) > 0 && $arrData['password'] == $password) {
                        $newSession = [
                        'username'  => $arrData['username'],
                        'email'     => 'rapee.wvs@hmail.com',
                        'key'       => $arrPermission,
                        'logged_in' => true,
                        ];
                        $resSession = $this->lib_session->setSession($newSession);

                        redirect($strHome, 'Home');
                        } else {
                            $this->session->set_flashdata(array('message' => '<p class="login-box-msg" style="color:red;">ชื่อผู้ใช้หรือรหัสผ่านผิดพลาด!</p>'));
                            redirect('permission/login', 'refresh');
                        }
                    }
                }
            }
        }
    }

 public function testEncode()
 {
  $plaintext      = "message to be encrypted";
  $cipher         = "AES-128-CBC";
  $key            = "wvs2020";
  $options        = OPENSSL_RAW_DATA;
  $ivlen          = openssl_cipher_iv_length($cipher);
  $iv             = openssl_random_pseudo_bytes($ivlen);
  $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options, $iv);
  $hmac           = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
  $ciphertext     = base64_encode($iv . $hmac . $ciphertext_raw);

  echo '<br/>ciphertext=' . $ciphertext;

  $c                  = base64_decode($ciphertext);
  $ivlen              = openssl_cipher_iv_length($cipher);
  $iv                 = substr($c, 0, $ivlen);
  $hmac               = substr($c, $ivlen, $sha2len = 32);
  $ciphertext_raw     = substr($c, $ivlen + $sha2len);
  $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options, $iv);
  $calcmac            = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
  if (hash_equals($hmac, $calcmac)) //PHP 5.6+ timing attack safe comparison
  {
   echo '<br/>original_plaintext=' . $original_plaintext . "\n";
  }
 }

 public function logout()
 {
  $resSession = $this->lib_session->delSession();
  // if($this->session->userdata('logged_in'))
  // {
  //     $this->session->sess_destroy();
  // }

  // $arrContent['template'] = array('header'=>'StationLocation');

  redirect('permission/login');
 }

}