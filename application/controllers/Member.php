<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
		//load lib CI
		$this->load->helper('url');
		$this->load->helper('string');
		//load lib custom
		$arrBlank = array();
		$this->load->library(array('lib_session','lib_trigger','lib_sms','lib_crypted') );
		$this->load->library('lib_template',$arrBlank);

		//Load model 
		$this->load->model(array('mol_member','mol_walletuser','mol_sms','mol_setting'));

		//check session
		$this->lib_session->chkSession();		

    }	 	
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

    public function memberlist()
	{
		$setTemplate = $this->lib_template->getTemplateVar();
		$arrMenu['menu'] = $this->lib_template->getTemplateMenu();
		$arrMenu['permissiom'] = 'MEMBER';
		$arrMenu['page'] = 'ข้อมูลสมาชิก';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_member->selectMemberAll();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ข้อมูลสมาชิก',
											'parent'=>'สมาชิก',
											'link'=>'member/memberlist',
											'page'=>'ข้อมูลสมาชิก'
										);
		$arrFooter['footer'] = $setTemplate['footer'];

		// $arrContent['data']['linkadd'] = 'addroutegroup';
		 $arrContent['data']['linkedit'] = 'returnpassword';

		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/datatable',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

	public function returnpassword(){
		$UserName = $this->input->get('username');
		$unique = random_string('alnum',6);
		$data['msg'] = 'รหัสผ่านชั่วคราวของคุณคือ';
		$data['msg'] .= $unique;
        $data['phone'] = $UserName;
		$data['type'] = 'SMS';
		
		$userid = $this->mol_walletuser->GetWalletUserByUserName($UserName);
		if($userid != null){
			$data['Id'] = $userid;
			$data['UserId'] = $userid[0]->Id;
        }
        $result = $this->lib_sms->SMS( $data );
        $data['result'] = $result;
		$key = $this->mol_setting->GetKey('UserKey');
		$data['enpass'] = $this->lib_crypted->Encrypt_password($unique,$key[0]->Value);
		$this->mol_walletuser->UpdatePassword($data);
		$this->mol_sms->addLogsms($data);
		
		redirect('member/memberlist');
	}	


}