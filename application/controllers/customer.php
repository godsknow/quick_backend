<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Customer extends CI_Controller {
    public function __construct()
 {
        parent::__construct();
        // Your own constructor code
        //load lib CI
        $this->load->helper( 'url' );

        //load lib custom
        $arrBlank = array();
        $this->load->library( 'lib_session' );
		$this->load->library( 'lib_template', $arrBlank );
		$this->load->library('lib_crypted');

        //Load model
        $this->load->model( array( 'mol_station', 'mol_oiltype', 'mol_cost','mol_customer','mol_setting','mol_machine' ) );

        //check session
        $this->lib_session->chkSession();

    }

    public function index()
 {
        $this->load->view( 'welcome_message' );
    }

    public function ShowCustomer( Type $var = null )
 {
    				
		$setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'SETTING';
		$arrMenu['page'] = 'ข้อมูลลูกค้า';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
        $arrContent['data'] = $this->mol_customer->ShowCustomer();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'ลูกค้า',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'station/StationData',
											'page'=>'แสดงข้อมูลลูกค้า'
										);
		$arrContent['data']['linkedit'] = 'EditCustomer';
		$arrContent['data']['linkAdd'] = 'AddCustomer';

		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
    	$this->load->view('adminlte/customer',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);

    }
    public function showsettingdata()
	{
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'Development';
		$arrMenu['page'] = 'แสดงการตั้งค่าข้อมูล';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_setting->selectSettingAll();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'แสดงการตั้งค่าข้อมูล',
											'parent'=>'--------',
											'link'=>'customer/settingdata',
											'page'=>'แสดงการตั้งค่าข้อมูล'
										);

		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/setSetting',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}

    public function showSettingDefault()
	{
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'SETTING';
		$arrMenu['page'] = 'ข้อมูลพื้นฐานตู้';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		$arrContent['data'] = $this->mol_setting->selectSettingDefault();
		$arrContent['data']['result'][0]['หน่วย'] = 'บาท';
		$arrContent['data']['result'][1]['หน่วย'] = 'ลิตร';
		$arrContent['data']['result'][2]['หน่วย'] = 'บาท';
		// echo'<br/>';var_dump($arrContent['data']);
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'แสดงการตั้งค่าข้อมูล',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'customer/settingdata',
											'page'=>'แสดงการตั้งค่า'
										);
		$arrContent['data']['linkedit'] = 'editSettingDefault';

		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/datatable',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}	
	
	// public function showboard()
	// {
	// 	$setTemplate = $this->lib_template->getTemplateVar();
    //     $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
    //     $arrMenu['permissiom'] = 'SETTINGMACHINE';
	// 	$arrMenu['page'] = 'แสดงบอร์ด';
	// 	//set variable DB
	// 	$arrData['duration'] = 20;
	// 	$arrData['offset'] = 0; 
	// 	$arrContent['data'] = $this->mol_machine->showMachineAll();
	// 	$arrMenu['customer'] = array('happyoil','wealthoil');
		
	// 	$arrContent['template'] = array(	'header'=>'แสดงข้อมูลบอร์ด',
	// 										'parent'=>'ตั้งค่าตู้น้ำมัน',
	// 										'link'=>'customer/showboard',
	// 										'page'=>'แสดงข้อมูลบอร์ด'
	// 									);

	// 	$arrFooter['footer'] = $setTemplate['footer'];
	// 	$this->load->view('adminlte/menu',$arrMenu);
	// 	$this->load->view('adminlte/navbar');
	// 	$this->load->view('adminlte/datatablenoaction',$arrContent);
	// 	$this->load->view('adminlte/footer',$arrFooter);
	// }
	public function editSettingDefault(){
		$setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'SETTING';
		$arrMenu['page'] = 'แสดงการตั้งค่าข้อมูล';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		if(isset($_GET['id'])&& intval($_GET['id']) > 0 ) {
			$arrData['Id'] = $_GET['id'];
		$arrContent['data'] = $this->mol_setting->getSettingDefaultById($arrData);
			//var_dump($arrContent['data']);exit();
		}
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'แสดงการตั้งค่าข้อมูล',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'customer/settingdata',
											'page'=>'แสดงการตั้งค่า'
										);
		// $arrContent['data']['linkadd'] = 'addSettingDefault';
		$arrContent['data']['linkedit'] = 'editSettingDefault';

		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/editSettingDefault',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}
	public function UpdateSetting()
    {
        if(isset($_POST)){
			$arrData['Id'] = $_POST['Id'];
			$arrData['UpdateDate'] = date("Y-m-d H:i:s");   
			$arrData['Value'] =  $_POST['Value']; 
			$res = $this->mol_setting->UpdateSetting($arrData);
			//var_dump($arrData['UpdateDate']);exit();
		}
        redirect('customer/showSettingDefault', '');
	}
	public function AddCustomer(){
		$setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'SETTING';
		$arrMenu['page'] = 'เพิ่มข้อมูลลูกค้า';
		//set variable DB
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		// $arrContent['data'] = $this->mol_setting->selectSettingDefault();
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'เพิ่มข้อมูลลูกค้า',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'customer/AddCustomer',
											'page'=>'แสดงการตั้งค่า'
										);
		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/AddCustomer',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}
	public function InsertCustomer(){
		if(isset($_POST)){
			$arrData['CreateDate'] = date("Y-m-d H:i:s");   
			$arrData['CompanyName'] =  $_POST['CompanyName']; 
			$arrData['FirstName'] =  $_POST['FirstName']; 
			$arrData['LastName'] =  $_POST['LastName']; 
			$arrData['Address1'] =  $_POST['Address1']; 
			$arrData['Email'] =  $_POST['Email']; 
			$arrData['Tel'] =  $_POST['Tel']; 
			$arrData['CusCode'] = $_POST['CusCode']; 
			$arrData['CountryId'] = '1';
			$arrData['TaxNo'] = $_POST['TaxNo'];
			$UserName = $_POST['UserName']; 
			$Password = $_POST['Password']; 
			$createdate = date("Y-m-d H:i:s");
			//var_dump($arrData);exit();
			$keyUser = $this->mol_setting->GetKey('UserKey', '1');
			foreach ($keyUser as $item) {
				$enPass = $this->lib_crypted->Encrypt_password($Password,$item->Value);
			}
			$res = $this->mol_customer->InsertCustomer($arrData);
			$arrData['Id'] = $res;   
			if ($enPass != null) {
				$data = array(
					'DeleteFlag' => 0,
					'CreateDate' => $createdate ,
					'Ordering' => 0,
					'UserName' => $UserName,
					'Password' => $enPass,
					'FirstName' => $arrData['FirstName'],
					'LastName' => $arrData['LastName'],
					'UserStatus' => 0,
					'UserType' => 0,
					'OTPCreateDate' => $createdate,
					'CustomerId' => $res,
				);
				$this->db->insert('Users', $data);
			}
			$this->load->model('mol_wallet');
			$t = $this->db->insert('Customers',$arrData);
			redirect('customer/ShowCustomer', '');
		}
	}
	public function EditCustomer(){
		$setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'SETTING';
		$arrMenu['page'] = 'แสดงการตั้งค่าข้อมูล';
		$arrData['duration'] = 20;
		$arrData['offset'] = 0; 
		if(isset($_GET['id'])&& intval($_GET['id']) > 0 ) {
		$arrData['Id'] = $_GET['id'];
		$arrContent['data'] = $this->mol_customer->getCustomerById($arrData);
		//var_dump($arrContent['data']);exit();
		}
		$arrMenu['customer'] = array('happyoil','wealthoil');
		$arrContent['template'] = array(	'header'=>'แสดงการแก้ไข',
											'parent'=>'ตั้งค่าข้อมูล',
											'link'=>'customer/EditCustomer',
											'page'=>'แสดงการตั้งค่า'
										);
		// $arrContent['data']['linkadd'] = 'addSettingDefault';

		$arrFooter['footer'] = $setTemplate['footer'];
		$this->load->view('adminlte/menu',$arrMenu);
		$this->load->view('adminlte/navbar');
		$this->load->view('adminlte/editCustomer',$arrContent);
		$this->load->view('adminlte/footer',$arrFooter);
	}
	public function UpdateCustomer(){
		if(isset($_POST)){
			$arrData['UpdateDate'] = date("Y-m-d H:i:s");   
			$arrData['CompanyName'] =  $_POST['CompanyName']; 
			$arrData['FirstName'] =  $_POST['FirstName']; 
			$arrData['LastName'] =  $_POST['LastName']; 
			$arrData['Address1'] =  $_POST['Address1']; 
			$arrData['Email'] =  $_POST['Email']; 
			$arrData['Tel'] =  $_POST['Tel']; 
			$arrData['CusCode'] = $_POST['CusCode']; 
			$arrData['Id'] = $_POST['Id'];
			// var_dump($arrData);exit();
			$res = $this->mol_customer->UpdateCustomer($arrData);
			redirect('customer/ShowCustomer', '');
		}
	}


}