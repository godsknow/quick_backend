<?php

require APPPATH . 'libraries\REST_Controller.php';

class WalletUser extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct() {
        parent::__construct();

        $params = array( 'type' => 'large', 'color' => 'red' );
        $this->load->model( array( 'mol_walletuser', 'mol_setting', 'mol_customer' ) );
        $this->load->helper( 'string' );
        $this->load->library( 'lib_crypted' );

    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */
  /**
   * api เรียกข้อมูลของลูกค้าอย่างเดียวไม่รวมเงินคงเหลือ
   */
    public function GetWalletUser_get() {
        $data = $this->mol_walletuser->GetWalletUser();
        return $this->response( $data, REST_Controller::HTTP_OK );
    }
/**
 * api เพิ่มข้อมูลของลูกค้า
 */
    public function AddWalletUser_post() {
        $data['UserName'] = $this->input->post( 'UserName' );
        $dt['Password'] = $this->input->post( 'Password' );
        $data['Phone'] = $this->input->post( 'PhoneNumber' );
        $data['Email'] = $this->input->post( 'Email' );
        $dt['Customer'] = $this->input->post( 'CusCode' );
        $data['value'] = $this->input->post( 'Value' );

        $cus = $this->mol_customer->GetCustomerByCusCode( $dt['Customer'] );

        $key = $this->mol_setting->GetKey( 'UserKey', $cus[0]->Id );

        $data['EnPassword'] = $this->lib_crypted->Encrypt_password( $dt['Password'], $key[0]->Value );

        if ( $this->mol_walletuser->InsertWalletUser( $data ) ) {
            $data['WallUserId'] = $this->mol_walletuser->GetWalletUserByUserName( $data['UserName'] );
            $data['CardNumber'] = $data['Phone'];
            if ( $this->mol_wallet->InsertWallet( $data ) ) {
                return $this->response( array( 'StatusCode' => 'Success' ), REST_Controller::HTTP_OK );
            }
        } else return $this->response( array( 'StatusCode' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

    }
/**
 * api แก้ไขข้อมูลของลูกค้า
 */
    public function EditWalletUser_post() {
        $data['Id'] = $this->input->post( 'WalletUserId' );
        $data['Phone'] = $this->input->post( 'Phone' );
        $data['Email'] = $this->input->post( 'Email' );

        if ( $this->mol_walletuser->UpdateWalletUser( $data ) ) {
            return $this->response( array( 'StatusCode' => 'Success' ), REST_Controller::HTTP_OK );
        } else return $this->response( array( 'StatusCode' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
    }

    public function DeleteWalletUser_get() {
        $data['Id'] = $this->input->get( 'WalletUserId' );

        if ( $this->mol_walletuser->DeleteWalletUser( $data['Id'] ) ) {
            return $this->response( array( 'StatusCode' => 'Success' ), REST_Controller::HTTP_OK );
        } else return $this->response( array( 'StatusCode' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
    }
}