<?php

require APPPATH . 'libraries\REST_Controller.php';

class Logdaily extends REST_Controller
{

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        //    $this->load->database();
        $this->load->model('mol_logdaily');
    }

    /**
     * api ส่วนที่ใช้ทั้ง mobile และ ตู้น้ำมัน
     *
     * @return Response
     */

     /**
      * api เรียกข้อมูลของ logdaily เอาไปใช้ใน Mobile
      */
    public function index_get()
    {
        $data = $this->mol_logdaily->Logdaily();

        $this->response($data, REST_Controller::HTTP_OK);
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    /**
      * api เรียกข้อมูลของ logdaily ด้วย ชิ่อเครื่อง เอาไปใช้ใน Mobile
      */
    public function DailyMachine_post()
    {
        $data['date']        = $this->input->post('date');
        $data['MachineName'] = $this->input->post('MachineName');

        $daily = $this->mol_logdaily->DailyMachine($data);

        $this->response($daily, 'Item created successfully.', REST_Controller::HTTP_OK);
    }


}