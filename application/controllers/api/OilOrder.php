<?php

require APPPATH . 'libraries\REST_Controller.php';

class OilOrder extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();

        $params = array( 'type' => 'large', 'color' => 'red' );
        $this->load->model( array( 'mol_oilorder', 'mol_status', 'mol_station', 'mol_oiltype', 'mol_routeman' ) );
    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */
    /**
     * api ขออนุมัติการเติมน้ำมันของ Service Center
     */
    public function Approve_post() {
        $data = $this->input->post( 'PurchaseNo' );
        $this->mol_oilorder->ApproveUpdate( $data );
        return $this->response( $data, REST_Controller::HTTP_OK );
    }
    /**
     * api อนุมัติการเติมน้ำมันของ เจ้าของตู้
     */
    public function Confirm_post() {
        $data = $this->input->post( 'PurchaseNo' );
        $this->mol_oilorder->ConfirmUpdate( $data );
        return $this->response( $data, REST_Controller::HTTP_OK );
    }
    /**
     * api การเติมน้ำมันเสร็จแล้วของ RouteMan
     */
    public function Refill_post() {
        $data['po'] = $this->input->post( 'PurchaseNo' );
        $data['rmid'] = $this->input->post( 'RoutemanId' );
        $this->mol_oilorder->RefillUpdate( $data );
        return $this->response( $data, REST_Controller::HTTP_OK );
    }

    /**
     * api คำสั่งเติมน้ำมันของเจ้าของตู้
     */
    public function InsertOilOrder_post() {
        $data['Machine'] = $this->input->post( 'MachineId' );
        $data['oiltype'] = $this->input->post( 'oiltypeId' );
        $data['qty'] = $this->input->post( 'qty' );
        $data['cost'] = $this->input->post( 'cost' );

        $basedata['Status'] = $this->mol_status->GetStatus( 'Enable' );

        foreach ( $basedata['Status'] as $key ) {
            $data['statusId'] = $key->Id;
        }

        if ( $this->mol_oilorder->InsertOilOrder( $data ) ) {
            return $this->response( 'Success', REST_Controller::HTTP_OK );
        } else  return $this->response( 'Unsuccess', REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
    }

    public function test_get() {
        $data = $this->mol_routeman->selectRouteMan();
        return $this->response( $data, REST_Controller::HTTP_OK );
    }

}