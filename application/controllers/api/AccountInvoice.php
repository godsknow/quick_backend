    <?php

require APPPATH . 'libraries/REST_Controller.php';

class AccountInvoice extends REST_Controller
{

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        //    $this->load->database();
        $this->load->model(array('mol_station', 'mol_machineError', 'mol_accountinvoice', 'mol_machine', 'mol_logdaily', 'mol_stockmove'));
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function SumAmountNet_get()
    {
        $data = $this->mol_accountinvoice->selectAccountInvoice();

        $this->response($data, REST_Controller::HTTP_OK);

    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */

    /** 
     * api รับข้อมูลจากตู้น้ำมัน 
     * 
    */
    public function index_post()
    {
       
        $post = $this->input->post();
        $input_data = json_decode(trim(file_get_contents('php://input')), true);

        // $data['BoardNumber']    = $this->input->post('BoardNumber');
        $data['SumCoin'] = $input_data['Data']['DATA'][0];
        $data['SumBankNote'] = $input_data['Data']['DATA'][1];
        $data['Oil_Remaining'] = $input_data['Data']['DATA'][2];
        $data['CostLitre'] = $input_data['Data']['DATA'][3];
        $data['Sum'] = $input_data['Data']['DATA'][4];
        $data['BeforeSum'] = $input_data['Data']['DATA'][5];
        $data['Error'] = $input_data['Data']['DATA'][6];
        $data['Coin'] = $input_data['Data']['DATA'][7];
        $data['BankNote'] = $input_data['Data']['DATA'][8];
        $data['LitreSold'] = $input_data['Data']['DATA'][9];
        $data['Fuel'] = $input_data['Data']['DATA'][10];
        $data['Direction'] = $input_data['Data']['DATA'][11];
        $data['Paid'] = $input_data['Data']['DATA'][12];
        $data['StatusId'] = 1;
        $data['Deleteflag'] = 0;
        $data['CreateDate'] = date('Y-m-d H:i:s');
        $data['Ordering'] = 0;
       
        $AI['WalletUser'] = ($input_data['TelNumber'] == NULL)? NULL:$input_data['TelNumber'];
        // var_dump;exit;
        $AI['SumCost'] = $data['Coin'] + $data['BankNote'];
        $AI['RefuelNo'] = $input_data['RefuelNo'];
        $AI['BoardNumber'] = $input_data['BoardNumber'];
        $AI['StationCode'] = $input_data['StationCode'];

        $insert = $this->chkInvoice($data, $AI);
        if ($insert != true) {
            $this->response(array('StatusCode' => 'Add Unsuccessful!'), REST_Controller::HTTP_BAD_REQUEST);

        } else {
            $this->response(array('StatusCode' => 'Add Successful!'), REST_Controller::HTTP_OK);

        }
    }

    /**
     * api insertข้อมูลน้ำมันขาเข้าลง stock 
     */
    private function stockin($arrData = null, $rowData = null)
    {
        $mc = $this->mol_machine->selectMachineOne($rowData['BoardNumber']);
        $st = $this->mol_station->SelectStationStockMove($rowData['StationCode'])->result();
        $chkfuel = $this->mol_machineError->selectMachineErrorByFuel();
        
                foreach ($mc as $value) {
                    foreach ($st as $value1) {
                        foreach ($chkfuel as $value3) {
                            if  ($arrData['Fuel'] != $value3->Fuel){
                            $Stock = array(
                                'DeleteFlag' => 0,
                                'CreateUserId' => 1,
                                'CreateDate' => date('Y-m-d H:i:s'),
                                'UpdateUserId' => 1,
                                'UpdateDate' => date('Y-m-d H:i:s'),
                                'Ordering' => 0,
                                'StatusId' => 1,
                                'StationId' => $value1->Id,
                                'Direction' => 'IN',
                                'Capacity' => $arrData['Fuel'],
                                'Description' => $value->Name,
                                'OilTypeId' => $value->OilTypeId,
                                'MachineId' => $value->Id,
                        );
                        if($this->mol_stockmove->InsertStockMove($Stock)){
                            $res = true;
                        }
                        else 
                        {
                            $res = false;
                        }
                    }
                    else {
                        $res = false;
                    }
                }
            }
        }
        return $res;
    }
    /**
     * api บันทึกข้อมูล AccountInvoice และลง stock ทั้งหมดที่เป็นขาออก
     */
    private function chkInvoice($arrData = null, $rowData = null)
    {

        $date = date('Y-m-d H:i:s');
       
        if(!$this->stockin($arrData,$rowData)){
            $mce = $this->mol_machineError->selectMachineErrorByRemain($arrData['Oil_Remaining']);
           
            if ($mce != null) {
               
                if (!$this->db->insert('MachineError', $arrData)) {
                    $res = false;

                } else {
                  
                    //insert accountinvoice
                    if ($arrData['Error'] == "0") {
                        
                        $data['RefuelNo'] = $rowData['RefuelNo'];
                        $data['QtyNet'] = $arrData['LitreSold'];
                        $data['Qty'] = $arrData['LitreSold'];
                        $data['Amount'] = $rowData['SumCost'];
                        $data['AmountNet'] = $rowData['SumCost'];
                        $data['BoardNumber'] = $rowData['BoardNumber'];
                        $data['StationCode'] = $rowData['StationCode'];
                        // var_dump( $rowData['WalletUser']);exit;
                        $mc = $this->mol_machine->selectMachineOne($rowData['BoardNumber']);

                        $st = $this->mol_station->SelectStationStockMove($rowData['StationCode'])->result();

                        foreach ($mc as $value) {
                            foreach ($st as $value1) {
                                $acc = array(
                                    'InvoiceNo' => $rowData['RefuelNo'],
                                    'StationId' => $value1->Id,
                                    'MachineId' => $value->Id,
                                    'StatusId' => $arrData['StatusId'],
                                    'QtyNet' => $arrData['LitreSold'],
                                    'Qty' => $arrData['LitreSold'],
                                    'Amount' => $rowData['SumCost'],
                                    'AmountNet' => $rowData['SumCost'],
                                    'Vat' => 0,
                                    'VatPercent' => 0,
                                    'Discount' => 0,
                                    'DiscountPercent' => 0,
                                    'Total' => 0,
                                    'Cost' => 0,
                                    'Margin' => 0,
                                    'ServiceId' => 1,
                                    'WalletNumber' => $rowData['WalletUser'],
                                    'PaymentedDate' => $date,
                                    'FinishTimeRefuel' => $date,
                                    'StatusId' => 1,
                                    'Deleteflag' => 0,
                                    'CreateDate' => date('Y-m-d H:i:s'),
                                    'Ordering' => 0,
                                );
                                
                                if ($this->db->insert('AccountInvoice', $acc)) {

                                    $datalog = $this->mol_logdaily->ChkLogdialy();
                                    if ($datalog == null) {
                                        $accus = array(
                                            'DeleteFlag' => 0,
                                            'CreateUserId' => 1,
                                            'CreateDate' => date('Y-m-d H:i:s'),
                                            'UpdateUserId' => 1,
                                            'UpdateDate' => date('Y-m-d H:i:s'),
                                            'Ordering' => 0,
                                            'StatusId' => 1,
                                            'Remark' => 0,
                                            'StationId' => $value1->Id,
                                            'MachineId' => $value->Id,
                                            'ServiceId' => 1,
                                            'Value' => $rowData['SumCost'],
                                            'Capacity' => $arrData['LitreSold'],
                                            'Margin' => 0,
                                            'Cost' => 0,
                                        );
                                        $this->db->insert('LogDaily', $accus);
                                        //var_dump($this->db->insert('Logdaily', $accus));exit;
                                    } else {
                                        foreach ($datalog as $update) {
                                            $accu = array(
                                                'UpdateDate' => date('Y-m-d H:i:s'),
                                                'Value' => $update->Value + $data['AmountNet'],
                                                'Capacity' => $update->Capacity + $arrData['LitreSold'],
                                            );
                                           
                                            $this->db->update('LogDaily', $accu, array('Id' => $update->Id));
                                        }   
                                    }

                                    $Stock = array(
                                        'DeleteFlag' => 0,
                                        'CreateUserId' => 1,
                                        'CreateDate' => date('Y-m-d H:i:s'),
                                        'UpdateUserId' => 1,
                                        'UpdateDate' => date('Y-m-d H:i:s'),
                                        'Ordering' => 0,
                                        'StatusId' => 1,
                                        'StationId' => $value1->Id,
                                        'Direction' => 'OUT',
                                        'Capacity' => $arrData['LitreSold'],
                                        'Description' => $value->Name,
                                        'OilTypeId' => $value->OilTypeId,
                                        'MachineId' => $value->Id,
                                    );

                                    if (!$this->mol_stockmove->InsertStockMove($Stock)) {
                                        $res = false;
                                    }
                                    $res = true;
                                } else {
                                    $res = false;

                                }
                            }
                        }
                    } else {
                        echo $arrData['Error'];
                        $res = false;
                    }
                }
            } else {
                $res = false;
            }
        } else{
            $res = true;
        }

        return $res;
    }

}