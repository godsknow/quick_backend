<?php
require APPPATH . 'libraries\REST_Controller.php';

class Login extends REST_Controller
{

 /**
  * Get All Data from this method.
  *
  * @return Response
  */

 public function __construct()
 {
  parent::__construct();

  $params = array('type' => 'large', 'color' => 'red');
  $this->load->model(array('mol_walletuser', 'mol_setting', 'mol_customer','mol_sms'));
  $this->load->library(array('lib_crypted','lib_sms'));
}

 /**
  * Get All Data from this method.
  *
  * @return Response
  */
  
/**
 * api login ของตู้น้ำมัน
 */
 public function Login_post()
 {
  // Get the post data

  $user = $this->input->post('Tel');
  $password = $this->input->post('password');

  if (!empty($user) && !empty($password)) {

   $data = $this->mol_walletuser->GetWalletUserByUserName($user);
   $data['profile'] = $this->mol_walletuser->GetWalletUserById($data[0]->Id);
   
   if ($data != null) {
    foreach ($data as $row) {
      
     $encode = $this->mol_setting->GetKey('UserKey');
    //  var_dump($encode);exit;
     foreach ($encode as $item) {

      $enpass = $this->lib_crypted->Decrypt_password($row->password, $item->Value);
      
      if ($user == strtoupper($row->username) && $password == $enpass) {
        
       return $this->response([
        'status' => true,
        'message' => 'Login Successful.',
        'data' => $user,
        'profile' => $data['profile'],
       ], REST_Controller::HTTP_OK);
      } else {

        return $this->response([
        'status' => false,
        'message' => 'Wrong user or password.',
        'data' => 'None',
       ], REST_Controller::HTTP_BAD_REQUEST);
      }
     }
    }
   } else {
    return $this->response([
     'status' => false,
     'message' => 'Wrong user or password.',
     'data' => 'None',
    ], REST_Controller::HTTP_BAD_REQUEST);
   }
  } else {
    return $this->response([
    'status' => null,
    'message' => 'Please! Insert user and password.',
    'data' => 'None',
   ], REST_Controller::HTTP_BAD_REQUEST);
  }
 }
 /**
  * api สมัครสมาชิกของตู้น้ำมัน
  */
  public function Register_post(){
    $res = "False";
    $data['UserName'] = $this->input->post('tel');
    $Password = $this->input->post('password');
    $comfirm = $this->input->post('confirmpassword');
  if(empty($data['UserName']) || empty($Password) || empty($comfirm)){
   return $this->response( array ( 'message' => "Data Empty"),REST_Controller::HTTP_BAD_REQUEST);
  }
  $walletuser = $this->mol_walletuser->GetWalletUserByUserName($data['UserName']);
  if($walletuser != null){
    return $this->response( array ( 'message' => "Data Duplicate"),REST_Controller::HTTP_BAD_REQUEST);
  }
    if($Password == $comfirm ){
       if(!empty($data['UserName']) && !empty($Password)){
       $Key = $this->mol_setting->GetKey('UserKey');
       $data['EnPassword'] = $this->lib_crypted->Encrypt_password($Password,$Key[0]->Value);   

         if($this->mol_walletuser->InsertWalletUser($data)){
             
             $data['msg'] = 'คุณได้ทำการสมัครผ่านทางตู้ HappyOil สำเร็จแล้ว';
             $data['phone'] = $data['UserName'] ;
             $data['type'] = 'SMS';
             $userid = $this->mol_walletuser->GetWalletUserByUserName($data['UserName']);
             $data['UserId'] = null;
             if($userid != null){
               $data['UserId'] = $userid[0]->Id;
             }
       
             $sms = $this->lib_sms->SMS( $data );
             $data['result'] = $sms;
             $this->mol_sms->addLogsms($data);
             $res = "Success";
             return $this->response( array ( 'StatusCode' => $res , 'message' => "Register Success"),REST_Controller::HTTP_OK);
         }
     }
 }
 else {
   return $this->response( array ( 'message' => "Data Incorrect"),REST_Controller::HTTP_BAD_REQUEST);
 }
}

}