<?php

require APPPATH . 'libraries\REST_Controller.php';

class RouteMan extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();

        $params = array( 'type' => 'large', 'color' => 'red' );
        $this->load->model( array( 'mol_machine', 'mol_requestrouteman', 'mol_status', 'mol_routeman', 'mol_user', 'mol_setting', 'mol_oilorder' ) );
        $this->load->helper( 'string' );
        $this->load->library( 'lib_crypted' );

    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */
/**
 * api login ของ Routeman ของ Mobile
 */
    public function RouteManLogin_post() {

        $user = strtoupper( $this->input->post( 'username' ) );
        $password = $this->input->post( 'password' );
        // redirect( 'api/RouteMan/GenUID?data='.$user );

        if ( !empty( $user ) && !empty( $password ) ) {

            $data = $this->mol_routeman->GetUserRouteMan( $user );
            // var_dump( $data[0] );
            // exit;
            if ( $data != null ) {
                foreach ( $data as $row ) {
                    $encode = $this->mol_setting->GetKeyRouteMan( 'UserKey');

                    foreach ( $encode as $item ) {

                        $enpass = $this->lib_crypted->Decrypt_password( $row->password, $item->Value );
                        // var_dump( $enpass );
                        // exit;
                        if ( $user == strtoupper( $row->username ) && $password == $enpass ) {
                            $itemdata = array(
                                'RouteManId' => $row->Id,
                                'UserName' => $row->username,
                                'FirstName' => $row->FirstName,
                                'LastName' => $row->LastName
                            );

                            $this->response( [
                                'status' => true,
                                'message' => 'Login Successful.',
                                'data' => $itemdata,
                            ], REST_Controller::HTTP_OK );
                        } else {

                            $this->response( [
                                'status' => false,
                                'message' => 'Wrong user or password.',
                                'data' => 'None',
                            ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
                        }
                    }
                }
            } else {
                $this->response( [
                    'status' => false,
                    'message' => 'Wrong user or password.',
                    'data' => 'None1',
                ], REST_Controller::HTTP_BAD_REQUEST );
            }
        } else {
            $this->response( [
                'status' => null,
                'message' => 'Please! Insert user and password.',
                'data' => 'None',
            ], REST_Controller::HTTP_BAD_REQUEST );
        }
    }

    /**
     * api check token ของ Routeman 
     */
    public function RecieveCode_post() {

        $data['token'] = $this->input->post( 'token' );
        $data['machineid'] = $this->input->post( 'machineid' );
        $token = $this->mol_requestrouteman->selectRequestByToken( $data );
        if ( $token == NULL ) {
            $res = array( 'StatusCode' => 'Unsuccess' );
            return $this->response( $res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
        } else {
            $res = array( 'StatusCode' => 'Success' );
            return $this->response( $res, REST_Controller::HTTP_OK );
        }

    }
   /**
     * api สร้าง token ของ Routeman 
     */
    public function GenUID_get() {
        $result['routemanid'] = $this->input->post( 'routemanid' );
        $result['machineid'] = $this->input->post( 'machineid' );
        $data = random_string( 'unique', 8 );
        $arr = array(
            'DeleteFlag' => 0,
            'CreateUserId' => 1,
            'CreateDate' => date( 'Y-m-d H:i:s' ),
            'Ordering' => 0,
            'CustomerId' => 1,
            'StatusId' => 1,
            'Action' => 'Open',
            'Expired_Date' => date( 'Y-m-d H:i:s', strtotime( '+5 minutes', strtotime( date( 'Y-m-d H:i:s' ) ) ) ),
            'MachineId' =>$result['machineid'],
            'RouteManId' =>$result['routemanid'],
            'Token' => $data
        );
        if ( $this->db->insert( 'RequestRouteMan', $arr ) ) {
            $datastring = array(
                'uid' => $data
            );
            return $this->response( $datastring, REST_Controller::HTTP_OK );
        }
        $res = array( 'StatusCode' => 'Unsuccess' );
        return $this->response( $res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

    }
   /**
     * api สร้างคำขอต่างๆของ Routeman ทั้ง PM เปิดตู้ และ เติมน้ำมัน
     */
    public function RequestRouteMan_post() {

        $data['machine'] = $this->input->post( 'MachineName' );
        $res['RoutemanId'] = $this->input->post( 'routemanId' );
        $res['Action'] = $this->input->post( 'Action' );

        $machine = $this->mol_machine->selectMachineByName( $data['machine'] );

        foreach ( $machine as $item ) {
            $res['MachineId'] = $item->Id;

            if ( $this->mol_requestrouteman->InsertRequest( $res ) ) {
                if ( $this->mol_requestrouteman->InsertLogRouteMan( $res['RoutemanId'] ) ) {

                    return $this->response( array( 'StatusCode ' => 'Success' ), REST_Controller::HTTP_OK );
                }
            }

        }
        return $this->response( array( 'StatusCode ' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
    }
/**
 * test
 */
    public function RouteManPM_post() {

        return $this->response( $data, REST_Controller::HTTP_OK );
    }
/**
 * api เรียกดูข้อมูล คำสั่งเติมน้ำมัน
 */
    public function GetOilOrder_get() {
        $data['routemanid'] = $this->input->get( 'routemanid' );
        $datares = $this->mol_oilorder->SelectOrderByRouteManId( $data );

        return $this->response( array( 'data' => $datares ), REST_Controller::HTTP_OK );

    }
/**
 * api update token ของ routeman token คือ รหัสเฉพาะของ firebase ที่จะใช้ในการยิง Notification 
 */
    public function UpdateTokenRouteMan_post() {
        $data['Token'] = $this->input->post( 'Token' );
        $data['Username'] = $this->input->post( 'Username' );
        if ( $this->mol_routeman->UpdateTokenRouteManByUserName( $data ) ) {
            return $this->response( array( 'StatusCode' => 'Success' ), REST_Controller::HTTP_OK );
        }

        return $this->response( array( 'StatusCode' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

    }

}