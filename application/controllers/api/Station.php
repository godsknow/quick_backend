<?php

require APPPATH . 'libraries\REST_Controller.php';

class Station extends REST_Controller
{

 /**
  * Get All Data from this method.
  *
  * @return Response
  */

 public function __construct()
 {
  parent::__construct();
  //    $this->load->database();
  $this->load->model('mol_station');
 }

 /**
  * Get All Data from this method.
  *
  * @return Response
  */
/**
 * api เรียกดูข้อมูล station 
 */
 public function index_get()
 {

  $input = $this->input->get();
  if ($input != null) {
   $data = $this->mol_station->SelectStation($input)->result();
  } else {
   $data = $this->mol_station->selectStationAll();
  }

  $this->response($data, REST_Controller::HTTP_OK);
 }

 /**
  * Get All Data from this method.
  *
  * @return Response
  */

 public function index_post()
 {
  $input = $this->input->post();

  $this->db->insert('station', $input);

  $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
 }

}