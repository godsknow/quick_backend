<?php
   
require  APPPATH . 'libraries\REST_Controller.php';
     
class StationLocation extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
    //    $this->load->database();
       $this->load->model('mol_station');
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    /**
     * api เรียกข้อมูล สถานีที่ตั้งของ station
     */
    public function index_get()
     {   
        $input = $this->input->get();
        if($input != null) 
            $data = $this->mol_station->SelectLocation($input)->result();
        else 
            $data = $this->mol_station->selectStationLocation();
        
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->input->post();
        $this->db->insert('station',$input);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('station', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('items', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}