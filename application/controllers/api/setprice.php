<?php

require APPPATH . 'libraries/REST_Controller.php';

class Setprice extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();
        //    $this->load->database();
        $this->load->model( array( 'mol_machine', 'mol_station', 'mol_machineError', 'mol_logdaily', 'mol_pricelist' ) );
    }
/**
 * api ตั่งค่าราคาน้ำมัน ของ Mobile
 */
    public function Setprice_post() {

        $data['MachineId'] = $this->input->post( 'MachineId' );
        $data['price'] =  $this->input->post( 'price' );
        $dataResult = $this->mol_machine->setPrice( $data );

        if ( $dataResult == true ) {
            $result = array( 'StatusCode'=> 'Success' );
            $this->response( $result, REST_Controller::HTTP_OK );
        } else {
            $result = array( 'StatusCode'=> 'Unsuccess' );
            $this->response( $result, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    // public function index_post()
    // {
    //     $post       = $this->input->post();
    //     $input_data = json_decode( trim( file_get_contents( 'php://input' ) ), true );

    //     $arrData['"MHE".BoardNumber'] = $input_data['BoardNumber'];

    //     $data = $this->mol_pricelist->selectPriceAPI( $arrData );
    //     $valueString = strval( $data['data']['values'] );
    //     $valueString = str_replace( '.', '', $valueString );
    //     $numLen =  strlen( $valueString );
    //     if ( $numLen<3 )
    // {
    //         $valueString = $valueString.'000000000';
    //         $valueString = substr( $valueString, 0, 4 );
    //     }
    //     $data['data']['valueString'] = $valueString;
    //     // print_r( $data );
    // exit();
    //     // $data['data'] = array(
    //     //     'id'          => 6,
    //     //     'machineName' => 'WVS001',
    //     //     'boardNumber' => 'WVS-001',
    //     //     'oilName'     => 'E90',
    //     //     'values'      => 43.97,
    //     //     'valueString' => '4397'
    //     // );

    //     $this->response( $data, REST_Controller::HTTP_OK );
    // }
}