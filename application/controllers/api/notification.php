<?php

require APPPATH . 'libraries\REST_Controller.php';

class Notification extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();

        $params = array( 'type' => 'large', 'color' => 'red' );
        $this->load->model( array( 'mol_loglogin', 'mol_setting', 'mol_noti', 'mol_stockmove', 'mol_accountinvoice', 'mol_station', 'mol_oilorder' ) );
        $this->load->library( array( 'lib_postcurl', 'Lib_fcm' ) );
        $this->load->helper( 'url' );

        // $this->CI->load->controller( 'api/PushNotifications' );
    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */
    /**
     * api ทั้งหมดเป็นของ Mobile 
     */
    
    /**
     * api ยิง noti ของ routeman
     */
    public function GetNotiByRouteManId_get()
 {
        $data['RouteManId'] = $this->input->get( 'routemanid' );
        $dataResult = $this->mol_noti->GetNotiByRouteManId( $data['RouteManId'] );
        if ( $dataResult == null ) {
            $res = array( 'data' => 'NULL' );
            return $this->response( $res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
        } else {
            $res = array( 'data' => $dataResult );
            return $this->response( $res, REST_Controller::HTTP_OK );
        }
    }

    /**
     * api ยิง noti ของ routeman
     */
    public function NotiRouteMan_get()
 {

        // push token
        $token = $this->input->get( 'token' );
        // push message
        $message = $this->input->get( 'message' );
        // push title
        $title = $this->input->get( 'title' );
        // var_dump( $title );
        // exit;
        $this->lib_fcm->setTitle( $title );
        $this->lib_fcm->setMessage( $message );
        $this->lib_fcm->setIsBackground( false );
        $payload = array( 'notification' => '' );
        $this->lib_fcm->setPayload( $payload );

        /**
        * Send images in the notification
        */
        $this->lib_fcm->setImage( 'https://firebase.google.com/_static/9f55fd91be/images/firebase/lockup.png' );

        /**
        * Get the compiled notification data as an array
        */
        $json = $this->lib_fcm->getPush();

        $p = $this->lib_fcm->send( $token, $json['title'], $json['message'] );

        $data1 = json_decode( $p );
        $data['to'] = $token;
        $data['message'] = $message;
        $data['title'] = $title;
        // $draft2 = ( $data1->results[0]->message_id == null )? 0:1;
        $data['complete'] = ( $data1->success == 1 ) ? 1 : 0;
        $dataitem = ( $data1->success == 1 ) ? $data1->results[0]->message_id : $data1->results[0]->error;

        $add = $this->mol_noti->AddNoti( $data );

        //redirect( 'api/Notification/polar2?token='.$data['token'] .'&title='.$data['title'].'&message='.$data['message'] );
        redirect( 'api/Notification/Output?multicast_id=' . $data1->multicast_id . '&success=' . $data1->success . '&failure=' . $data1->failure . '&results=' . $dataitem );

    }
    /**
     * api output ข้อมูลหลังจากยิง  notification  
     */
    public function Output_get()
 {

        $data = $this->input->get();
        return $this->response( $data, REST_Controller::HTTP_OK );
    }

    //api แจ้งเตือนเมื่อ sotck น้ำมันหมด
    public function NotiStock_post()
 {
        $dataRes = $this->mol_stockmove->SelectStockOne();
        if ( $dataRes == null ) {
            return $this->response( array( 'data' => 'null' ), REST_Controller::HTTP_OK );
        }
        if ( $dataRes[0]->Remaining <= 20 ) {
            $dataResult = 'สเตชั่น : ';
            $dataResult .= $dataRes[0]->StationName;
            $dataResult .= ' เครื่อง : ';
            $dataResult .= $dataRes[0]->MachineName;
            $dataResult .= ' น้ำมันเหลือน้อย โปรดเติมน้ำมัน';

            // push token
            $token = $this->input->post( 'token' );
            // push message
            $message = $dataResult;
            // push title
            $title = 'ระดับน้ำมัน';

            $this->lib_fcm->setTitle( $title );
            $this->lib_fcm->setMessage( $message );
            $this->lib_fcm->setIsBackground( false );
            $payload = array( 'notification' => '' );
            $this->lib_fcm->setPayload( $payload );

            /**
            * Send images in the notification
            */
            $this->lib_fcm->setImage( 'https://firebase.google.com/_static/9f55fd91be/images/firebase/lockup.png' );

            /**
            * Get the compiled notification data as an array
            */
            $json = $this->lib_fcm->getPush();
            // var_dump( $token );
            // exit;
            $p = $this->lib_fcm->send( $token, $json['title'], $json['message'] );

            $data1 = json_decode( $p );
            $data['to'] = $token;
            $data['message'] = $message;
            $data['title'] = $title;
            // $draft2 = ( $data1->results[0]->message_id == null )? 0:1;
            $data['complete'] = ( $data1->success == 1 ) ? 1 : 0;
            $dataitem = ( $data1->success == 1 ) ? $data1->results[0]->message_id : $data1->results[0]->error;

            $add = $this->mol_noti->AddNoti( $data );

            //redirect( 'api/Notification/polar2?token='.$data['token'] .'&title='.$data['title'].'&message='.$data['message'] );
            redirect( 'api/Notification/Output?multicast_id=' . $data1->multicast_id . '&success=' . $data1->success . '&failure=' . $data1->failure . '&results=' . $dataitem );
        } else {
            return $this->response( array( 'data' => 'Normal' ), REST_Controller::HTTP_OK );
        }

    }

    //api แจ้งเตือนเมื่อเงินเต็มตู้

    public function NotiBalance_post()
 {
        $data['stationid'] = $this->input->post( 'stationid' );

        $dataRes = $this->mol_accountinvoice->SelectInvoice( $data );
        $dataRes2 = $this->mol_station->SelectBalanceStationById( $data );
        $sum = $dataRes[0]->AmountNet == $dataRes2[0]->balance;
        if ( $sum > 5000 ) {
            $dataResult = 'สเตชั่น : ';
            $dataResult .= $dataRes2[0]->stationCode;
            $dataResult .= ' จำนวนเงินเต็มตู้แล้ว';

            // push token
            $token = $this->input->post( 'token' );
            // push message
            $message = $dataResult;
            // push title
            $title = 'จำนวนเงินเต็มตู้';
            // var_dump( $title );
            // exit;
            $this->lib_fcm->setTitle( $title );
            $this->lib_fcm->setMessage( $message );
            $this->lib_fcm->setIsBackground( false );
            $payload = array( 'notification' => '' );
            $this->lib_fcm->setPayload( $payload );

            /**
            * Send images in the notification
            */
            $this->lib_fcm->setImage( 'https://firebase.google.com/_static/9f55fd91be/images/firebase/lockup.png' );

            /**
            * Get the compiled notification data as an array
            */
            $json = $this->lib_fcm->getPush();
            // var_dump( $token );
            // exit;
            $p = $this->lib_fcm->send( $token, $json['title'], $json['message'] );

            $data1 = json_decode( $p );
            $data['to'] = $token;
            $data['message'] = $message;
            $data['title'] = $title;
            // $draft2 = ( $data1->results[0]->message_id == null )? 0:1;
            $data['complete'] = ( $data1->success == 1 ) ? 1 : 0;
            $dataitem = ( $data1->success == 1 ) ? $data1->results[0]->message_id : $data1->results[0]->error;

            $add = $this->mol_noti->AddNoti( $data );

            //redirect( 'api/Notification/polar2?token='.$data['token'] .'&title='.$data['title'].'&message='.$data['message'] );
            redirect( 'api/Notification/Output?multicast_id=' . $data1->multicast_id . '&success=' . $data1->success . '&failure=' . $data1->failure . '&results=' . $dataitem );
        } else {
            return $this->response( array( 'data' => 'Normal' ), REST_Controller::HTTP_OK );
        }

    }

    //api แจ้งเตือนเมื่อ เตืมน้ำมันเสร็จ

    public function NotiOilOrderComplete_post()
 {
        $data['stationid'] = $this->input->post( 'stationid' );

        $dataRes = $this->mol_oilorder->SelectOilOrderByStationId( $data );

        if ( $dataRes == null ) {
            return $this->response( array( 'data' => 'null' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
        }
        if ( $dataRes[0]->RefilId != null ) {

            $dataResult = 'สเตชั่น : ';
            $dataResult .= $dataRes[0]->stationCode;
            $dataResult .= ' RouteMan เติมน้ำมันเสร็จแล้ว';
            // push token
            $token = $this->input->post( 'token' );
            // push message
            $message = $dataResult;
            // push title
            $title = 'เติมน้ำมัน';
            // var_dump( $title );
            // exit;
            $this->lib_fcm->setTitle( $title );
            $this->lib_fcm->setMessage( $message );
            $this->lib_fcm->setIsBackground( false );
            $payload = array( 'notification' => '' );
            $this->lib_fcm->setPayload( $payload );

            /**
            * Send images in the notification
            */
            $this->lib_fcm->setImage( 'https://firebase.google.com/_static/9f55fd91be/images/firebase/lockup.png' );

            /**
            * Get the compiled notification data as an array
            */
            $json = $this->lib_fcm->getPush();
            // var_dump( $token );
            // exit;
            $p = $this->lib_fcm->send( $token, $json['title'], $json['message'] );

            $data1 = json_decode( $p );
            $data['to'] = $token;
            $data['message'] = $message;
            $data['title'] = $title;
            // $draft2 = ( $data1->results[0]->message_id == null )? 0:1;
            $data['complete'] = ( $data1->success == 1 ) ? 1 : 0;
            $dataitem = ( $data1->success == 1 ) ? $data1->results[0]->message_id : $data1->results[0]->error;

            $add = $this->mol_noti->AddNoti( $data );

            //redirect( 'api/Notification/polar2?token='.$data['token'] .'&title='.$data['title'].'&message='.$data['message'] );
            redirect( 'api/Notification/Output?multicast_id=' . $data1->multicast_id . '&success=' . $data1->success . '&failure=' . $data1->failure . '&results=' . $dataitem );
        } else {
            return $this->response( array( 'data' => 'null' ), REST_Controller::HTTP_OK );
        }

    }

    //api แจ้งเตือนเมื่อมีคำสั่งเติมน้ำมัน

    public function NotioilOrder_post()
 {
        $data['routemanid'] = $this->input->post( 'routemanid' );

        $dataRes = $this->mol_oilorder->SelectOrderByRouteManId( $data );
        if ( $dataRes == null ) {
            return $this->response( array( 'data' => 'null' ), REST_Controller::HTTP_OK );
        }

        $dataResult = 'ออร์เดอร์ที่ : ';
        $dataResult .= $dataRes[0]->PurchaseNo;
        $dataResult .= ' สเตชั่น : ';
        $dataResult .= $dataRes[0]->StationCode;
        $dataResult .= ' เครื่อง : ';
        $dataResult .= $dataRes[0]->MachineName;
        $dataResult .= ' น้ำมัน : ';
        $dataResult .= $dataRes[0]->OilName;
        $dataResult .= ' ปริมาณ : ';
        $dataResult .= $dataRes[0]->Qty;
        $dataResult .= ' ลิตร';

        // var_dump( $dataResult );
        // exit;
        // push token
        $token = $this->input->post( 'token' );
        // push message
        $message = $dataResult;
        // push title
        $title = 'เติมน้ำมัน';
        // var_dump( $title );
        // exit;

        $this->lib_fcm->setTitle( $title );
        $this->lib_fcm->setMessage( $message );
        $this->lib_fcm->setIsBackground( false );
        $payload = array( 'notification' => '' );
        $this->lib_fcm->setPayload( $payload );

        /**
        * Send images in the notification
        */
        $this->lib_fcm->setImage( 'https://firebase.google.com/_static/9f55fd91be/images/firebase/lockup.png' );

        /**
        * Get the compiled notification data as an array
        */
        $json = $this->lib_fcm->getPush();
        // var_dump( $token );
        // exit;
        $p = $this->lib_fcm->send( $token, $json['title'], $json['message'] );
        // var_dump( $p );
        // exit;
        $data1 = json_decode( $p );
        $data['to'] = $token;
        $data['message'] = $message;
        $data['title'] = $title;
        // $draft2 = ( $data1->results[0]->message_id == null )? 0:1;
        $data['complete'] = ( $data1->success == 1 ) ? 1 : 0;
        $dataitem = ( $data1->success == 1 ) ? $data1->results[0]->message_id : $data1->results[0]->error;

        $add = $this->mol_noti->AddNoti( $data );

        //redirect( 'api/Notification/polar2?token='.$data['token'] .'&title='.$data['title'].'&message='.$data['message'] );
        redirect( 'api/Notification/Output?multicast_id=' . $data1->multicast_id . '&success=' . $data1->success . '&failure=' . $data1->failure . '&results=' . $dataitem );

    }
    /**
     * api สมัครข้อมูลจาก firebase  ลง mysql 
     */
    public function Registerfirebase_get()
 {

        $data['userid'] = $this->input->get( 'userid' );
        $data['username'] = $this->input->get( 'username' );
        $data['routemanid'] = $this->input->get( 'routemanid' );
        $data['Token'] = $this->input->get( 'Token' );

        $dbResult['New'] = $this->mol_loglogin->GetLogLoginByToken( $data );
        $dbResult['Old'] = $this->mol_loglogin->GetLogLoginOld( $dbResult );
        if ( $dbResult['Old'] != null ) {
            foreach ( $dbResult['Old'] as $item ) {
                $this->mol_loglogin->DeleteLogLogin( $item );
            }
        }
        $data['CusId'] = $this->mol_customer->GetCustomer( $dbResult['New'][0]->CustomerId );
        $result = array(
            'DeleteFlag' => 0,
            'CreateUserId' => 1,
            'CreateDate' => date( 'Y-m-d H:i:s' ),
            'CustomerId' => $data['CusId'],
            'Ordering' => 0,
            'StatusId' => 1,
            'UserName' => $data['username'],
            'UserId' => $data['userid'],
            'RouteManId' => $data['routemanid'],
            'Token' => $data['Token'],
        );
        $res = array( 'StatusCode' => 'Unsuccess' );
        if ( $this->mol_loglogin->AddLogLogIn( $result ) ) {
            $res = array( 'StatusCode' => 'Success' );
            return $this->response( $res, REST_Controller::HTTP_OK );
        }
        return $this->response( $res, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
    }

}