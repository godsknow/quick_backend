<?php

require APPPATH . 'libraries/REST_Controller.php';

class Pricelist extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();
        //    $this->load->database();
        $this->load->model( array( 'mol_machine', 'mol_station', 'mol_machineError', 'mol_logdaily', 'mol_pricelist' ) );
    }

    /**
     * api เรียกข้อมูลขอราคาน้ำมัน ของแต่ละตู้
     */
    public function ShowPrice_get() {

        $data['BoardNumber'] = $this->input->get( 'BoardNumber' );

        $dataResult = $this->mol_machine->selectpriceByBoardNumber( $data['BoardNumber'] );
        if($dataResult != null){
            foreach ($dataResult as $item) {
                $result = array(
                    'MachineName' => $item->MachineName,
                    'OilName' => $item->OilName,
                    'Value' => $item->Value,
                );
            }
            $this->response( $result, REST_Controller::HTTP_OK );
        }
         else {
            $result = array( 'StatusCode'=> 'Unsuccess' );
            $this->response( $result, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

}