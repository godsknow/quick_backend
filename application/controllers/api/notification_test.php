<?php
use Kreait\Firebase\Messaging\CloudMessage;
require APPPATH . 'libraries\REST_Controller.php';

class notification_test extends REST_Controller 
 {

    public function __construct() {
        parent::__construct();
        $params = array( 'type' => 'large', 'color' => 'red' );
        $this->load->library( 'noti' );
    }

    public function a() {
        $message = CloudMessage::withTarget( /* see sections below */ )
        ->withNotification( Notification::create( 'Title', 'Body' ) )
        ->withData( ['key' => 'value'] );

        $messaging->send( $message );
        $deviceToken = '...';

        $message = CloudMessage::withTarget( 'token', $deviceToken )
        ->withNotification( $notification ) // optional
        ->withData( $data ) // optional
        ;

        $message = CloudMessage::fromArray( [
            'token' => $deviceToken,
            'notification' => [/* Notification data as array */], // optional
            'data' => [/* data array */], // optional
        ] );

        $messaging->send( $message );
    }
}
?>