<?php

require APPPATH . 'libraries/REST_Controller.php';

class Password extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();
        //    $this->load->database();
        $this->load->model( array( 'mol_sms','mol_machine', 'mol_setting', 'mol_user', 'mol_routeman','mol_walletuser' ) );
        $this->load->library( array( 'lib_crypted','lib_sms' ) );
    }
    /**
     * api check ข้อมุลของเจ้าของตู้
     */
    public function CheckUser_post() {
        $name = $this->input->post( 'username' );
        if ( $this->mol_user->GetUser( $name ) != null ) {
            $result = array( 'StatusCode'=> 'Success' );
            return $this->response( $result, REST_Controller::HTTP_OK );
        } else {
            $result = array( 'StatusCode'=> 'Unsuccess' );
            return $this->response( $result, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
        }

    }
    /**
     * api check ข้อมูลของ Routeman
     */
    public function CheckRouteMan_post() {
        $name = $this->input->post( 'username' );
        if ( !is_null( $this->mol_routeman->GetUserRouteMan( $name ) ) ) {
            $result = array( 'StatusCode'=> 'Success' );
            return $this->response( $result, REST_Controller::HTTP_OK );
        } else {
            $result = array( 'StatusCode'=> 'Unsuccess' );
            return $this->response( $result, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
        }
    }
/**
 * api reset รหัสผ่านของเจ้าของตู้
 */
    public function ResetPasswordUser_post() {

        $data['username'] = $this->input->post( 'username' );
        $data['password'] =  $this->input->post( 'password' );
        $datauser = $this->mol_user->GetUser( $data['username'] );

        if ( !empty( $data['username'] ) && !empty( $data['password'] ) && $datauser != null ) {

            $encode = $this->mol_setting->GetKey( 'UserKey', $datauser[0]->customerId );
            $data['enpass'] = $this->lib_crypted->Encrypt_password( $data['password'], $encode[0]->Value );

            if ( $this->mol_user->UpdatePasswordUser( $data ) ) {

                $result = array( 'StatusCode'=> 'Success' );
                return   $this->response( $result, REST_Controller::HTTP_OK );

            } else {

                $result = array( 'StatusCode'=> 'Unsuccess' );
                return  $this->response( $result, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

            }
        } else return  $this->response( array( 'StatusCode'=> 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

    }
    /**
     * api reset รหัสผ่านของ RouteMan
     */
    public function ResetPasswordRouteMan_post() {

        $data['username'] = $this->input->post( 'username' );
        $data['password'] =  $this->input->post( 'password' );
        $datarouteman = $this->mol_routeman->GetUserRouteMan( $data['username'] );

        if ( !empty( $data['username'] ) && !empty( $data['password'] ) && $datarouteman != null ) {

            $encode = $this->mol_setting->GetKey( 'UserKey', $datarouteman[0]->customerId );
            $data['enpass'] = $this->lib_crypted->Encrypt_password( $data['password'], $encode[0]->Value );

            if ( $this->mol_routeman->UpdatePasswordRouteMan( $data ) ) {

                $result = array( 'StatusCode'=> 'Success' );
                return   $this->response( $result, REST_Controller::HTTP_OK );

            } else {

                $result = array( 'StatusCode'=> 'Unsuccess' );
                return  $this->response( $result, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

            }

        } else return  $this->response( array( 'StatusCode'=> 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

    }
    /**
     * api reset รหัสผ่านของตู้น้ำมัน
     */

    public function ResetPasswordKiosk_post() {

        $username = $this->input->post( 'username' );
        $data['password'] =  $this->input->post( 'password' );
        $walletname = $this->mol_walletuser->GetWalletUserByUserName( $username );
        if($walletname == null){
            return   $this->response( array('StatusCode' => false,'Message' => 'Wallet Is Null'), REST_Controller::HTTP_BAD_REQUEST);
        }else {
            $data['Id'] = $walletname;
            $data['UserId'] = $walletname[0]->Id;
        }
        // var_dump($username);exit;
        if ( !empty( $username ) && !empty( $data['password'] ) && $data['Id'] != null ) {

            $encode = $this->mol_setting->GetKey( 'UserKey');
            $data['enpass'] = $this->lib_crypted->Encrypt_password( $data['password'], $encode[0]->Value );

            if ( $this->mol_walletuser->UpdatePassword( $data ) ) {
                $data['msg'] = 'คุณได้ทำการเปลี่ยนรหัสผ่านทางตู้ HappyOil สำเร็จแล้ว';
                $data['phone'] = $username;
                $data['type'] = 'SMS';
                $sms = $this->lib_sms->SMS( $data );
                $data['result'] = $sms;
                $this->mol_sms->addLogsms($data);
                $result = array( 'StatusCode'=> 'Success' );
                return   $this->response( $result, REST_Controller::HTTP_OK );

            } else {

                $result = array( 'StatusCode'=> 'Unsuccess' );
                return  $this->response( $result, REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

            }

        } else return  $this->response( array( 'StatusCode'=> 'Unsuccess1' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

    }

}