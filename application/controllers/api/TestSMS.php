<?php

require APPPATH . 'libraries\REST_Controller.php';

class TestSMS extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();
        //    $this->load->database();
        $this->load->model( array('mol_station','mol_sms','mol_walletuser'));
        $this->load->library( array( 'lib_sms' ) );
    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function RequestOTP_get() {

        $data = $this->lib_sms->OTP( $file );

        $this->response( $data, REST_Controller::HTTP_OK );
    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function SendSMS_post() {
        $data['msg'] = $this->input->post( 'msg' );
        $data['phone'] = $this->input->post( 'phone' );
        $data['type'] =$this->input->post('type');
        $userid = $this->mol_walletuser->GetWalletUserByUserName($data['phone']);
        $data['UserId'] = null;
        if($userid != null){
            $data['UserId'] = $userid[0]->Id;
        }
        $result = $this->lib_sms->SMS( $data );
        $data['result'] = $result;
        
        $this->mol_sms->addLogsms($data);

        $this->response( $result, REST_Controller::HTTP_OK );
    }

}