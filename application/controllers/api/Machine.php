<?php

require APPPATH . 'libraries\REST_Controller.php';

class Machine extends REST_Controller
{

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();

        $params = array('type' => 'large', 'color' => 'red');
        $this->load->model('mol_machine');
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    /**
     * api เรียกข้อมูล ข้อมูลเครอื่งหรือ บอร์ด ให้ฝั่ง mobile
     */
    public function ShowMachine_get()
    {
        $data = $this->mol_machine->selectMachine();

        return $this->response($data, REST_Controller::HTTP_OK);

    }
/**
     * api เรียกข้อมูล ข้อมูลน้ำมันคงเหลือ ให้ฝั่ง mobile
     */
    public function ShowMachineRemain_get()
    {
        $machineName = $this->input->get('MachineName');
        $data        = $this->mol_machine->selectMachineRemain($machineName);

        return $this->response($data, REST_Controller::HTTP_OK);
    }
/**
     * api เรียกข้อมูล ข้อมูลที่อยู่ของเครื่อง ให้ฝั่ง mobile
     */
    public function LocationMachine_get()
    {
        $machinelocation = $this->mol_machine->locationMachine();
        return $this->response($machinelocation, REST_Controller::HTTP_OK);
    }
    /**
     * api เรียกข้อมูล ข้อมูลราคาน้ำมันของเครื่อง ให้ฝั่ง mobile
     */
    public function SelectMachineOilPrice_get()
    {
        $data        = $this->mol_machine->SelectMachineOilPrice();
        return $this->response($data, REST_Controller::HTTP_OK);
    }

}