<?php

require APPPATH . 'libraries\REST_Controller.php';

class StockMove extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();
        //    $this->load->database();
        $this->load->model( 'Mol_stockmove' );
        $this->load->model( 'Mol_station' );
        $this->load->model( 'Mol_machine' );
        $this->load->model( 'Mol_oiltype' );
    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */
/**
 * api  เรียกข้อมูล stockMove 
 */
    public function index_get()
 {

        $input = $this->input->get();
        if ( $input != null ) {
            $data = $this->mol_stockmove->SelectStockmMove( $input )->result();
        } else {
            $data = $this->mol_stockmove->SelectStockmMoveAll();
        }

        $this->response( $data, REST_Controller::HTTP_OK );
    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */
/**
 *  api เพิ่มข้อมู stockmove ลง database
 */
    public function AddStockMove_post()
 {
        $input['Token']       = $this->input->post( 'Token' );
        $input['location']    = $this->input->post( 'location' );
        $input['refuel']      = $this->input->post( 'refuel' );
        $input['stationCode'] = $this->input->post( 'stationCode' );
        $input['typeoil']     = $this->input->post( 'typeoil' );
        $input['des']         = $this->input->post( 'description' );
        $input['machine']     = $this->input->post( 'machine' );

        //$data['station'] = $this->Mol_station->SelectStationStockMove( $input['stationCode'] )->result();
        //$data['oil']     = $this->Mol_oiltype->selectOilTypeAll( $input['typeoil'] )->result();
        $data['machine'] = $this->Mol_machine->selectMachineAll( $input['machine'] )->result();
        $status          = 1;
        if ( $data['machine'] == null ) {
            $this->response( ['Empty Data.'], REST_Controller::HTTP_OK );
        }

        foreach ( $data['machine'] as $row2 ) {

            if ( $row2->stationCode != $input['stationCode'] ) {
                $this->response( ['Data is not Correct.'], REST_Controller::HTTP_OK );
            }
            if ( $row2->OilType != $input['typeoil'] ) {
                $this->response( ['Data is not Correct.'], REST_Controller::HTTP_OK );
            }
            $a = array(
                'StationId'   => $row2->StationId,
                'Direction'   => 'IN',
                'Capacity'    => $input['refuel'],
                'OilTypeId'   => $row2->OilTypeId,
                'Description' => $input['des'],
                'MachineId'   => $row2->Id,
                'StatusId'    => $status,
                'Deleteflag'  => false,
                'CreateDate'  => '2020-07-21',
                'Ordering'    => 0,
            );
            $this->db->insert( 'StockMove', $a );
            $this->response( ['Data created successfully.'], REST_Controller::HTTP_OK );

        }
    }

}