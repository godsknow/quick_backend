<?php
require APPPATH . 'libraries\REST_Controller.php';

class QrCodes extends REST_Controller
{

 /**
  * Get All Data from this method.
  *
  * @return Response
  */

 public function __construct()
 {
  parent::__construct();
  $this->load->helper('url');
  $this->load->helper('string');

  $params = array('type' => 'large', 'color' => 'red');
  $this->load->model(array('mol_oilorder', 'mol_status', 'mol_station', 'mol_oiltype', 'mol_qrcode'));
 }

 /**
  * Get All Data from this method.
  *
  * @return Response
  */

  /**
   * api สร้าง QrCode โดยใส่ข้อมูล Random และ save path ลง database
   */
 public function GenQrCode_get()
 {
  $data['img_url'] = '';
  $this->load->library('ciqrcode');
  $qr_image = rand() . '.png';
  $params['data'] = random_string('unique', 8);
  $params['level'] = 'H';
  $params['size'] = 10;
  $params['savename'] = FCPATH . 'uploads/qr_image/' . $qr_image;
  // var_dump( FCPATH );
  exit;
  if ($this->ciqrcode->generate($params)) {
   $data['img_url'] = $params['savename'];
   $params['qr_image'] = $qr_image;
   $this->mol_qrcode->Addqrcode($params);

  }

  return $this->response($data, REST_Controller::HTTP_OK);
 }
/**
 * api insert QrCode ของ ตู้น้ำมัน
 */
 public function AddQrCode_post()
 {
  $data['date'] = $this->input->post('date');
  $data['text'] = $this->input->post('text');
  $data['ImageName'] = $this->input->post('Imagename');
  $status = $this->mol_status->GetStatus('Enable');

  foreach ($status as $item) {
   $data['StatusId'] = $item->Id;
  }
  if ($this->mol_qrcode->Addqrcode($data)) {
   return $this->response('true', REST_Controller::HTTP_OK);
  } else {
   return $this->response('False', REST_Controller::HTTP_BAD_REQUEST);
  }

 }

 /**
  * api ที่เอาไว้ check ข้อมูลของ QrCode ว่าตรงกันกับใน database ไหม
  */
 public function CheckQrCode_post()
 {
  try {
   $data['date'] = $this->input->post('date');
   $data['text'] = $this->input->post('text');
   // $data['ImageName'] = $this->input->post( 'Imagename' );

   $qr = $this->mol_qrcode->SelectQrCode($data);
   foreach ($qr as $item) {
    if ($item->Expired_Date >= date('Y-m-d H:i:s')) {
     throw new Exception('Error Processing Request', 1);
    }
    $res = true;
    $this->rest->format('application/json');
    return $this->response($res, REST_Controller::HTTP_OK);

   }
  } catch (\Throwable $e) {
   return $e;
  }

 }

}