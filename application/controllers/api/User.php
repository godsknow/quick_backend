<?php

require APPPATH . 'libraries\REST_Controller.php';

class RouteMan extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct()
 {
        parent::__construct();

        $params = array( 'type' => 'large', 'color' => 'red' );
        $this->load->model( array( 'mol_machine', 'mol_requestrouteman', 'mol_status', 'mol_routeman', 'mol_user', 'mol_setting', 'mol_oilorder' ) );
        $this->load->helper( 'string' );
        $this->load->library( 'lib_crypted' );

    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */
/**
 *  api อัพเดท token จาก firebasae 
 */
    public function UpdateToken_post() {
        $data['Token'] = $this->input->post( 'Token' );
        $data['Username'] = $this->input->post( 'Username' );
        if ( $this->mol_user->UpdateTokenByUserName( $data ) ) {
            return $this->response( array( 'StatusCode' => 'Success' ), REST_Controller::HTTP_OK );
        }

        return $this->response( array( 'StatusCode' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

    }

}