<?php
   
require  APPPATH . 'libraries\REST_Controller.php';

     
class LoginOwner extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       
       $params = array('type' => 'large', 'color' => 'red');
       $this->load->model(array('mol_user','mol_setting','mol_customer'));

      //  require_once('Header.php');
	   //load lib custom
		// $params = array('type' => 'large', 'color' => 'red');
		$this->load->library('lib_crypted');
      // $CI =& get_instance();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
/**
 * api login ของ app mobile ของเจ้าของตู้
 */
      public function index_post() {
         // Get the post data

         $user = strtoupper($this->input->post('username'));
         $password = $this->input->post('password');

         if(!empty($user) && !empty($password)){

            $data = $this->mol_user->GetUser($user);

               if($data != null){
                    foreach($data as $row){
                        $encode = $this->mol_setting->GetKey('UserKey');
                        $cuscode = $this->mol_customer->GetCustomer($row->customerId);
                        //  var_dump($cuscode);exit();
                        if($cuscode != null){
                            foreach($cuscode as $cus){
                                foreach($encode as $item){

                                    $enpass = $this->lib_crypted->Decrypt_password($row->password,$item->Value);

                                    if($user == strtoupper($row->username) && $password == $enpass){

                                        $this->response([
                                                'status' => TRUE,
                                                'message' => 'Login Successful.',
                                                'data' => $user,
                                                'Customer' => $cus->CusCode
                                        ], REST_Controller::HTTP_OK);
                                    }else{

                                        $this->response([ 
                                            'status' => FALSE,
                                            'message' => 'Wrong user or password.',
                                            'data' => 'None'
                                        ], REST_Controller::HTTP_BAD_REQUEST);
                                    }
                                }
                            }
                        }
                    }
                }

               else {
                  $this->response([ 
                     'status' => FALSE,
                     'message' => 'Wrong user or password.',
                     'data' => 'None'
                  ], REST_Controller::HTTP_BAD_REQUEST);
               }
            }else{
             $this->response([ 
               'status' => NULL,
               'message' => 'Please! Insert user and password.',
               'data' => 'None'
            ], REST_Controller::HTTP_BAD_REQUEST);
         }
      }


     }