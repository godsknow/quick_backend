<?php

require APPPATH . 'libraries\REST_Controller.php';

class PushNotifications extends REST_Controller
 {
    /**
    * api ตัวอย่างการยิง Notification 
    */

    public function __construct() {
        parent::__construct();

        $params = array( 'type' => 'large', 'color' => 'red' );
        $this->load->library( 'Lib_fcm' );
    }

    public function sendNotification_get() {

        // $data['link'] = $this->input->get( 'path' );
        $data['token'] = $this->input->post( 'token' );
        $data['title'] = $this->input->post( 'title' );
        $data['message'] = $this->input->post( 'message' );
        // push token
        $token = $data['token'];
        // $token = $this->input->post( 'Registratin_id' );
        // push message
        $message = $data['message'];
        // $message = $this->input->post( 'message' );
        // push title
        $title = $data['title'];
        // $title = $this->input->post( 'title' );

        $this->lib_fcm->setTitle( $title );
        $this->lib_fcm->setMessage( $message );

        /**
        * set to true if the notificaton is used to invoke a function
        * in the background
        */
        $this->lib_fcm->setIsBackground( false );

        /**
        * payload is userd to send additional data in the notification
        * This is purticularly useful for invoking functions in background
        * -----------------------------------------------------------------
        * set payload as null if no custom data is passing in the notification
        */
        $payload = array( 'notification' => '' );
        $this->lib_fcm->setPayload( $payload );

        /**
        * Send images in the notification
        */
        $this->lib_fcm->setImage( 'https://firebase.google.com/_static/9f55fd91be/images/firebase/lockup.png' );

        /**
        * Get the compiled notification data as an array
        */
        $json = $this->lib_fcm->getPush();

        $p = $this->lib_fcm->send( $token, $json['title'], $json['message'] );

        $data1 = json_decode( $p );
        $item = array(
            'multicast_id' => $data1->multicast_id,
            'success' => $data1->success,
            'failure' => $data1->failure,
            'results' => ( $data1->results[0]->error == NULL )? $data1->result[0]:$data1->results[0]->error,
        );

        redirect( 'api/Notification/polar2?multicast_id='.$data1->multicast_id.'&success='.$data1->success.'&failure='.$data1->failure.'&results='.$data1->results[0]->error );
    }

    /**
    * Send to multiple devices
    */

    public function sendToMultiple_post() {

        $token = $this->input->post( 'Registratin_id' );
        // array( 'Registratin_id1', 'Registratin_id2' );
        // array of push tokens
        $message = $this->input->post( 'message' );
        // push title
        $title = $this->input->post( 'title' );

        $this->load->library( 'Lib_fcm' );
        $this->Lib_fcm->setTitle( $title );
        $this->Lib_fcm->setMessage( $message );
        $this->Lib_fcm->setIsBackground( false );
        // set payload as null
        $payload = array( 'notification' => '' );
        $this->Lib_fcm->setPayload( $payload );
        $this->Lib_fcm->setImage( 'https://firebase.google.com/_static/9f55fd91be/images/firebase/lockup.png' );
        $json = $this->Lib_fcm->getPush();

        $data = array();
        foreach ( $token as $key=>$val ) {
            $data[] = array( 'Registratin_id'=>$val, 'title'=>$title, 'message'=>$message );
        }
        /**
        * Send to multiple
        *
        * @param array  $token     array of firebase registration ids ( push tokens )
        * @param array  $json      return data from getPush() method
        */
        $result = $this->Lib_fcm->sendMultiple( $data, $json );
    }
}