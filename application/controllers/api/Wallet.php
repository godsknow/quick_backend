<?php

require APPPATH . 'libraries\REST_Controller.php';

class Wallet extends REST_Controller
 {

    /**
    * Get All Data from this method.
    *
    * @return Response
    */

    public function __construct() {
        parent::__construct();

        $params = array( 'type' => 'large', 'color' => 'red' );
        $this->load->model( array( 'mol_walletuser', 'mol_wallet', 'mol_setting', 'mol_customer', 'mol_user' ) );
        $this->load->helper( array( 'string', 'url' ) );

    }

    /**
    * Get All Data from this method.
    *
    * @return Response
    */
/**
 * api ข้อมูล wallet ของลูกค้าและเงินคงเหลือ
 */
    public function GetWallet_get() {
        $data = $this->mol_wallet->GetWallet();
        return $this->response( $data, REST_Controller::HTTP_OK );
    }
    /**
     * api เพิ่มข้อมูล wallet ของลูกค้า
     */
    public function AddWallet_post() {
        $data['TelNumber'] = $this->input->post( 'TelNumber' );
        $data['Value'] = $this->input->post( 'Value' );
        $data['username'] = $this->input->post( 'username' );

        $data['userid'] = $this->mol_user->GetUser( $data['username'] );
        $data['cus'] = $this->mol_customer->GetCustomerByCusCode( $dt['Customer'] );
        $data['WalletUser'] = $this->mol_walletuser->GetWalletUserByPhoneNumber( $data['TelNumber'] );

        if ( $this->mol_wallet->InsertWallet( $data ) ) {
            return $this->response( array( 'StatusCode' => 'Success' ), REST_Controller::HTTP_OK );
        } else return $this->response( array( 'StatusCode' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );

    }
    /**
     * api แก้ไขข้อมูล wallet ของลูกค้า
     */
    public function EditWallet_post() {
        $data['Id'] = $this->input->post( 'WalletUserId' );
        $data['Value'] = $this->input->post( 'Value' );

        if ( $this->mol_wallet->UpdateWallet( $data ) ) {
            return $this->response( array( 'StatusCode' => 'Success' ), REST_Controller::HTTP_OK );
        } else return $this->response( array( 'StatusCode' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
    }
/**
 * api ลบข้อมูล wallet ของลูกค้า
 */
    public function DeleteWallet_get() {
        $data['Id'] = $this->input->get( 'WalletUserId' );

        if ( $this->mol_wallet->DeleteWalletUser( $data['Id'] ) ) {
            return $this->response( array( 'StatusCode' => 'Success' ), REST_Controller::HTTP_OK );
        } else return $this->response( array( 'StatusCode' => 'Unsuccess' ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR );
    }
}