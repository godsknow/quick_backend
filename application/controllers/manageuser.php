<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manageuser extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        //load lib CI
        $this->load->helper('url');

        //load lib custom
        $arrBlank = array();
        $this->load->library('lib_session');
        $this->load->library('lib_crypted');
        $this->load->library('lib_template', $arrBlank);

        // $this->load->library(array('form_validation', 'lib_crypted'));

        //Load model
        $this->load->model(array('mol_manage_user', 'mol_user', 'mol_setting', 'mol_customer'));

        //check session
        $this->lib_session->chkSession();
    }

    public function index()
    {
        $this->load->view('welcome_message');
    }

    public function manageuser1()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrContent['data'] = $this->mol_manage_user->selectUserdata();
        // var_dump( $arrData );

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'จัดการผู้ใช้งาน',
            'parent' => 'จัดการผู้ใช้งาน',
            'page' => 'ข้อมูลผู้ใช้งาน',
            'link' => 'manageuser/manage_user');

        $arrFooter['footer'] = $setTemplate['footer'];
        //var_dump( $arrContent );
        exit();
        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/manageuser', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function setRole1()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'SETTING';
        $arrMenu['page'] = 'ตั้งค่าข้อมูลลูกค้า';
        $arrData['duration'] = 20;
        $arrData['offset'] = 0;

        if (isset($_GET['UserName']) && intval($_GET['UserName']) != null) {
            $arrData['UserName'] = $_GET['UserName'];
            $arrData['selectRole'] = $this->input->post('selectRole');
            //$arrContent['data'] = $this->mol_pricelist->UpdateOilvalue( $arrData );
            $arrContent['data'] = $this->mol_manage_user->GetPermission($arrData);
        }
        $arrContent['UserName'] = $_GET['UserName'];
        $arrContent['select'] = $this->mol_manage_user->GetRole();
        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'ตั้งค่าการเข้าใช้งาน',
            'parent' => 'การตั้งค่า',
            'page' => 'ตั้งค่าข้อมูลลูกค้า',
            'link' => 'Manageuser/manageuser',
        );
        $arrFooter['footer'] = $setTemplate['footer'];
        //   print_r($arrContent['data']);exit;
        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/setrole', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function getRole()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'Development';
        $arrMenu['page'] = 'ตั้งค่ากลุ่มผู้ใช้งาน';
        $arrData['duration'] = 20;
        $arrData['offset'] = 0;

        $arrContent['data'] = $this->mol_manage_user->SelectKeyAll($arrData);
        $arrContent['grouproledata'] = $this->mol_manage_user->SelectGroupRole($arrData);
        //var_dump($arrContent['grouproledata']);
        // exit;

        $arrContent['data']['linkedit'] = 'setkey';
        $arrContent['data']['linkedit'] = 'setgroupkey';

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'ตั้งค่าข้อมูลผู้ใช้งาน',
            'parent' => '--------',
            'page' => 'ตั้งค่าข้อมูลผู้ใช้งาน',
            'link' => 'Manageuser/manageuser',
        );
        $arrFooter['footer'] = $setTemplate['footer'];

        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/role', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function UpdateRoleUser()
    {
        $arr['User'] = $this->input->post('hiddenId');
        $arr['Value'] = $this->input->post('selectRole');
        $data = $this->mol_user->GetUser($arr['User']);
        $role = $this->mol_manage_user->GetRoleById($arr['Value']);

        foreach ($data as $item) {

            if ($role != null) {

                foreach ($role as $item1) {
                    $result = array(
                        "UserId" => $item->Id,
                        "KeyGroupId" => $item1->Id,
                        "DeleteFlag" => 0,
                        'CreateUserId' => 1,
                        "CreateDate" => date('Y-m-d H:i:s'),
                        "Ordering" => 0,
                        "CustomerId" => 1,
                    );
                    $this->db->insert('UserPermission', $result);
                }
            }
            $this->db->set('KeyGroupId', $arr['Value']);
            $this->db->where('UserId', $item->Id);
            $this->db->update('UserPermission');
        }
        redirect('manageuser/manageuser', 'refresh');

    }

    public function setkey()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'USER';
        $arrMenu['page'] = 'ตั้งค่าข้อมูลผู้ใช้งาน';
        $arrData['duration'] = 20;
        $arrData['offset'] = 0;

        if (isset($_GET['id']) && intval($_GET['id']) > 0) {
            $arrData['id'] = $_GET['id'];
            $arrContent['data'] = $this->mol_manage_user->selectSetOilType($arrData)->result();
        }
        $arrKey = $this->mol_manage_user->selectKeyAll();
        $arrContent['data']['key'] = $arrKey['result'];
        //  echo'<br/>data =';print_r($arrContent['data']);
        //  $arrMenu['customer'] = array('happyoil','wealthoil');
        $arrContent['template'] = array('header' => 'ตั้งค่าข้อมูลผู้ใช้งาน',
            'parent' => 'ผู้ใช้งาน',
            'link' => 'station/setkey',
            'page' => 'ตั้งค่าข้อมูลผู้ใช้งาน',
        );
        $arrFooter['footer'] = $setTemplate['footer'];

        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/setkey', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function setgroupkey()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'USER';
        $arrMenu['page'] = 'ตั้งค่าข้อมูลผู้ใช้งาน';
        $arrData['duration'] = 20;
        $arrData['offset'] = 0;

        if (isset($_GET['id']) && intval($_GET['id']) > 0) {
            $arrData['id'] = $_GET['id'];

            $arrContent['data'] = $this->mol_manage_user->SelectGroupRolebyId($arrData['id']);
            $arrPermission = $this->mol_manage_user->SelectKeybyId($arrData['id']);
            foreach ($arrPermission['result'] as $key => $value) {
                $test[$key] = $value['keyname'];
            }
            $arrContent['data']['result']['key'] = $test;
        }
        $arrKey = $this->mol_manage_user->selectKeyAll();
        $arrContent['data']['key'] = $arrKey['result'];
        //  echo'<br/>data =';print_r($arrContent['data']);
        //  $arrMenu['customer'] = array('happyoil','wealthoil');
        $arrContent['template'] = array('header' => 'ตั้งค่าข้อมูลผู้ใช้งาน',
            'parent' => 'ผู้ใช้งาน',
            'link' => 'station/setgroupkey',
            'page' => 'ตั้งค่าข้อมูลผู้ใช้งาน',
        );
        $arrFooter['footer'] = $setTemplate['footer'];

        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/setgroupkey', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function addKey()
    {
        $data['KeyName'] = $this->input->post('keyname');
        $data['Value'] = $this->input->post('key');

        $result = $this->mol_manage_user->InsertKeyGroup($data['KeyName']);
        $keys = $this->mol_manage_user->GetRoleByName($data['KeyName']);
        foreach ($keys as $item) {
            if ($result == true) {
                foreach ($data['Value'] as $key => $value1) {
                    // var_dump($key);exit;
                    $this->mol_manage_user->InsertKeyPermission($item->Id, $key);
                }
            }
            $this->getRole();
        }

        //  var_dump($this->input->post('key'));exit;

    }

    public function editkey()
    {

        $data['KeyName'] = $this->input->post('keyname');
        $data['Value'] = $this->input->post('key');
        $data['GroupKey'] = $this->input->post('groupkey');
        $this->mol_manage_user->Deletekeyingroup($data['GroupKey']);
        $this->mol_manage_user->UpdateGroupkey($data['GroupKey'], $data['KeyName']);

        //$result = $this->mol_manage_user->InsertKeyGroup($data['KeyName']);
        $keys = $this->mol_manage_user->GetRoleByName($data['KeyName']);
        foreach ($keys as $item) {
            foreach ($data['Value'] as $key => $value1) {

                $this->mol_manage_user->InsertKeyPermission($item->Id, $key);
            }
            $this->getRole();
        }

    }

    public function setpassword()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'USER';
        $arrMenu['page'] = 'ตั้งค่ารหัสผ่านใหม่';
        $arrData['duration'] = 20;
        $arrData['offset'] = 0;

        if (isset($_GET['id']) && intval($_GET['id']) > 0) {
            $arrData['id'] = $_GET['id'];

            // var_dump($_GET['id']);
            $arrUserdata = $this->mol_manage_user->GetUserById($arrData['id']);

            foreach ($arrUserdata['result'] as $key => $value) {
                $test[$key] = $value['UserName'];
                $cusId[$key] = $value['CustomerId'];

                //var_dump($value['UserName']);

            }
            $arrContent['data']['result'] = $test;
            $arrContent['data']['cusId'] = $cusId;

            // var_dump($arrContent['data']['result']);

            // var_dump($Enpassword);

        }

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'ตั้งค่ารหัสผ่านใหม่',
            'parent' => 'ผู้ใช้งาน',
            'link' => 'station/setpassword',
            'page' => 'ตั้งค่ารหัสผ่านใหม่',
        );
        $arrFooter['footer'] = $setTemplate['footer'];

        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/setpassword', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function editpassword()
    {
        $data['Newpassword'] = $this->input->post('newpassword');
        $data['UserId'] = $this->input->post('userid');
        $data['Username'] = $this->input->post('username');
        $data['CustomerId'] = $this->input->post('customerid');
        $data['UpdateDate'] = date("Y-m-d H:i:s");

        $_SESSION['username'];

        var_dump($data['CustomerId']);
        $keyUser = $this->mol_setting->GetKey('UserKey', $data['CustomerId']);
        foreach ($keyUser as $item) {
            $enPass = $this->lib_crypted->Encrypt_password($data['Newpassword'], $item->Value);

            $userid = $this->mol_user->GetUser($_SESSION['username']);
            foreach ($userid as $item) {

                if ($enPass != null) {
                    $updatepassword = $this->mol_manage_user->UpdatePassword($data['UserId'], $enPass, $data['UpdateDate'], $item->Id);

                    if ($updatepassword != false) {

                        echo '<script>alert("You Have Successfully updated your Password");</script>';
                        redirect('manageuser/resetPassword', 'refresh');

                    } else {
                        echo '<script>alert("Update Password failed try again");</script>';
                        redirect('manageuser/resetPassword', 'refresh');
                        //$this->load->view('adminlte/register');
                    }

                }

            }
        }
        //}

    }

    public function edituser()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'USER';
        $arrMenu['page'] = 'ตั้งค่าผู้ใช้งาน';
        $arrData['duration'] = 20;
        $arrData['offset'] = 0;

        if (isset($_GET['id']) && intval($_GET['id']) > 0) {
            $arrData['id'] = $_GET['id'];

            // var_dump($_GET['id']);
            $arrUserdata = $this->mol_manage_user->GetUserById($arrData['id']);

            //var_dump($arrUserdata['result']);

            foreach ($arrUserdata['result'] as $key => $value) {
                $test[$key] = $value['UserName'];
                $cusId[$key] = $value['CustomerId'];
                $firstname[$key] = $value['FirstName'];
                $lastname[$key] = $value['LastName'];

                //  var_dump($value['FirstName']);
                //  var_dump($value['LastName']);

                //var_dump($value['UserName']);

            }
            $arrContent['data']['result'] = $test;
            $arrContent['data']['cusId'] = $cusId;
            $arrContent['data']['firstname'] = $firstname;
            $arrContent['data']['lastname'] = $lastname;

            // var_dump($arrContent['data']['result']);

            // var_dump($Enpassword);

        }

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'ตั้งค่าผู้ใช้งาน',
            'parent' => 'ผู้ใช้งาน',
            'link' => 'station/edituser',
            'page' => 'ตั้งค่าผู้ใช้งาน',
        );
        $arrFooter['footer'] = $setTemplate['footer'];

        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/edituser', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function updateuser()
    {
        //$data['Newpassword'] = $this->input->post('newpassword');
        $data['UserId'] = $this->input->post('userid');
        $data['Username'] = $this->input->post('username');
        $data['CustomerId'] = $this->input->post('customerid');
        $data['UpdateDate'] = date("Y-m-d H:i:s");
        $data['FirstName'] = $this->input->post('firstname');
        $data['LastName'] = $this->input->post('lastname');

        $updatename = $this->mol_manage_user->UpdateName($data['UserId'], $data['FirstName'], $data['LastName'], $data['UpdateDate']);
        if ($updatename != false) {
            echo '<script>alert("You Have Successfully updated your FirstName and LastName");</script>';
            redirect('manageuser/resetPassword', 'refresh');
        } else {
            echo '<script>alert("Update Password failed try again");</script>';
            redirect('manageuser/resetPassword', 'refresh');
            //$this->load->view('adminlte/register');
        }

    }

    public function Userlist()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'SETTING';
        $arrMenu['page'] = 'ตั้งค่าข้อมูลผู้ใช้งาน';
        //set variable DB
        $arrData['duration'] = 20;
        $arrData['offset'] = 0;
        // $arrContent['data'] = $this->mol_station->selectStationList();
        $arrContent['data'] = $this->mol_manage_user->selectuser($arrData);
        $arrContent['data']['linkedit'] = 'edituser';
        $arrContent['data']['linkadd'] = 'adduser';

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'ตั้งค่าข้อมูลผู้ใช้งาน',
            'parent' => 'ผู้ใช้งาน',
            'link' => 'station/StationData',
            'page' => 'ข้อมูลผู้ใช้งาน',
        );
        $arrFooter['footer'] = $setTemplate['footer'];

        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/user', $arrContent); //resetpassword
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function adduser()
    {
        $setTemplate = $this->lib_template->getTemplateVar();
        $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
        $arrMenu['permissiom'] = 'USER';
        $arrMenu['page'] = 'เพิ่มผู้ใช่งาน';
        $arrData['duration'] = 20;
        $arrData['offset'] = 0;

        $arrContent['data'] = $this->mol_customer->GetCustomerlist();

        $arrMenu['customer'] = array('happyoil', 'wealthoil');
        $arrContent['template'] = array('header' => 'เพิ่มผู้ใช้งาน',
            'parent' => 'ผู้ใช้งาน',
            'link' => 'manageuser/adduser',
            'page' => 'ตั้งค่าผู้ใช้งาน',
        );
        $arrFooter['footer'] = $setTemplate['footer'];

        $this->load->view('adminlte/menu', $arrMenu);
        $this->load->view('adminlte/navbar');
        $this->load->view('adminlte/AddUser', $arrContent);
        $this->load->view('adminlte/footer', $arrFooter);
    }

    public function insert_user()
    {

        $firstname = $this->input->post('fName');
        $lastname = $this->input->post('lName');
        $email = $this->input->post('email');
        $username = $this->input->post('UserName');
        $password = $this->input->post('password');
        $cusId = $this->input->post('CusId');
        $createdate = date("Y-m-d H:i:s");
        $keyUser = $this->mol_setting->GetKey('UserKey', $cusId);
        $userid = $this->mol_user->GetUser($_SESSION['username']);

        foreach ($keyUser as $item) {
            $enPass = $this->lib_crypted->Encrypt_password($password, $item->Value);
        }
        // var_dump($enPass);

        if ($enPass != null) {

            $data = array(
                'DeleteFlag' => 0,
                //'CreateUserId' => $userid['id'],
                'CreateDate' => $createdate,
                'Ordering' => 0,
                'UserName' => $username,
                'Password' => $enPass,
                'FirstName' => $firstname,
                'LastName' => $lastname,
                'UserStatus' => 0,
                'UserType' => 0,
                'OTPCreateDate' => $createdate,
            );

            $this->db->insert('Users', $data);
        }

    }
}