<?php
class routegroup extends CI_Controller
{
 public function __construct()
 {
  parent::__construct();
  // Your own constructor code
  //load lib CI
  $this->load->helper('url');

  //load lib custom
  $arrBlank = array();
  $this->load->library('lib_session');
  $this->load->library('lib_template', $arrBlank);

  //Load model
  $this->load->model('mol_routegroup');

  //check session
  $this->lib_session->chkSession();

 }
 public function showrouteGroup()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  $arrMenu['permissiom'] = 'Station';
  $arrMenu['page'] = 'เส้นทางเติมน้ำมัน';

  $arrMenu['customer'] = array('happyoil', 'wealthoil');
  $arrContent['template'] = array('header' => 'เส้นทางเติมน้ำมัน',
   'parent' => 'ตู้น้ำมัน',
   'link' => 'routegroup/showrouteGroup',
   'page' => 'เส้นทางเติมน้ำมัน',
  );
  $arrContent['data'] = $this->mol_routegroup->SelectRouteGroup();
  $arrContent['data']['linkadd'] = 'addroutegroup';
  $arrContent['data']['linkedit'] = 'editRouteGroup';

  $arrFooter['footer'] = $setTemplate['footer'];
  $this->load->view('adminlte/menu', $arrMenu);
  $this->load->view('adminlte/navbar');
  // $this->load->view('adminlte/routegroup',$arrContent);
  $this->load->view('adminlte/datatable', $arrContent);
  $this->load->view('adminlte/footer', $arrFooter);
 }
 public function addrouteGroup()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  $arrMenu['permissiom'] = 'Station';
  $arrMenu['page'] = 'เส้นทางเติมน้ำมัน';

  $arrMenu['customer'] = array('happyoil', 'wealthoil');
  $arrContent['template'] = array('header' => 'เส้นทาง',
   'parent' => 'เพิ่ม Route Group',
   'link' => 'routegroup/addrouteGroup',
   'page' => 'Route Group',
  );
  $arrFooter['footer'] = $setTemplate['footer'];
  $this->load->view('adminlte/menu', $arrMenu);
  $this->load->view('adminlte/navbar');
  $this->load->view('adminlte/addroutegroup', $arrContent);
  $this->load->view('adminlte/footer', $arrFooter);

 }
 public function insertRouteGroup()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  if (isset($_POST)) {
   // $arrData['id'] = $_GET['id'];
   $arrData['DeleteFlag'] = 0;
   $arrData['CreateDate'] = date("Y-m-d H:i:s");
   $arrData['Ordering'] = 0;
   $arrData['StatusId'] = 1;
   $arrData['Name'] = $_POST['NameRoute'];
   $arrData['Description'] = $_POST['Description'];
   $res = $this->mol_routegroup->InsertRouteGroup($arrData);
   //var_dump($res );exit();
  }
  redirect('routegroup/showrouteGroup', '');
 }
 public function editRouteGroup()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  $arrMenu['permissiom'] = 'Station';
  $arrMenu['page'] = 'เส้นทางเติมน้ำมัน';
  $arrData['duration'] = 20;
  $arrData['offset'] = 0;
  if (isset($_GET['id']) && intval($_GET['id']) > 0) {
   $arrData['id'] = $_GET['id'];
   $arrContent['selectEdit'] = $this->mol_routegroup->SelectRouteGroupById($arrData);
   $arrContent['id'] = $arrData['id'];
  }
  $arrMenu['customer'] = array('happyoil', 'wealthoil');
  $arrContent['template'] = array('header' => 'แก้ไขเส้นทาง',
   'parent' => 'แก้ไข Route Group',
   'link' => 'routegroup/editRouteGroup',
   'page' => 'Route Group',
  );
  $arrFooter['footer'] = $setTemplate['footer'];

  $this->load->view('adminlte/menu', $arrMenu);
  $this->load->view('adminlte/navbar');
  $this->load->view('adminlte/editroutegroup', $arrContent);
  $this->load->view('adminlte/footer', $arrFooter);
 }
 public function setEditRouteGroup()
 {
  $setTemplate = $this->lib_template->getTemplateVar();
  $arrMenu['menu'] = $this->lib_template->getTemplateMenu();
  if (isset($_POST)) {
   //var_dump($_POST['value']);exit();
   $arrData['Id'] = $_POST['Id'];
   $arrData['Name'] = $_POST['RouteName'];
   $arrData['Desc'] = $_POST['Description'];
   $resUpdate = $this->mol_routegroup->EditRouteGroup($arrData);
   //var_dump($resUpdate); exit();
   // $resUpdate = $this->mol_cost->InsertCost($arrData);
  }
  redirect('routegroup/showrouteGroup', '');
 }
}